/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.coretechies.veepropbeta.service.calculateDRBandRB;

import com.coretechies.veepropbeta.dao.bonustransaction.BonusTransactionDao;
import com.coretechies.veepropbeta.dao.commission.CommissionDao;
import com.coretechies.veepropbeta.dao.ingamecreditdetails.InGameCreditDetailsDao;
import com.coretechies.veepropbeta.dao.notification.NotificationDao;
import com.coretechies.veepropbeta.dao.traderrelativedata.TraderRelativeDataDao;
import com.coretechies.veepropbeta.dao.transaction.TransactionDao;
import com.coretechies.veepropbeta.domain.BonusTransaction;
import com.coretechies.veepropbeta.domain.Commission;
import com.coretechies.veepropbeta.domain.InGameTraderDetail;
import com.coretechies.veepropbeta.domain.Notification;
import com.coretechies.veepropbeta.domain.Transaction;
import com.coretechies.veepropbeta.web.controller.algorithm.CommisionPercentage;
import com.coretechies.veepropbeta.web.controller.algorithm.CommisionType;
import com.coretechies.veepropbeta.web.controller.algorithm.TransactionType;
import java.util.Date;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Noppp
 */
@Service
public class CalculateDRBAndRBServiceImpl implements CalculateDRBAndRBService{
    
    private final Logger logger = LoggerFactory.getLogger(getClass());
    
    @Autowired
    private TraderRelativeDataDao traderRelativeDataDao;
    
    @Autowired
    private InGameCreditDetailsDao inGameCreditDetailsDao;
    
    @Autowired
    private TransactionDao transactionDao;
            
    @Autowired
    private CommissionDao commissionDao;
    
    @Autowired
    private BonusTransactionDao bonusTransactionDao;
    
    @Autowired
    private NotificationDao notificationDao;

    @Override
    public void calculateDRB(InGameTraderDetail IGTD, Integer vCredit, Double bonusPercentage,Integer commisionType) {
        
        logger.info("bonus on :"+vCredit);
        
        int elligibleCP = CommisionPercentage.LEVEL.getCommisionPercentageByLevel(IGTD.getLevelIN());
        Double elligibleACP = IGTD.getACP();
        Double totalElligiblePercentage = elligibleACP + elligibleCP;
        Double residualBonusPercentage = bonusPercentage - totalElligiblePercentage;
        if(bonusPercentage < totalElligiblePercentage){
            totalElligiblePercentage = bonusPercentage;
        }
        if(totalElligiblePercentage > 10.00){
            totalElligiblePercentage = 10.00;
        }
        
        float bonusPaid = (float) (vCredit* totalElligiblePercentage/100);
        
        logger.info("Bonus Paid is:"+bonusPaid);
        float bonusAsVCredit = (float)(bonusPaid * 70/100);
        float bonusAsIGC = (float)(bonusPaid * 30/100);
        logger.info("vcredit bonus for user :"+IGTD.getIdentificationNumber()+" is :" +bonusAsVCredit);
        logger.info("IGC bonus for user :"+IGTD.getIdentificationNumber()+" is :" +bonusAsIGC);
        IGTD.setvCredit(IGTD.getvCredit()+bonusAsVCredit);
        IGTD.setIGC(IGTD.getIGC()+bonusAsIGC);
        inGameCreditDetailsDao.updateIGTD(IGTD);
        
        Transaction transaction = new Transaction();
        transaction.setAmount((double)bonusPaid);
        transaction.setSellerIN(2);
        transaction.setBuyerIN(IGTD.getIdentificationNumber());
        transaction.setTransactionDate(new Date());
        transaction.setTransactionTime(new Date(System.currentTimeMillis()));
        transaction.setTransactionType(TransactionType.BONUSTRANSACTION.getTransactionTypeValue());
        
        transactionDao.saveTransaction(transaction);
        
        Notification notification = new Notification();
        notification.setNotificationStatus(Boolean.FALSE);
        notification.setIdentificationNumber(IGTD.getIdentificationNumber());
        notification.setNotificationDate(new Date());
        notification.setNotification("You have recieved "+bonusPaid+" USD as "+CommisionType.BONUS.getCommisionType(commisionType));
        String link = "/veepropbeta/notification/"+transaction.getTransactionId()+"/"+transaction.getTransactionType();
        notification.setNotificationLink(link);
        
        notificationDao.saveNotification(notification);
        
        BonusTransaction bonusTransaction = new BonusTransaction();
        bonusTransaction.setBonusTransactionToken(transaction.getTransactionId());
        bonusTransaction.setBonusType(commisionType);
        
        bonusTransactionDao.saveBonus(bonusTransaction);
        
        Commission commission = new Commission();
        commission.setAmmount(bonusPaid);
        commission.setCommissionType(commisionType);
        commission.setTransactionId(transaction.getTransactionId());
        commission.setUserIn(IGTD.getIdentificationNumber());
        
        commissionDao.saveCommission(commission);
        
        if(residualBonusPercentage > 0.00){
            Integer retrieveParent = traderRelativeDataDao.retrieveReferalParent(IGTD.getIdentificationNumber());
            if(retrieveParent != 0){
                InGameTraderDetail parent = inGameCreditDetailsDao.retrieveIGCD(retrieveParent);
                calculateDRB(parent,vCredit,residualBonusPercentage,CommisionType.RB.getCommisionTypeValue());
            }
        }
    }
    
}
