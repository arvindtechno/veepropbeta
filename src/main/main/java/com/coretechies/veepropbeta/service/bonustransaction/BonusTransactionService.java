/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.coretechies.veepropbeta.service.bonustransaction;

import com.coretechies.veepropbeta.domain.BonusTransaction;
import java.util.List;

/**
 *
 * @author Noppp
 */
public interface BonusTransactionService {

    public BonusTransaction retrieveBonusTransactionByTransaction(Integer transactionId);

    public List<BonusTransaction> retrieveAllBonus(Integer report);
    
}
