/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.coretechies.veepropbeta.service.directreferaldecide;

import com.coretechies.veepropbeta.domain.InGameTraderDetail;

/**
 *
 * @author Noppp
 */
public interface DirectReferalDecideService {

    public InGameTraderDetail DirectReferalDecide(int levelDecided, InGameTraderDetail retrieveIGCD);
    
}
