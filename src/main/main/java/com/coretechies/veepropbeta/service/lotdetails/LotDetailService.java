/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.coretechies.veepropbeta.service.lotdetails;

import com.coretechies.veepropbeta.domain.LotDetails;
import java.util.List;

/**
 *
 * @author Arvind
 */
public interface LotDetailService {
    
   public Integer retrieveDetail();
    
   public void updateLotDetail(LotDetails lotdetail);
   
   public LotDetails retrieveLotDetail();
   
   public void updateShareCount(LotDetails lot, Integer shareSold);

    public List<LotDetails> retrieveAllLotEntries();
}
