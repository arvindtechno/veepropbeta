/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.coretechies.veepropbeta.service.transaction;

import com.coretechies.veepropbeta.dao.transaction.TransactionDao;
import com.coretechies.veepropbeta.domain.Transaction;
import java.util.Date;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Noppp
 */
@Service
public class TransactionServiceImpl implements TransactionService{
    
    private final Logger logger = LoggerFactory.getLogger(getClass());
    
    @Autowired
    private TransactionDao transactionDao;

    @Override
    public void saveTransaction(Transaction transaction) {
      transactionDao.saveTransaction(transaction);
    }

    @Override
    public Transaction retrieveTransaction(Integer transactionId) {
        return transactionDao.retrieveTransaction(transactionId);
    }

    @Override
    public void updateTransaction(Transaction transaction) {
        transactionDao.updateTransaction(transaction);
    }

    @Override
    public List<Transaction> retrieveSellTransactionByDates(int indentificationNumber, Date initialDate, Date EndDate, int transactionType) {
        return transactionDao.retrieveSellTransactionByDates(indentificationNumber, initialDate, EndDate, transactionType);
    }

    @Override
    public List<Transaction> retrieveBuyTransactionByDates(int indentificationNumber, Date initialDate, Date EndDate, int transactionType) {
        return transactionDao.retrieveBuyTransactionByDates(indentificationNumber, initialDate, EndDate, transactionType);
    }

    @Override
    public List<Transaction> retrieveTransactionsPastTenDates(Date date) {
        return transactionDao.retrieveTransactionsPastTenDates(date);
    }
    
    
}
