/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.coretechies.veepropbeta.service.notification;

import com.coretechies.veepropbeta.dao.notification.NotificationDao;
import com.coretechies.veepropbeta.domain.Notification;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Noppp
 */
@Service
public class NotificationServiceImpl implements NotificationService{
    
    @Autowired
    private NotificationDao notificationDao;
    
    @Override
    public void saveNotification(Notification notification){
        notificationDao.saveNotification(notification);
    }
    
    @Override
    public List<Notification> retreiveNotification(Integer identificationNumber) {
        return notificationDao.retreiveNotification(identificationNumber);
    }

    @Override
    public void updateNotification(Integer notificationId) {
        notificationDao.updateNotification(notificationId);
    }

    @Override
    public Notification retreiveNotificationById(Integer notificationId) {
        return notificationDao.retreiveNotificationById(notificationId);
    }
    
}
