package com.coretechies.veepropbeta.service.mail;

/**
 * Created with IntelliJ IDEA.
 * User: Hemant Joshi
 * Date: 02/07/13
 * Time: 21:54
 */
public interface MailService {

    void sendMail(String subject, String content, String to);
}
