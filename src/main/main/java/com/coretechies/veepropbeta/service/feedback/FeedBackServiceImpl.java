/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.coretechies.veepropbeta.service.feedback;


import com.coretechies.veepropbeta.dao.feedback.FeedbackDao;
import com.coretechies.veepropbeta.domain.Feedback;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FeedBackServiceImpl implements FeedBackService {
     
    @Autowired
    private FeedbackDao feedbackdao;
    
    @Override
    public void saveFeedBack(Feedback feedBack, int feedBackId) {
        feedbackdao.saveFeedBack(feedBack, feedBackId);
        
    }
    
    @Override
    public List<Feedback> retrieveFeedBack() {
        return feedbackdao.retrieveFeedBack();
    }
    
}
