/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.coretechies.veepropbeta.service.announcement;

import com.coretechies.veepropbeta.domain.Announcement;
import java.util.Date;
import java.util.List;
import com.coretechies.veepropbeta.dao.announcement.announcementDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
 *
 * @author Rohit
 */
@Service
public class announcementServiceImpl  implements announcementService
{

    @Autowired
    private announcementDao announcementdao;
    
    @Override
    public List<Announcement> Retriveannouncement(Date date) 
    {
        return announcementdao.Retriveannouncement(date);
    }

    @Override
    public void saveAnnouncement(Announcement announcement) {
        announcementdao.saveAnnouncement(announcement);
    }
    
}
