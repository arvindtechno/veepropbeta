/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.coretechies.veepropbeta.service.user;

import com.coretechies.veepropbeta.domain.SecurityQuestion;
import com.coretechies.veepropbeta.domain.User;
import java.util.List;

/**
 *
 * @author Noppp
 */
public interface UserService {
    
    public User retrieveUser(Integer identificationNumber); 

    public List<SecurityQuestion> retrieveSecurityQuestion();

    public void saveUser(User trader);

    public List<User> retrieveUser(String str);

    public void updateUser(User userGet);

    public User retrieveUserByMail(String str);

    public List<String> retrieveAllUserEmail();
    
}
