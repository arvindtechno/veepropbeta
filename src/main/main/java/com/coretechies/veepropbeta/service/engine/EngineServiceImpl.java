/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.coretechies.veepropbeta.service.engine;

import com.coretechies.veepropbeta.dao.engine.EngineDao;
import com.coretechies.veepropbeta.domain.Engine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EngineServiceImpl implements EngineService {
     
    @Autowired
    private EngineDao enginedao;
    public Engine retieveAll(Engine engine) {
        return enginedao.retieveAll(engine);
       
    }

    public Engine updateEngine(Engine engine) {
        return enginedao.updateEngine(engine);
       
    }
    
}
