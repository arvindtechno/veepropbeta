/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.coretechies.veepropbeta.service.dispute;

import com.coretechies.veepropbeta.dao.dispute.DisputeDao;
import com.coretechies.veepropbeta.domain.Dispute;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Noppp
 */
@Service
public class DisputeServiceImpl implements DisputeService{

    @Autowired
    private DisputeDao disputeDao;
    
    @Override
    public void saveDispute(Dispute dispute) {
        disputeDao.saveDispute(dispute);
    }

    @Override
    public Dispute retrieveDisputeForModByVCT(Integer vctId) {
        return disputeDao.retrieveDisputeForModByVCT(vctId);
    }

    @Override
    public void deleteDispute(Dispute dispute) {
        disputeDao.deleteDispute(dispute);
    }
    
}
