/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.coretechies.veepropbeta.service.traderrelativedata;

import com.coretechies.veepropbeta.dao.traderrelativedata.TraderRelativeDataDao;
import com.coretechies.veepropbeta.domain.TraderRelativeData;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Noppp
 */
@Service
public class TraderRelativeDataServiceImpl implements TraderRelativeDataService{
 
    @Autowired
    public TraderRelativeDataDao traderRelativeDataDao;

    @Override
    public List<TraderRelativeData> retrieveChild(Integer identificationNumber) {
        return traderRelativeDataDao.retrieveChild(identificationNumber); 
    }
    
    @Override
    public void saveTRD(TraderRelativeData trd) {
        traderRelativeDataDao.saveTRD(trd);
    }

    @Override
    public Integer retrieveReferalParent(Integer identificationNumber) {
        return traderRelativeDataDao.retrieveReferalParent(identificationNumber);
    }

    @Override
    public TraderRelativeData retrieveBinaryParent(Integer bp) {
        return traderRelativeDataDao.retrieveBinaryParent(bp);
    }
    
    
}
