/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.coretechies.veepropbeta.service.sharetradingtransaction;

import com.coretechies.veepropbeta.dao.sharetradingtransaction.ShareTradingTransactionDao;
import com.coretechies.veepropbeta.domain.ShareTradingTransaction;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Noppp
 */
@Service
public class ShareTradingTransactionServiceImpl implements ShareTradingTransactionService{

    @Autowired
    private ShareTradingTransactionDao shareTradingTransactionDao;
    
    @Override
    public void saveShareTrade(ShareTradingTransaction shareTradingTransaction) {
        shareTradingTransactionDao.saveShareTrade(shareTradingTransaction);
    }
    
    @Override
    public List<ShareTradingTransaction> retrievePendingTransaction(int transactionStatusValue) {
        return shareTradingTransactionDao.retrievePendingTransaction(transactionStatusValue);
    }

    @Override
    public ShareTradingTransaction retrieveSTT(Integer shareTransactionId) {
        return shareTradingTransactionDao.retrieveSTT(shareTransactionId);
    }

    @Override
    public void updateSTT(ShareTradingTransaction STT) {
        shareTradingTransactionDao.updateSTT(STT);
    }

    @Override
    public ShareTradingTransaction retrieveSTTByTransaction(Integer transactionId) {
        return shareTradingTransactionDao.retrieveSTTByTransaction(transactionId);
    }
    
    
}
