/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.coretechies.veepropbeta.service.traderdynamicdata;

import com.coretechies.veepropbeta.dao.traderdynamicdata.TraderDynamicDataDao;
import com.coretechies.veepropbeta.domain.TraderDynamicData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Noppp
 */
@Service
public class TraderDynamicDataServiceImpl implements TraderDynamicDataService{
    
    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private TraderDynamicDataDao traderDynamicDataDao;
    
    @Override
    public void saveTDD(TraderDynamicData tdd) {
        traderDynamicDataDao.saveTDD(tdd);
    }

    @Override
    public TraderDynamicData retrieveTDD(Integer identificationNumber) {
        return traderDynamicDataDao.retrieveTDD(identificationNumber);
    }

    @Override
    public void updateTDD(TraderDynamicData TDD) {
        traderDynamicDataDao.updateTDD(TDD);
    }
    
    
}
