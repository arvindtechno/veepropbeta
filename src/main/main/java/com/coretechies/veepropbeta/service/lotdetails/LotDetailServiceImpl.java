/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.coretechies.veepropbeta.service.lotdetails;

import com.coretechies.veepropbeta.dao.lotdetails.LotDetailsDao;
import com.coretechies.veepropbeta.domain.LotDetails;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LotDetailServiceImpl implements LotDetailService {
    
    @Autowired
    private LotDetailsDao lotdetailsdao;
    
    @Override
    public Integer retrieveDetail() {
        return lotdetailsdao.retrieveDetail();    
    }

    @Override
    public void updateLotDetail(LotDetails lotdetail) {
        lotdetailsdao.updateLotDetail(lotdetail);
    }

    @Override
    public LotDetails retrieveLotDetail() {
        return lotdetailsdao.retrieveLotDetail();
        
    }

    @Override
    public void updateShareCount(LotDetails lot,Integer shareSold) {
        lotdetailsdao.updateShareCount(lot,shareSold);
    }

    @Override
    public List<LotDetails> retrieveAllLotEntries() {
        return lotdetailsdao.retrieveAllLotEntries();
    }
}
