/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.coretechies.veepropbeta.service.faq;

import com.coretechies.veepropbeta.dao.faq.FaqDao;
import com.coretechies.veepropbeta.domain.Faq;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FaqServiceImpl implements FaqService {

    @Autowired
    private FaqDao faqdao;
   
    @Override
    public void saveFaqData(Faq faq) {
        faqdao.saveFaqData(faq);
    }

    @Override
    public List<Faq> retrieveFAQs() {
        return faqdao.retrieveFAQs();
    }
    
}
