/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.coretechies.veepropbeta.service.contact;

import com.coretechies.veepropbeta.dao.contact.ContactDao;
import com.coretechies.veepropbeta.domain.ContactUs;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ContactServiceImpl implements ContactService {
  
    @Autowired
    private ContactDao contactdao;
    
    @Override
    public void addContact(ContactUs contact) {
        contactdao.addContact(contact);
    }

    @Override
    public List<ContactUs> retrieveContacts() {
        return contactdao.retrieveContacts();
    }

    @Override
    public void deleteContact(Integer var) {
        contactdao.deleteContact(var);
    }
    
}
