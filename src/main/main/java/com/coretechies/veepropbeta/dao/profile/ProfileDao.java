/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.coretechies.veepropbeta.dao.profile;
import com.coretechies.veepropbeta.domain.User;
import com.coretechies.veepropbeta.domain.UserAddressInfo;
import com.coretechies.veepropbeta.domain.UserBankDetail;

/**
 *
 * @author Rohit
 */
public interface ProfileDao{
    
    public User retrivePersonal(Integer identificationNumber);
    
    public UserAddressInfo retriveAddress(Integer identificationNumber);
    
    public UserBankDetail retriveBank(Integer identificationNumber);
    
    public void addAddresssInfo(UserAddressInfo userAddressinfo);

    public void updateProfile(UserAddressInfo address, User personal);
}
