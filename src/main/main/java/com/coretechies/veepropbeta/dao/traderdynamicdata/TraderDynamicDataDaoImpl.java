/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.coretechies.veepropbeta.dao.traderdynamicdata;

import com.coretechies.veepropbeta.domain.TraderDynamicData;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Noppp
 */
@Repository("TraderDynamicDataDao")
public class TraderDynamicDataDaoImpl implements TraderDynamicDataDao{
    
    private final Logger logger = LoggerFactory.getLogger(getClass());
    
    private EntityManager entityManager;

    @PersistenceContext
    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Transactional(readOnly = false)
    public void saveTDD(TraderDynamicData tdd) {
        try{
            entityManager.persist(tdd);
            logger.info("Trader Dynamic Data Added Successfully");
        }catch(Exception e){
            logger.info("Trader Dynamic Data Added Failed:"+e.getStackTrace());
        }
    }

    @Override
    public TraderDynamicData retrieveTDD(Integer bp) {
        TypedQuery<TraderDynamicData> query = entityManager.createQuery("select t from TraderDynamicData t where t.identificationNumber=:identificationNumber", TraderDynamicData.class);
        query.setParameter("identificationNumber", bp);
        return query.getSingleResult();
    }

    @Override
    @Transactional(readOnly = false)
    public void updateTDD(TraderDynamicData binaryParentTDD) {
        try{
            entityManager.merge(binaryParentTDD);
        }catch(Exception e){
            logger.info("error in updateing TDD is :"+e.getStackTrace());
        }
        
    }
}
