/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.coretechies.veepropbeta.dao.contact;

import com.coretechies.veepropbeta.domain.ContactUs;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Arvind
 */
@Repository("ContactDao")
public class ContactDaoImp implements ContactDao{
           
     private final Logger logger = LoggerFactory.getLogger(getClass());

     private EntityManager entitymanager;

    @PersistenceContext
    public void setEntitymanager(EntityManager entitymanager) {
        this.entitymanager = entitymanager;
    }
    
    @Override
    @Transactional
    public void addContact(ContactUs contact) {
        try{
             entitymanager.persist(contact);
        }
        catch(Exception e) {
           logger.info("Error"+e);
        }    
    }

    @Override
    public List<ContactUs> retrieveContacts() {
        TypedQuery<ContactUs> query = entitymanager.createQuery("select c from ContactUs c",ContactUs.class);
        return query.getResultList();
    }

    @Override
    @Transactional(readOnly = false)
    public void deleteContact(Integer var) {
         ContactUs contact = entitymanager.find(ContactUs.class, var);
         try{
             entitymanager.remove(contact);
         }catch(Exception e){
             logger.info("error in delete is:"+e.getStackTrace());
         }
    }
    
}
