/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.coretechies.veepropbeta.dao.announcement;
import com.coretechies.veepropbeta.domain.Announcement;
import java.util.Date;
import java.util.List;


/**
 *
 * @author Rohit
 */
public interface announcementDao 
{
    public List<Announcement> Retriveannouncement(Date date);
    
    public void saveAnnouncement(Announcement announcement);       
}
