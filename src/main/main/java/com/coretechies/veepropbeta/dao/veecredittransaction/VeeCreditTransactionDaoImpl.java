/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.coretechies.veepropbeta.dao.veecredittransaction;

import com.coretechies.veepropbeta.domain.VeeCreditTransaction;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Noppp
 */
@Repository("VeeCreditTransactionDao")
public class VeeCreditTransactionDaoImpl implements VeeCreditTransactionDao{
    
    private final Logger logger = LoggerFactory.getLogger(getClass());
    
    private EntityManager entityManager;

    @PersistenceContext
    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    @Transactional(readOnly = false)
    public void saveVCT(VeeCreditTransaction vct) {
        try{
            entityManager.persist(vct);
        }catch(Exception e){
            logger.info("Transaction failed:"+e.getStackTrace());
        }
    }

    @Override
    public List<VeeCreditTransaction> retrievePendingTransaction(int transactionStatusValue) {
        TypedQuery<VeeCreditTransaction> query = entityManager.createQuery("Select v from VeeCreditTransaction v where v.transactionStatus=:transactionStatusValue", VeeCreditTransaction.class);
        query.setParameter("transactionStatusValue", transactionStatusValue);
        return query.getResultList();
    }

    @Override
    public VeeCreditTransaction retrieveVCT(Integer veeCreditTransactionId) {
        return entityManager.find(VeeCreditTransaction.class, veeCreditTransactionId);
    }

    @Override
    @Transactional(readOnly = false)
    public void updateVCT(VeeCreditTransaction VCT) {
        try{
            entityManager.merge(VCT);
        }catch(Exception e){
            logger.info("error in updation VCT is"+e.getStackTrace());
        }
    }

    @Override
    public VeeCreditTransaction retrieveVCTByTransaction(Integer transactionId) {
        TypedQuery<VeeCreditTransaction> query = entityManager.createQuery("Select v from VeeCreditTransaction v where v.veeCreditTransactionToken=:transactionId",VeeCreditTransaction.class);
        query.setParameter("transactionId", transactionId);
        return query.getSingleResult();
    } 
}
