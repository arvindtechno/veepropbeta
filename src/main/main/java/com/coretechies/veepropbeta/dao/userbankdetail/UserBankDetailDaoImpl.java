/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.coretechies.veepropbeta.dao.userbankdetail;

import com.coretechies.veepropbeta.domain.UserBankDetail;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository("UserBankDetailDao")
public class UserBankDetailDaoImpl implements UserBankDetailDao {
    private final Logger logger=LoggerFactory.getLogger(getClass());
    private EntityManager entitymanager;
    
    @PersistenceContext
    public void setEntitymanager(EntityManager entitymanager) {
        this.entitymanager = entitymanager;
    }
    
    @Override
    @Transactional(readOnly = false)
    public void addBankDeatil(UserBankDetail bankDetail, int UserId) {
        try{
            bankDetail.setIdentificationNumber(UserId);
            entitymanager.persist(bankDetail);
            logger.info("Bank Detail Added Successfully");
        }catch(Exception e){
            logger.info("Error"+e.getStackTrace());
        }
    }

     @Override
     @Transactional
    public UserBankDetail retriveBank(Integer identificationNumber){
        TypedQuery<UserBankDetail> query=entitymanager.createQuery("Select b From UserBankDetail b where b.identificationNumber=:identificationNumber", UserBankDetail.class);
        query.setParameter("identificationNumber",identificationNumber);
        return query.getSingleResult();
    }
     @Override
     @Transactional
     public void updateBankDetail(UserBankDetail bankDetail) {
        try{
           
            entitymanager.merge(bankDetail);
        }catch(Exception e){
            logger.info("Error"+e.getStackTrace());
        }
    }
}
