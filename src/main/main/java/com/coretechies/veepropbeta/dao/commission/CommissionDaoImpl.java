/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.coretechies.veepropbeta.dao.commission;

import com.coretechies.veepropbeta.domain.Commission;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Noppp
 */
@Repository("CommissionDao")
public class CommissionDaoImpl implements CommissionDao{
    
    private final Logger logger =LoggerFactory.getLogger(getClass());
    
    private EntityManager entityManager;

    @PersistenceContext
    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    @Transactional(readOnly = false)
    public void saveCommission(Commission commission) {
        try{
            entityManager.persist(commission);
        }catch(Exception e){
            logger.info("error in transaction commission is:"+e.getStackTrace());
        }
    }
    
    
}
