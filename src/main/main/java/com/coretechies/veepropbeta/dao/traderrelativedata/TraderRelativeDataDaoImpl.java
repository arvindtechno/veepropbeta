/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.coretechies.veepropbeta.dao.traderrelativedata;

import com.coretechies.veepropbeta.domain.TraderRelativeData;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Noppp
 */
@Repository("TraderRelativeDataDao")
public class TraderRelativeDataDaoImpl implements TraderRelativeDataDao{

    private final Logger logger = LoggerFactory.getLogger(getClass());
    
    private EntityManager entityManager;

    @PersistenceContext
    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }
    
    @Override
    public List<TraderRelativeData> retrieveChild(Integer identificationNumber) {
        logger.info("object is here 1");
        TypedQuery<TraderRelativeData> query = entityManager.createQuery("select t from TraderRelativeData t where t.bP=:bP order by t.pTP asc",TraderRelativeData.class);
        query.setParameter("bP", identificationNumber);
        try{
            return query.getResultList();
        }catch(Exception e){
            logger.info("Error in query"+e.getStackTrace());
            return null;
        }
    }

    @Transactional(readOnly = false)
    @Override
    public void saveTRD(TraderRelativeData trd) {
        try{
            entityManager.persist(trd);
            logger.info("Trader Relative Data added Successfully");
        }catch(Exception e){
            logger.info("Trader Relative Data added Failed:"+e.getStackTrace());
        }
    }
    
    @Override
    public Integer retrieveReferalParent(Integer identificationNumber) {
        logger.info("identified is:"+identificationNumber);
        TypedQuery<Integer> query = entityManager.createQuery("select t.rP from TraderRelativeData t where t.identificationNumber=:identificationNumber",Integer.class);
        query.setParameter("identificationNumber", identificationNumber);
        try{
            return query.getSingleResult();
        }catch(Exception NPE){
            logger.info("No Parent Found"+NPE.getStackTrace());
            return 0;
        }
    }

    @Override
    public TraderRelativeData retrieveBinaryParent(Integer bp) {
        logger.info("identified is:"+bp);
        TypedQuery<TraderRelativeData> query = entityManager.createQuery("select t from TraderRelativeData t where t.identificationNumber=:identificationNumber",TraderRelativeData.class);
        query.setParameter("identificationNumber", bp);
        try{
            return query.getSingleResult();
        }catch(Exception NPE){
            logger.info("No Parent Found"+NPE.getStackTrace());
            return null;
        }    
    }
    
}
