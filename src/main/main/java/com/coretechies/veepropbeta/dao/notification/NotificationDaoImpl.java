/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.coretechies.veepropbeta.dao.notification;

import com.coretechies.veepropbeta.domain.Notification;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Noppp
 */
@Repository("NotificationDao")
public class NotificationDaoImpl implements NotificationDao{
    
    private final Logger logger = LoggerFactory.getLogger(getClass());
    
    private EntityManager entityManager;

    @PersistenceContext
    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public Notification retrieveNotification(Integer notificationId){
        return entityManager.find(Notification.class, notificationId);
    }
    
    @Override
    @Transactional(readOnly = false)
    public void saveNotification(Notification notification) {
        try{
            entityManager.persist(notification);
        }catch(Exception e){
            logger.info("error in Notification Save is:"+e.getStackTrace());
        }
    }

    public List<Notification> retreiveNotification(Integer identificationNumber) {
        TypedQuery<Notification> query = entityManager.createQuery("select n from Notification n where n.identificationNumber=:identificationNumber and n.notificationStatus=:check",Notification.class);
        query.setParameter("identificationNumber", identificationNumber);
        query.setParameter("check",Boolean.FALSE);
        return query.getResultList();
    }

    @Override
    @Transactional(readOnly = false)
    public void updateNotification(Integer notificationId) {
        Notification notification = retrieveNotification(notificationId);
        notification.setNotificationStatus(Boolean.TRUE); //read
        try{
            entityManager.merge(notification);
        }catch(Exception e){
            logger.info("error in notification update is:"+e.getStackTrace());
        }
    }

    @Override
    public Notification retreiveNotificationById(Integer notificationId) {
        return entityManager.find(Notification.class, notificationId);
    }
    
    
}
