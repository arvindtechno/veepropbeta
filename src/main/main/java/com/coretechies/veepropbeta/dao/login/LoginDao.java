/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.coretechies.veepropbeta.dao.login;

import com.coretechies.veepropbeta.domain.Login;
import java.util.List;

/**
 *
 * @author Noppp
 */
public interface LoginDao {

    public Boolean isCredential(Login login);
    
    public Login retrieveUser(String userName);

    public void saveTraderCredentials(Login log);
    
    public Long countUser(String userRole);

    public List<Integer> retrieveUserByRole(String userRoleValue);

    public Login retrieveUser(Integer buyerIN);
}
