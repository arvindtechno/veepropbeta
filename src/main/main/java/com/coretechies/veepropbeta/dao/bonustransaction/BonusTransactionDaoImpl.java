/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.coretechies.veepropbeta.dao.bonustransaction;

import com.coretechies.veepropbeta.domain.BonusTransaction;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Noppp
 */
@Repository("BonusTransactionDao")
public class BonusTransactionDaoImpl implements BonusTransactionDao{
    
    private final Logger logger = LoggerFactory.getLogger(getClass());
    
    private EntityManager entityManager;

    @PersistenceContext
    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    @Transactional(readOnly = false)
    public void saveBonus(BonusTransaction bonusTransaction) {
        try{
            entityManager.persist(bonusTransaction);
        }catch(Exception e){
            logger.info("error in bonus transaction is:"+e.getStackTrace());
        }
    }

    public BonusTransaction retrieveBonusTransactionByTransaction(Integer transactionId) {
        TypedQuery<BonusTransaction> query = entityManager.createQuery("Select b from BonusTransaction b where b.bonusTransactionToken=:transactionId", BonusTransaction.class);
        query.setParameter("transactionId", transactionId);
        return query.getSingleResult();
    }

    @Override
    public List<BonusTransaction> retrieveAllBonus(Integer bonusType) {
        TypedQuery<BonusTransaction> query = entityManager.createQuery(""
                + "select b from BonusTransaction b where b.bonusType=:bonus", BonusTransaction.class);
        query.setParameter("bonus", bonusType);
        return query.getResultList();
    }
    
    
}
