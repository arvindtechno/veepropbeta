/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.coretechies.veepropbeta.dao.transaction;

import com.coretechies.veepropbeta.domain.Transaction;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Noppp
 */
public interface TransactionDao {

    public void saveTransaction(Transaction transaction);

    public Transaction retrieveTransaction(Integer transactionId);

    public void updateTransaction(Transaction transaction);

    public List<Transaction> retrieveSellTransactionByDates(int indentificationNumber,Date initialDate,Date EndDate,int transactionType);
    
    public List<Transaction> retrieveBuyTransactionByDates(int indentificationNumber,Date initialDate,Date EndDate,int transactionType);

    public List<Transaction> retrieveTransactionsPastTenDates(Date date);
    
}
