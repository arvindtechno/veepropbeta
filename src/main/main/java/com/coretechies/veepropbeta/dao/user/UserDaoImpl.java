/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.coretechies.veepropbeta.dao.user;

import com.coretechies.veepropbeta.domain.SecurityQuestion;
import com.coretechies.veepropbeta.domain.User;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Noppp
 */
@Repository("UserDao")
public class UserDaoImpl implements UserDao{
    
    private final Logger logger = LoggerFactory.getLogger(getClass());
    
    private EntityManager entityManager;

    @PersistenceContext
    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public User retrieveUser(Integer identificationNumber) {
        logger.info("id is"+ identificationNumber);
        return entityManager.find(User.class, identificationNumber);
    } 

    @Override
    public List<SecurityQuestion> retrieveSecurityQuestion() {
        TypedQuery<SecurityQuestion> query = entityManager.createQuery("select s from SecurityQuestion s",SecurityQuestion.class);
        try{
            return  query.getResultList();
        }catch(Exception e){
            logger.info("error is:"+e.getStackTrace());
            return null;
        }
    }

    @Transactional(readOnly = false)
    public void saveUser(User trader) {
        try{
            entityManager.persist(trader);
            logger.info("Trader Registered SuccessFully");
        }catch(Exception e){
            logger.info("Trader Registered Failed:"+e.getStackTrace());
        }
    }

    public List<User> retrieveUser(String str) {
        TypedQuery<User> query = entityManager.createQuery("select u from User u where u.name like :str or u.email like :str",User.class);
        query.setParameter("str", "%"+str+"%");
        return query.getResultList();
    }

    @Override
    @Transactional(readOnly = false)
    public void updateUser(User user) {
        try{
            entityManager.merge(user);
        }catch(Exception e){
            logger.info("error in update user is");
        }
    }

    @Override
    public User retrieveUserByMail(String str) {
        try{
            TypedQuery<User> query = entityManager.createQuery("select u from User u where u.email=:str",User.class);
            query.setParameter("str",str);
            return query.getSingleResult();
        }catch(Exception e){
            logger.info("error in email search is:"+e.getStackTrace());
        }
        return null;
    }

    @Override
    public List<String> retrieveAllUserEmail() {
        TypedQuery<String> query = entityManager.createQuery("select u.email from User u", String.class);
        return query.getResultList();
    }
}
