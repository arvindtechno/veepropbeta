/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.coretechies.veepropbeta.dao.traderdynamicdata;

import com.coretechies.veepropbeta.domain.TraderDynamicData;

/**
 *
 * @author Noppp
 */
public interface TraderDynamicDataDao {

    public void saveTDD(TraderDynamicData tdd);

    public TraderDynamicData retrieveTDD(Integer bp);

    public void updateTDD(TraderDynamicData binaryParentTDD);
    
}
