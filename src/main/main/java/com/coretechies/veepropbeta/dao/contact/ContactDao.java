/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.coretechies.veepropbeta.dao.contact;

import com.coretechies.veepropbeta.domain.ContactUs;
import java.util.List;

/**
 *
 * @author Arvind
 */
public interface ContactDao {
    
    public void addContact(ContactUs contact);

    public List<ContactUs> retrieveContacts();

    public void deleteContact(Integer var);
    
}
