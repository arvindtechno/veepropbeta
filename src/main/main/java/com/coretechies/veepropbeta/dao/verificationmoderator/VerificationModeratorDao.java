/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.coretechies.veepropbeta.dao.verificationmoderator;

import com.coretechies.veepropbeta.domain.VerificationModerator;
import java.util.List;

/**
 *
 * @author Noppp
 */
public interface VerificationModeratorDao {

    public void saveVerificationModerator(VerificationModerator verificationModerator);

    public VerificationModerator retrieveVM(Integer transactionId);

    public void updateVM(VerificationModerator retrieveVM);
    
    public void deleteVM(VerificationModerator retrieveVM);

    public List<VerificationModerator> retrieveVMApprovedByModerator(Integer moderatorIN);
    
}
