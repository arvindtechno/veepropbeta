/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.coretechies.veepropbeta.domain;

import java.sql.Time;
import java.util.Date;
import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author Arvind
 */
@Entity
@Table(name="upgradationrequest")
@Access(AccessType.FIELD)
public class UpgradationRequest {
    
    @Id
    @GenericGenerator(name = "upgradationrequest", strategy = "increment")
    @GeneratedValue(generator = "upgradationrequest")
    private Integer upgradationRequestID;
    
    @Column
    private Integer IdentificationNumber;
    
    @Column
    private Integer rank;
    
    @Column
    private Integer amount;
   
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date requestDate;
    
    @Column
    private Time requestTime;
    
    @Column
    private Integer requestStatus;

    public Integer getUpgradationRequestID() {
        return upgradationRequestID;
    }

    public void setUpgradationRequestID(Integer upgradationRequestID) {
        this.upgradationRequestID = upgradationRequestID;
    }

    public Integer getIdentificationNumber() {
        return IdentificationNumber;
    }

    public void setIdentificationNumber(Integer IdentificationNumber) {
        this.IdentificationNumber = IdentificationNumber;
    }

    public Integer getRank() {
        return rank;
    }

    public void setRank(Integer rank) {
        this.rank = rank;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public Date getRequestDate() {
        return requestDate;
    }

    public void setRequestDate(Date requestDate) {
        this.requestDate = requestDate;
    }

    public Time getRequestTime() {
        return requestTime;
    }

    public void setRequestTime(Time requestTime) {
        this.requestTime = requestTime;
    }

    public Integer getRequestStatus() {
        return requestStatus;
    }

    public void setRequestStatus(Integer requestStatus) {
        this.requestStatus = requestStatus;
    }

    @Override
    public String toString() {
        return "upgradationrequest{" + "upgradationRequestID=" + upgradationRequestID + ", IdentificationNumber=" + IdentificationNumber + ", rank=" + rank + ", amount=" + amount + ", requestDate=" + requestDate + ", requestTime=" + requestTime + ", requestStatus=" + requestStatus + '}';
    }
    
    
    
}
