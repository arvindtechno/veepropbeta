/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.coretechies.veepropbeta.domain;

import java.util.Date;
import javax.persistence.*;
import org.hibernate.annotations.GenericGenerator;
/**
 *
 * @author Noppp
 */
@Entity
@Table(name = "ingametraderdetail")
@Access(AccessType.FIELD)
public class InGameTraderDetail {
    
    @Id
    @GenericGenerator(name = "ingametraderdetail", strategy = "increment")
    @GeneratedValue(generator = "ingametraderdetail")
    private Integer inGameTraderDetailId;
    
    @Column
    private Integer identificationNumber;
    
    @Column
    private Integer levelIN;
    
    @Column
    private Integer shareHold;
    
    @Column
    private Double vCredit;
    
    @Column
    private Double IGC;
    
    @Column
    private Double DMB;
    
    @Column
    @Temporal(TemporalType.DATE)
    private Date LTD;
    
    @Column
    private Integer AERG;
    
    @Column
    private Double ACP;
    
    @Column
    private Integer DR1000;

    @Column
    private Integer DR2000;    

    @Column
    private Integer DR5000;

    @Column
    private Integer DRS;

    @Column
    private Integer DRG;

    @Column
    private Integer DRD;

    @Column 
    private Integer title;
    
    @Column
    private Double vCreditHold;

    public Double getvCreditHold() {
        return vCreditHold;
    }

    public void setvCreditHold(Double vCreditHold) {
        this.vCreditHold = vCreditHold;
    }

    public Integer getInGameTraderDetailId() {
        return inGameTraderDetailId;
    }

    public void setInGameTraderDetailId(Integer inGameTraderDetailId) {
        this.inGameTraderDetailId = inGameTraderDetailId;
    }

    public Integer getIdentificationNumber() {
        return identificationNumber;
    }

    public void setIdentificationNumber(Integer identificationNumber) {
        this.identificationNumber = identificationNumber;
    }

    public Integer getLevelIN() {
        return levelIN;
    }

    public void setLevelIN(Integer levelIN) {
        this.levelIN = levelIN;
    }

    public Integer getShareHold() {
        return shareHold;
    }

    public void setShareHold(Integer shareHold) {
        this.shareHold = shareHold;
    }

    public Double getvCredit() {
        return vCredit;
    }

    public void setvCredit(Double vCredit) {
        this.vCredit = vCredit;
    }

    public Double getIGC() {
        return IGC;
    }

    public void setIGC(Double IGC) {
        this.IGC = IGC;
    }

    public Double getDMB() {
        return DMB;
    }

    public void setDMB(Double DMB) {
        this.DMB = DMB;
    }

    public Date getLTD() {
        return LTD;
    }

    public void setLTD(Date LTD) {
        this.LTD = LTD;
    }

    public Integer getAERG() {
        return AERG;
    }

    public void setAERG(Integer AERG) {
        this.AERG = AERG;
    }

    public Double getACP() {
        return ACP;
    }

    public void setACP(Double ACP) {
        this.ACP = ACP;
    }

    public Integer getDR1000() {
        return DR1000;
    }

    public void setDR1000(Integer DR1000) {
        this.DR1000 = DR1000;
    }

    public Integer getDR2000() {
        return DR2000;
    }

    public void setDR2000(Integer DR2000) {
        this.DR2000 = DR2000;
    }

    public Integer getDR5000() {
        return DR5000;
    }

    public void setDR5000(Integer DR5000) {
        this.DR5000 = DR5000;
    }

    public Integer getDRS() {
        return DRS;
    }

    public void setDRS(Integer DRS) {
        this.DRS = DRS;
    }

    public Integer getDRG() {
        return DRG;
    }

    public void setDRG(Integer DRG) {
        this.DRG = DRG;
    }

    public Integer getDRD() {
        return DRD;
    }

    public void setDRD(Integer DRD) {
        this.DRD = DRD;
    }

    public Integer getTitle() {
        return title;
    }

    public void setTitle(Integer title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return "InGameTraderDetail{" + "inGameTraderDetailId=" + inGameTraderDetailId + ", identificationNumber=" + identificationNumber + ", levelIN=" + levelIN + ", shareHold=" + shareHold + ", vCredit=" + vCredit + ", IGC=" + IGC + ", DMB=" + DMB + ", LTD=" + LTD + ", AERG=" + AERG + ", ACP=" + ACP + ", DR1000=" + DR1000 + ", DR2000=" + DR2000 + ", DR5000=" + DR5000 + ", DRS=" + DRS + ", DRG=" + DRG + ", DRD=" + DRD + ", title=" + title + ", vCreditHold=" + vCreditHold + '}';
    }
}
