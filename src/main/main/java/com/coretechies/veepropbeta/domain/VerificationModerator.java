/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.coretechies.veepropbeta.domain;

import java.sql.Time;
import java.util.Date;
import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author Arvind
 */
@Entity
@Table
@Access(AccessType.FIELD)
public class VerificationModerator {
    
     @Id
     @GenericGenerator(name = "verificationModertor", strategy = "increment")
     @GeneratedValue(generator = "verificationModertor")
     private Integer verificationId;
    
    @Column
    private Integer transactionId;
    
    @Column
    private Integer identificationNumber;
    
    @Column
    @Temporal(TemporalType.DATE)
    private Date verificationDate;
    
    @Column
    @Temporal(TemporalType.TIME)
    private Date verificationTime;
   
    @Column
    private String Comment;
    
    @Column
    private Boolean verificationStatus;

    public Boolean isVerificationStatus() {
        return verificationStatus;
    }

    public void setVerificationStatus(Boolean verificationStatus) {
        this.verificationStatus = verificationStatus;
    }
    
    public Integer getVerificationId() {
        return verificationId;
    }

    public void setVerificationId(Integer verificationId) {
        this.verificationId = verificationId;
    }

    public Integer getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(Integer transactionId) {
        this.transactionId = transactionId;
    }

    public Integer getIdentificationNumber() {
        return identificationNumber;
    }

    public void setIdentificationNumber(Integer identificationNumber) {
        this.identificationNumber = identificationNumber;
    }

    public Date getVerificationDate() {
        return verificationDate;
    }

    public void setVerificationDate(Date verificationDate) {
        this.verificationDate = verificationDate;
    }

    public Date getVerificationTime() {
        return verificationTime;
    }

    public void setVerificationTime(Date verificationTime) {
        this.verificationTime = verificationTime;
    }

    public String getComment() {
        return Comment;
    }

    public void setComment(String Comment) {
        this.Comment = Comment;
    }

    @Override
    public String toString() {
        return "VerificationModerator{" + "verificationId=" + verificationId + ", transactionId=" + transactionId + ", identificationNumber=" + identificationNumber + ", verificationDate=" + verificationDate + ", verificationTime=" + verificationTime + ", Comment=" + Comment + ", verificationStatus=" + verificationStatus + '}';
    }
}
