/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.coretechies.veepropbeta.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.constraints.NotBlank;

/**
 *
 * @author Arvind
 */
@Entity
@Table(name="useraddressinfo")
public class UserAddressInfo {
    @Id
    @GenericGenerator(name = "useraddressinfo", strategy = "increment")
    @GeneratedValue(generator = "useraddressinfo")
    private Integer userAddressInfoId;
    
    @Column
    private Integer IdentificationNumber;
    
    @Column
    private String houseNumber;
    
    @Column
    private String streetName1;
    
    @Column
    private String streetName2;
    
    @Column
    private String city;
    
    @Column
    private String landmark;
    
    @Column
    private String userstate;
    
    @Column
    private String country;
    
    @Column
    private String zipcode;

    public Integer getUserAddressInfoId() {
        return userAddressInfoId;
    }

    public void setUserAddressInfoId(Integer userAddressInfoId) {
        this.userAddressInfoId = userAddressInfoId;
    }

    public Integer getIdentificationNumber() {
        return IdentificationNumber;
    }

    public void setIdentificationNumber(Integer IdentificationNumber) {
        this.IdentificationNumber = IdentificationNumber;
    }

    public String getHouseNumber() {
        return houseNumber;
    }

    public void setHouseNumber(String houseNumber) {
        this.houseNumber = houseNumber;
    }

    public String getStreetName1() {
        return streetName1;
    }

    public void setStreetName1(String streetName1) {
        this.streetName1 = streetName1;
    }

    public String getStreetName2() {
        return streetName2;
    }

    public void setStreetName2(String streetName2) {
        this.streetName2 = streetName2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getLandmark() {
        return landmark;
    }

    public void setLandmark(String landmark) {
        this.landmark = landmark;
    }

    public String getUserstate() {
        return userstate;
    }

    public void setUserstate(String userstate) {
        this.userstate = userstate;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    @Override
    public String toString() {
        return "useraddressinfo{" + "userAddressInfoId=" + userAddressInfoId + ", IdentificationNumber=" + IdentificationNumber + ", houseNumber=" + houseNumber + ", streetName1=" + streetName1 + ", streetName2=" + streetName2 + ", city=" + city + ", landmark=" + landmark + ", userstate=" + userstate + ", country=" + country + ", zipcode=" + zipcode + '}';
    }
    
    
}
