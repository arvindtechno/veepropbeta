/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.coretechies.veepropbeta.domain;

import java.util.Date;
import javax.persistence.*;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.constraints.NotBlank;

/**
 *
 * @author Noppp
 */
@Entity
@Table(name = "login")
@Access(AccessType.FIELD)
public class Login {
    
    @Id
    @GenericGenerator(name = "login", strategy = "increment")
    @GeneratedValue(generator = "login")
    private Integer loginId;
    
    @Column
    private Integer identificationNumber;
    
    @Column
    @NotBlank(message = "Please Enter A UserName")
    private String userName;
    
    @Column
    @NotBlank(message = "Please Enter a Password")
    private String password;
    
    @Column
    private String userRole;
    
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastLoginAttempt;

    public Integer getLoginId() {
        return loginId;
    }

    public void setLoginId(Integer loginId) {
        this.loginId = loginId;
    }

    public Integer getIdentificationNumber() {
        return identificationNumber;
    }

    public void setIdentificationNumber(Integer identificationNumber) {
        this.identificationNumber = identificationNumber;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUserRole() {
        return userRole;
    }

    public void setUserRole(String userRole) {
        this.userRole = userRole;
    }

    public Date getLastLoginAttempt() {
        return lastLoginAttempt;
    }

    public void setLastLoginAttempt(Date lastLoginAttempt) {
        this.lastLoginAttempt = lastLoginAttempt;
    }

    @Override
    public String toString() {
        return "login{" + "loginId=" + loginId + ", identificationNumber=" + identificationNumber + ", userName=" + userName + ", password=" + password + ", userRole=" + userRole + ", lastLoginAttempt=" + lastLoginAttempt + '}';
    }
}

