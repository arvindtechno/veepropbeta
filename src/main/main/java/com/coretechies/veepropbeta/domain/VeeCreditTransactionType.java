/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.coretechies.veepropbeta.domain;

/**
 *
 * @author Noppp
 */
public enum VeeCreditTransactionType {
    
    FROMTRADER(0),
    
    FROMCOMPANY(1);
    
    private final int VeeCreditTransactionTypeValue;
    
    private VeeCreditTransactionType(int VeeCreditTransactionTypeValue){
        this.VeeCreditTransactionTypeValue = VeeCreditTransactionTypeValue;
    }
    
    public int getVeeCreditTransactionTypeValue(){
        return VeeCreditTransactionTypeValue;
    }
}
