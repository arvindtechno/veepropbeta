/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.coretechies.veepropbeta.domain;

import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.constraints.NotBlank;

/**
 *
 * @author Noppp
 */
@Entity
@Table(name="announcement")
@Access(javax.persistence.AccessType.FIELD)
public class Announcement {
    
    @Id
    @GenericGenerator(name = "announcement", strategy = "increment")
    @GeneratedValue(generator = "announcement")
    private Integer announcementId;
    
    @Column
    @NotNull(message = "Please Select Announcement Type")
    private String announcementType;
    
    @Column
    @NotBlank(message = "Please Enter a Content")
    private String content;
    
    @Column
    private Integer createdBy;
     
    @Column
    @NotBlank(message = "Please Enter Heading")
    private String heading;

  
    
    @Column
    @Temporal(TemporalType.DATE)
    private Date creationDate;
    
    @Column
    @Temporal(TemporalType.DATE)
    @NotNull(message = "please Enter Expiry Date")
    private Date expiryDate;

    public Integer getAnnouncementId() {
        return announcementId;
    }

    public void setAnnouncementId(Integer announcementId) {
        this.announcementId = announcementId;
    }

    public String getAnnouncementType() {
        return announcementType;
    }

    public void setAnnouncementType(String announcementType) {
        this.announcementType = announcementType;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(Date expiryDate) {
        this.expiryDate = expiryDate;
    }
      public String getHeading() {
        return heading;
    }

    public void setHeading(String Heading) {
        this.heading = Heading;
    }

    @Override
    public String toString() {
        return "announcement{" + "announcementId=" + announcementId + ", announcementType=" + announcementType + ", content=" + content + ", createdBy=" + createdBy + ", creationDate=" + creationDate + ",Heading="+heading+", expiryDate=" + expiryDate + '}';
    }
    
    
}
