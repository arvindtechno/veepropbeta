/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.coretechies.veepropbeta.domain;

import java.sql.Time;
import java.util.Date;
import javax.persistence.*;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author Noppp
 */
@Entity
@Table(name="generationDetails")
@Access(AccessType.FIELD)
public class GenerationDetails {
    
    @Id
    @GenericGenerator(name = "generationDetails", strategy = "increment")
    @GeneratedValue(generator = "generationDetails")
    private Integer generationNumber;
    
    @Column
    @Temporal(TemporalType.DATE)
    private Date releaseDate;
    
    @Column
    @Temporal(TemporalType.TIME)
    private Date releaseTime;
    
    @Column
    @Temporal(TemporalType.DATE)
    private Date endDate;
    
    @Column
    @Temporal(TemporalType.TIME)
    private Date endTime;

    public Integer getGenerationNumber() {
        return generationNumber;
    }

    public void setGenerationNumber(Integer generationNumber) {
        this.generationNumber = generationNumber;
    }

    public Date getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(Date releaseDate) {
        this.releaseDate = releaseDate;
    }

    public Date getReleaseTime() {
        return releaseTime;
    }

    public void setReleaseTime(Date releaseTime) {
        this.releaseTime = releaseTime;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    @Override
    public String toString() {
        return "generationDetails{" + "generationNumber=" + generationNumber + ", releaseDate=" + releaseDate + ", releaseTime=" + releaseTime + ", endDate=" + endDate + ", endTime=" + endTime + '}';
    }
    
}
