/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.coretechies.veepropbeta.domain;

import java.sql.Time;
import java.util.Date;
import javax.persistence.*;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author Noppp
 */
@Entity
@Table(name = "lotdetails")
@Access(AccessType.FIELD)
public class LotDetails {
    
    @Id
    @GenericGenerator(name = "lotdetails",strategy = "increment")
    @GeneratedValue(generator = "lotdetails")
    private Integer lotNumber;
    
    @Column
    private Float pricePerShare;
    
    @Column
    private Integer numberOfShare;
    
    @Column
    @Temporal(TemporalType.DATE)
    private Date releaseDate;
    
    @Column
    @Temporal(TemporalType.TIME)
    private Date releaseTime;
    
    @Column
    @Temporal(TemporalType.DATE)
    private Date endDate;
    
    @Column
    @Temporal(TemporalType.TIME)
    private Date endTime;    

    @Column
    private Integer generationNumber;

    public Integer getLotNumber() {
        return lotNumber;
    }

    public void setLotNumber(Integer lotNumber) {
        this.lotNumber = lotNumber;
    }

    public Float getPricePerShare() {
        return pricePerShare;
    }

    public void setPricePerShare(Float pricePerShare) {
        this.pricePerShare = pricePerShare;
    }

    public Integer getNumberOfShare() {
        return numberOfShare;
    }

    public void setNumberOfShare(Integer numberOfShare) {
        this.numberOfShare = numberOfShare;
    }

    public Date getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(Date releaseDate) {
        this.releaseDate = releaseDate;
    }

    public Date getReleaseTime() {
        return releaseTime;
    }

    public void setReleaseTime(Date releaseTime) {
        this.releaseTime = releaseTime;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public Integer getGenerationNumber() {
        return generationNumber;
    }

    public void setGenerationNumber(Integer generationNumber) {
        this.generationNumber = generationNumber;
    }

    @Override
    public String toString() {
        return "lotDetails{" + "lotNumber=" + lotNumber + ", pricePerShare=" + pricePerShare + ", numberOfShare=" + numberOfShare + ", releaseDate=" + releaseDate + ", releaseTime=" + releaseTime + ", endDate=" + endDate + ", endTime=" + endTime + ", generationNumber=" + generationNumber + '}';
    }
}
