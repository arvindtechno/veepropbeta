/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.coretechies.veepropbeta.domain;

import javax.persistence.*;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author Core Techies
 */
@Entity
@Table(name = "bonustransaction")
@Access(AccessType.FIELD)
public class BonusTransaction {
    
    @Id
    @GenericGenerator(name = "bonusTransaction", strategy = "increment")
    @GeneratedValue(generator = "bonusTransaction")
    private Integer bonusTransactionId;
    
    @Column
    private Integer bonusTransactionToken;  //foreign key with transaction table
    
    @Column
    private Integer bonusType;

    public Integer getBonusTransactionId() {
        return bonusTransactionId;
    }

    public void setBonusTransactionId(Integer bonusTransactionId) {
        this.bonusTransactionId = bonusTransactionId;
    }

    public Integer getBonusTransactionToken() {
        return bonusTransactionToken;
    }

    public void setBonusTransactionToken(Integer bonusTransactionToken) {
        this.bonusTransactionToken = bonusTransactionToken;
    }

    public Integer getBonusType() {
        return bonusType;
    }

    public void setBonusType(Integer bonusType) {
        this.bonusType = bonusType;
    }

    @Override
    public String toString() {
        return "bonusTransaction{" + "bonusTransactionId=" + bonusTransactionId + ", bonusTransactionToken=" + bonusTransactionToken + ", bonusType=" + bonusType + '}';
    }
    
    
}
