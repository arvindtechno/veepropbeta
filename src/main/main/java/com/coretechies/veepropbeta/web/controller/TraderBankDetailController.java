/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.coretechies.veepropbeta.web.controller;

import com.coretechies.veepropbeta.domain.Login;
import com.coretechies.veepropbeta.domain.Notification;
import com.coretechies.veepropbeta.domain.UserBankDetail;
import com.coretechies.veepropbeta.service.notification.NotificationService;
import com.coretechies.veepropbeta.service.userbankdetail.UserBankDetailService;
import com.coretechies.veepropbeta.web.controller.algorithm.UserRole;
import com.coretechies.veepropbeta.web.controller.webdomain.Bank;
import java.util.List;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author Arvind
 */
@Controller
@SessionAttributes("user")
public class TraderBankDetailController {
  private final Logger logger =LoggerFactory.getLogger(getClass());
    
   @Autowired
   private UserBankDetailService bankDeatilService;
   
   @Autowired
   NotificationService notificationService;
   

    

    
    @RequestMapping(value = "/updatebankinfo",method = RequestMethod.GET)
    public ModelAndView updateDate(@ModelAttribute("updatebankinfo")Bank userbankdetail,ModelMap model){
        Login user=(Login) model.get("user");
        if(user != null && user.getUserRole().equals(UserRole.TRADER.getUserRoleValue())){
            List<Notification> notification = notificationService.retreiveNotification(user.getIdentificationNumber());
            model.addAttribute("notification", notification);             
            UserBankDetail usrbank=bankDeatilService.retriveBank(user.getIdentificationNumber());
            userbankdetail.setBankName(usrbank.getBankName());
            userbankdetail.setAccountNumber(usrbank.getAccountNumber());
            userbankdetail.setiFSC(usrbank.getiFSC());
            userbankdetail.setAccountType(usrbank.getAccountType());
            userbankdetail.setBankAddress(usrbank.getBankAddress());
            userbankdetail.setBranch(usrbank.getBranch());
            userbankdetail.setSwift(usrbank.getSwift());
            return new ModelAndView("updatebankinfo");
        }
        return new ModelAndView("redirect:/login");
     }
       
    @RequestMapping(value = "/updatebankinfo",method = RequestMethod.POST)
    public ModelAndView SaveChange(@Valid @ModelAttribute("updatebankinfo")Bank userbankdetail,BindingResult result,ModelMap model){
        Login user=(Login) model.get("user");
        if(user != null && user.getUserRole().equals(UserRole.TRADER.getUserRoleValue())){ 
            if(result.hasErrors()){
                return  new  ModelAndView("updatebankinfo");
            }
            UserBankDetail usrbank=bankDeatilService.retriveBank(user.getIdentificationNumber());
            usrbank.setAccountNumber(userbankdetail.getAccountNumber());
            usrbank.setBankName(userbankdetail.getBankName());
            usrbank.setiFSC(userbankdetail.getiFSC());
            usrbank.setAccountType(userbankdetail.getAccountType());
            usrbank.setBankAddress(userbankdetail.getBankAddress());
            usrbank.setBranch(userbankdetail.getBranch());
            usrbank.setSwift(userbankdetail.getSwift());
            bankDeatilService.updateBankDetail(usrbank);
            return new ModelAndView("redirect:/profile");
        }
        return new ModelAndView("redirect:/login");
    }
      
    
}
