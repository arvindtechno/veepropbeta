/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.coretechies.veepropbeta.web.controller;

import com.coretechies.veepropbeta.web.controller.algorithm.UserRole;
import com.coretechies.veepropbeta.domain.Login;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author Noppp
 */
@Controller
@SessionAttributes({"user"})
public class CompanyController {
    
    private final Logger logger = LoggerFactory.getLogger(getClass());
    
    @RequestMapping(value = "/launchipo", method = RequestMethod.GET)
    public ModelAndView launchIPO(ModelMap model){
        Login user = (Login)model.get("user");
        if(user != null){
            if(user.getUserRole().equals(UserRole.SUPERADMIN.getUserRoleValue())){
                
            }
        }
        return new ModelAndView("redirect:/login");
    }
}
