/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.coretechies.veepropbeta.web.controller.webdomain;

import java.util.Date;

/**
 *
 * @author Noppp
 */
public class PendingVeeCreditTransaction {
 
    private String sellerName;
    
    private Integer sellerIn;
    
    private Integer buyerIn;
    
    private String buyerName;
    
    private Integer transactionId;
    
    private Double veeCredit;
    
    private Date transactionDate;
    
    private String transactionStatus;

    @Override
    public String toString() {
        return "PendingVeeCreditTransaction{" + "sellerName=" + sellerName + ", sellerIn=" + sellerIn + ", buyerIn=" + buyerIn + ", buyerName=" + buyerName + ", transactionId=" + transactionId + ", veeCredit=" + veeCredit + ", transactionDate=" + transactionDate + ", transactionStatus=" + transactionStatus + '}';
    }

    public String getSellerName() {
        return sellerName;
    }

    public void setSellerName(String sellerName) {
        this.sellerName = sellerName;
    }

    public Integer getSellerIn() {
        return sellerIn;
    }

    public void setSellerIn(Integer sellerIn) {
        this.sellerIn = sellerIn;
    }

    public Integer getBuyerIn() {
        return buyerIn;
    }

    public void setBuyerIn(Integer buyerIn) {
        this.buyerIn = buyerIn;
    }

    public String getBuyerName() {
        return buyerName;
    }

    public void setBuyerName(String buyerName) {
        this.buyerName = buyerName;
    }

    public Integer getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(Integer transactionId) {
        this.transactionId = transactionId;
    }

    public Double getVeeCredit() {
        return veeCredit;
    }

    public void setVeeCredit(Double veeCredit) {
        this.veeCredit = veeCredit;
    }

    public Date getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(Date transactionDate) {
        this.transactionDate = transactionDate;
    }

    public String getTransactionStatus() {
        return transactionStatus;
    }

    public void setTransactionStatus(String transactionStatus) {
        this.transactionStatus = transactionStatus;
    }
    
    
}
