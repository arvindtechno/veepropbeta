/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.coretechies.veepropbeta.web.controller.algorithm;

/**
 *
 * @author Noppp
 */
public class ERentalDecide {
    
    private int ERentalDecided;

    public int getERentalDecided() {
        return ERentalDecided;
    }
    
    public ERentalDecide(Integer level) {
        if(level == 1){
            this.ERentalDecided=0;
        }else if(level == 2){
            this.ERentalDecided=0;
        }else if(level == 3){
            this.ERentalDecided=1;
        }else if(level == 4){
            this.ERentalDecided=2;
        }else if(level == 5){
            this.ERentalDecided=3;
        }else if(level == 6){
            this.ERentalDecided=4;
        }else if(level == 7){
            this.ERentalDecided=5;
        }else if(level == 8){
            this.ERentalDecided=6;
        }
    }
}
