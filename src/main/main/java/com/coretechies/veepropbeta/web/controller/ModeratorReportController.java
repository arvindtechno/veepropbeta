/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.coretechies.veepropbeta.web.controller;

import com.coretechies.veepropbeta.domain.BonusTransaction;
import com.coretechies.veepropbeta.domain.Login;
import com.coretechies.veepropbeta.domain.LotDetails;
import com.coretechies.veepropbeta.domain.ProfileChangeRequest;
import com.coretechies.veepropbeta.domain.Transaction;
import com.coretechies.veepropbeta.domain.User;
import com.coretechies.veepropbeta.domain.UserAddressInfo;
import com.coretechies.veepropbeta.domain.VeeCreditTransaction;
import com.coretechies.veepropbeta.domain.VerificationModerator;
import com.coretechies.veepropbeta.service.bonustransaction.BonusTransactionService;
import com.coretechies.veepropbeta.service.login.LoginService;
import com.coretechies.veepropbeta.service.lotdetails.LotDetailService;
import com.coretechies.veepropbeta.service.profile.ProfileService;
import com.coretechies.veepropbeta.service.profilechangerequest.ProfileChangeRequestService;
import com.coretechies.veepropbeta.service.transaction.TransactionService;
import com.coretechies.veepropbeta.service.user.UserService;
import com.coretechies.veepropbeta.service.veecredittransaction.VeeCreditTransactionService;
import com.coretechies.veepropbeta.service.verificationmoderator.VerificationModeratorService;
import com.coretechies.veepropbeta.web.controller.algorithm.TransactionStatus;
import com.coretechies.veepropbeta.web.controller.algorithm.UserRole;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author Noppp
 */
@Controller
@SessionAttributes({"user"})
public class ModeratorReportController {
    
    private final Logger logger = LoggerFactory.getLogger(getClass());
    
    @Autowired
    private UserService userService;
    
    @Autowired
    private LoginService loginService;
    
    @Autowired
    private ProfileService profileService;
    
    @Autowired
    private VerificationModeratorService verificationModeratorService;
    
    @Autowired
    private TransactionService transactionService;
    
    @Autowired
    private ProfileChangeRequestService profileChangeRequestService;
    
    @Autowired
    private VeeCreditTransactionService veeCreditTransactionService;
    
    @Autowired
    private BonusTransactionService bonusTransactionService;
    
    @Autowired
    private LotDetailService lotDetailService;
    
    @RequestMapping(value = "/moderator/report", method = RequestMethod.GET)
    public ModelAndView viewReportPage(ModelMap model){
        Login user = (Login)model.get("user");
        if(user != null && user.getUserRole().equals(UserRole.ADMIN.getUserRoleValue())){
            List<Integer> moderatorIN = loginService.retrieveUserByRole(UserRole.MODERATOR.getUserRoleValue());
            ArrayList<User> moderators = new ArrayList<User>();
            for(Integer i : moderatorIN){
                moderators.add(userService.retrieveUser(i));
            }
            model.addAttribute("moderators",moderators);
            return new ModelAndView("moderatorprofile");
        }
        return new ModelAndView("redirect:/login");
    }
    
    @RequestMapping(value = "/admin/{moderatorIN}/{report}", method = RequestMethod.GET)
    public 
    @ResponseBody
    ModelAndView moderatorReportGeneration(@PathVariable Integer moderatorIN,@PathVariable String report,
        HttpServletResponse response,HttpServletRequest request,ModelMap model) throws BadElementException, IOException{
        Login user = (Login)model.get("user");
        if(user != null && user.getUserRole().equals(UserRole.ADMIN.getUserRoleValue())){
            User moderator = userService.retrieveUser(moderatorIN);
            List<VerificationModerator> approvedTransactionByModerator = verificationModeratorService.retrieveVMApprovedByModerator(moderatorIN);
            
            ArrayList<Transaction> transactionList = new ArrayList<Transaction>();
            for(VerificationModerator vm : approvedTransactionByModerator){
                Transaction retrieveTransaction = transactionService.retrieveTransaction(vm.getTransactionId());
                VeeCreditTransaction retrieveVCTByTransaction = veeCreditTransactionService.retrieveVCTByTransaction(retrieveTransaction.getTransactionId());
                if(retrieveVCTByTransaction.getTransactionStatus().equals(TransactionStatus.COMPLETE.getTransactionStatusValue())){
                    transactionList.add(retrieveTransaction);
                }
            }
            
            PdfPTable table = null;
            if(report.equalsIgnoreCase("transaction")){
                table = new PdfPTable(5);
                
                PdfPCell cell1 = new PdfPCell(new Phrase("vCredit"));// we add a cell with colspan 3
                PdfPCell cell2 = new PdfPCell(new Phrase("TransactionDate"));
                PdfPCell cell3 = new PdfPCell(new Phrase("Seller"));
                PdfPCell cell4 = new PdfPCell(new Phrase("Buyer"));
                PdfPCell cell5 = new PdfPCell(new Phrase("TransactionID"));
                
                table.setWidthPercentage(100);

                cell1.setPadding(5);
                cell2.setPadding(5);
                cell3.setPadding(5);
                cell4.setPadding(5);
                cell5.setPadding(5);
                table.addCell(cell1);
                table.addCell(cell2);
                table.addCell(cell3);
                table.addCell(cell4);
                table.addCell(cell5);
                
                for(Transaction t : transactionList){
                    table.addCell(String.valueOf(t.getAmount()));
                    table.addCell(String.valueOf(t.getTransactionDate()));
                    table.addCell(String.valueOf(t.getSellerIN()));
                    table.addCell(String.valueOf(t.getBuyerIN()));
                    table.addCell(String.valueOf(t.getTransactionId()));
                }
                
            }else if(report.equalsIgnoreCase("profile")){
                table = new PdfPTable(5);
                
                PdfPCell cell1 = new PdfPCell(new Phrase("Name"));// we add a cell with colspan 3
                PdfPCell cell2 = new PdfPCell(new Phrase("Identification"));
                PdfPCell cell3 = new PdfPCell(new Phrase("Email"));
                PdfPCell cell4 = new PdfPCell(new Phrase("Contact"));
                PdfPCell cell5 = new PdfPCell(new Phrase("Verification Date"));
                
                table.setWidthPercentage(100);

                cell1.setPadding(5);
                cell2.setPadding(5);
                cell3.setPadding(5);
                cell4.setPadding(5);
                cell5.setPadding(5);
                table.addCell(cell1);
                table.addCell(cell2);
                table.addCell(cell3);
                table.addCell(cell4);
                table.addCell(cell5);
                
                List<ProfileChangeRequest> retieveAllApprovedRequest = profileChangeRequestService.retieveAllApprovedRequest(moderatorIN);
                
                ArrayList<ProfileChangeRequest> approvedProfiles = new ArrayList<ProfileChangeRequest>();
                
                for(ProfileChangeRequest pcr : approvedProfiles){
                    table.addCell(String.valueOf(pcr.getName()));
                    table.addCell(String.valueOf(pcr.getIdentificationNumber()));
                    table.addCell(String.valueOf(pcr.getEmail()));
                    table.addCell(String.valueOf(pcr.getContact()));
                    table.addCell(String.valueOf(pcr.getVerificationDate()));
                }
            }
            
            
            
            ServletContext servletContext = request.getSession().getServletContext();
            String absoluteFilesystemPath = servletContext.getRealPath("/");

            Image image1 = Image.getInstance(absoluteFilesystemPath+"\\img\\ReportLogo1.png");  

            Font font1 = new Font(Font.FontFamily.TIMES_ROMAN  ,15, Font.BOLDITALIC);
            Font font3 = new Font(Font.FontFamily.TIMES_ROMAN, 12,Font.BOLD);
            Font font4 = new Font(Font.FontFamily.TIMES_ROMAN, 10,Font.BOLDITALIC);

            UserAddressInfo retriveAddress = profileService.retriveAddress(moderatorIN);

            String name="Transaction history Approved by "+moderator.getName();
            Paragraph paragraph = new Paragraph(name.toUpperCase(),font1); 
            Paragraph paraName=new Paragraph(moderator.getName().toUpperCase(),font3);
            Paragraph paraAddress=new Paragraph(retriveAddress.getHouseNumber().toUpperCase()+","+retriveAddress.getStreetName1().toUpperCase(),font4);
            Paragraph paraCity=new Paragraph(retriveAddress.getCity().toUpperCase()+","+retriveAddress.getCountry().toUpperCase(),font4);
            Paragraph paragraph1=new Paragraph(moderator.getEmail().toLowerCase(),font3);

            paragraph.setAlignment(Element.ALIGN_RIGHT);
            paragraph.setIndentationRight(1);
            paragraph1.setAlignment(Element.ALIGN_RIGHT);
            paragraph1.setIndentationRight(1);
            paraName.setAlignment(Element.ALIGN_RIGHT);
            paraAddress.setAlignment(Element.ALIGN_RIGHT);
            paraAddress.setIndentationLeft(1);
            paraCity.setAlignment(Element.ALIGN_RIGHT);
            paraCity.setIndentationLeft(1);   
            try {
                    Document document = new Document();
                    PdfWriter.getInstance(document, response.getOutputStream());
                    document.open();
                    document.add(image1);
                    document.add(paragraph);
                    document.add(paraName);
                    document.add(paraAddress);
                    document.add(paraCity);
                    document.add(paragraph1);
                    document.add(table);
                    document.close();
               }catch (IOException e) {
               }catch (DocumentException e) {
               }
        }
        return new ModelAndView("redirect:/login");
    }

    @RequestMapping(value = "/admin/bonus/{report}", method = RequestMethod.GET)
    public 
    @ResponseBody
    ModelAndView bonusGeneration(@PathVariable Integer report,
        HttpServletResponse response,HttpServletRequest request,ModelMap model) 
            throws BadElementException, IOException{
        Login user = (Login)model.get("user");
        if(user != null && user.getUserRole().equals(UserRole.ADMIN.getUserRoleValue())){
                       
            PdfPTable table = new PdfPTable(3);

            PdfPCell cell1 = new PdfPCell(new Phrase("Name"));// we add a cell with colspan 3
            PdfPCell cell2 = new PdfPCell(new Phrase("Bonus"));
            PdfPCell cell3 = new PdfPCell(new Phrase("Date"));

            table.setWidthPercentage(100);

            cell1.setPadding(5);
            cell2.setPadding(5);
            cell3.setPadding(5);
            table.addCell(cell1);
            table.addCell(cell2);
            table.addCell(cell3);
            
            List<BonusTransaction> bonusTransactionList= bonusTransactionService.retrieveAllBonus(report);
            
            for(BonusTransaction bt : bonusTransactionList){
                Transaction retrieveTransaction = transactionService.retrieveTransaction(bt.getBonusTransactionToken());
                table.addCell(String.valueOf(userService.retrieveUser(retrieveTransaction.getSellerIN()).getName()));
                table.addCell(String.valueOf(retrieveTransaction.getAmount()));
                table.addCell(String.valueOf(retrieveTransaction.getTransactionDate()));    
            }
            
            ServletContext servletContext = request.getSession().getServletContext();
            String absoluteFilesystemPath = servletContext.getRealPath("/");

            Image image1 = Image.getInstance(absoluteFilesystemPath+"\\img\\ReportLogo1.png");  

            Font font1 = new Font(Font.FontFamily.TIMES_ROMAN  ,15, Font.BOLDITALIC);
            Font font3 = new Font(Font.FontFamily.TIMES_ROMAN, 12,Font.BOLD);
            Font font4 = new Font(Font.FontFamily.TIMES_ROMAN, 10,Font.BOLDITALIC);


            String name="Bonus history ";
            Paragraph paragraph = new Paragraph(name.toUpperCase(),font1); 
            
            paragraph.setAlignment(Element.ALIGN_RIGHT);
            paragraph.setIndentationRight(1);
            try {
                    Document document = new Document();
                    PdfWriter.getInstance(document, response.getOutputStream());
                    document.open();
                    document.add(image1);
                    document.add(paragraph);
                    document.add(table);
                    document.close();
               }catch (IOException e) {
               }catch (DocumentException e) {
               }
        }
        return new ModelAndView("redirect:/login");
    }

    @RequestMapping(value = "/system/report", method = RequestMethod.GET)
    public 
    @ResponseBody
    ModelAndView bonusGeneration(HttpServletResponse response,HttpServletRequest request,ModelMap model) 
            throws BadElementException, IOException{
        Login user = (Login)model.get("user");
        if(user != null && user.getUserRole().equals(UserRole.ADMIN.getUserRoleValue())){
            
            LotDetails lot = lotDetailService.retrieveLotDetail();
            List<Integer> trader = loginService.retrieveUserByRole(UserRole.TRADER.getUserRoleValue());
            List<Integer> moderator = loginService.retrieveUserByRole(UserRole.MODERATOR.getUserRoleValue());
            List<LotDetails> lotDetails  = lotDetailService.retrieveAllLotEntries();
            
            PdfPTable table = new PdfPTable(5);

            PdfPCell cell1 = new PdfPCell(new Phrase("Generation Number"));// we add a cell with colspan 3
            PdfPCell cell2 = new PdfPCell(new Phrase("Lot Number"));
            PdfPCell cell3 = new PdfPCell(new Phrase("Release Date"));
            PdfPCell cell4 = new PdfPCell(new Phrase("End Date Date"));
            PdfPCell cell5 = new PdfPCell(new Phrase("Share price"));

            table.setWidthPercentage(100);

            cell1.setPadding(5);
            cell2.setPadding(5);
            cell3.setPadding(5);
            cell4.setPadding(5);
            cell5.setPadding(5);
            table.addCell(cell1);
            table.addCell(cell2);
            table.addCell(cell3);
            table.addCell(cell4);
            table.addCell(cell5);
            
            for(LotDetails l : lotDetails){
                table.addCell(String.valueOf(l.getGenerationNumber()));
                table.addCell(String.valueOf(l.getLotNumber()%41));
                table.addCell(String.valueOf(l.getReleaseDate()));
                table.addCell(String.valueOf(l.getEndDate()));
                table.addCell(String.valueOf(l.getPricePerShare()));
            }
            
            ServletContext servletContext = request.getSession().getServletContext();
            String absoluteFilesystemPath = servletContext.getRealPath("/");

            Image image1 = Image.getInstance(absoluteFilesystemPath+"\\img\\ReportLogo1.png");  

            Font font1 = new Font(Font.FontFamily.TIMES_ROMAN  ,15, Font.BOLDITALIC);

            String name="System Status ";
            Paragraph paragraph = new Paragraph(name.toUpperCase(),font1);
            Paragraph p1 = new Paragraph("We are in Generation : "+lot.getGenerationNumber().toString(),font1); 
            Paragraph p2 = new Paragraph("Lot in Generation is : "+lot.getLotNumber().toString(),font1); 
            Paragraph p3 = new Paragraph("Number of share Available in this lot : "+lot.getNumberOfShare().toString(),font1); 
            Paragraph p4 = new Paragraph("Number of Trader : "+trader.size(),font1); 
            Paragraph p5 = new Paragraph("Number of Moderator : "+moderator.size(),font1); 
            Paragraph p6 = new Paragraph("Current Share Price : "+lot.getPricePerShare(),font1); 
            
            paragraph.setIndentationRight(1);
            p6.spacingAfter();
            
            try {
                    Document document = new Document();
                    PdfWriter.getInstance(document, response.getOutputStream());
                    document.open();
                    document.add(image1);
                    document.add(paragraph);
                    document.add(p1);
                    document.add(p2);
                    document.add(p3);
                    document.add(p4);
                    document.add(p5);
                    document.add(p6);
                    document.add(table);
                    document.close();
               }catch (IOException e) {
               }catch (DocumentException e) {
               }
        }
        return new ModelAndView("redirect:/login");
    }
}
