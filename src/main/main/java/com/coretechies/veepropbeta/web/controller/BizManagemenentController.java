/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.coretechies.veepropbeta.web.controller;

import com.coretechies.veepropbeta.web.controller.algorithm.*;
import com.coretechies.veepropbeta.domain.Login;
import com.coretechies.veepropbeta.domain.InGameTraderDetail;
import com.coretechies.veepropbeta.domain.LotDetails;
import com.coretechies.veepropbeta.domain.Notification;
import com.coretechies.veepropbeta.domain.ShareTradingTransaction;
import com.coretechies.veepropbeta.domain.TraderDynamicData;
import com.coretechies.veepropbeta.domain.TraderRelativeData;
import com.coretechies.veepropbeta.domain.Transaction;
import com.coretechies.veepropbeta.domain.User;
import com.coretechies.veepropbeta.domain.UserAddressInfo;
import com.coretechies.veepropbeta.domain.UserBankDetail;
import com.coretechies.veepropbeta.domain.VeeCreditTransaction;
import com.coretechies.veepropbeta.domain.VeeCreditTransactionType;import com.coretechies.veepropbeta.domain.VerificationModerator;
import com.coretechies.veepropbeta.service.calculateDRBandRB.CalculateDRBAndRBService;
import com.coretechies.veepropbeta.service.calculationmatchingBonus.CalculateMatchingBonusService;
import com.coretechies.veepropbeta.service.directreferaldecide.DirectReferalDecideService;
import com.coretechies.veepropbeta.service.ingamecreditdetails.InGameCreditDetailsService;
import com.coretechies.veepropbeta.service.login.LoginService;
import com.coretechies.veepropbeta.service.lotdetails.LotDetailService;
import com.coretechies.veepropbeta.service.notification.NotificationService;
import com.coretechies.veepropbeta.service.profile.ProfileService;
import com.coretechies.veepropbeta.service.sharetradingtransaction.ShareTradingTransactionService;
import com.coretechies.veepropbeta.service.traderdynamicdata.TraderDynamicDataService;
import com.coretechies.veepropbeta.service.traderrelativedata.TraderRelativeDataService;
import com.coretechies.veepropbeta.service.transaction.TransactionService;
import com.coretechies.veepropbeta.service.user.UserService;
import com.coretechies.veepropbeta.service.userbankdetail.UserBankDetailService;
import com.coretechies.veepropbeta.service.veecredittransaction.VeeCreditTransactionService;
import com.coretechies.veepropbeta.service.verificationmoderator.VerificationModeratorService;
import com.coretechies.veepropbeta.web.controller.webdomain.Registration;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.slf4j.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import java.util.Date;
import java.util.Random;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.validation.Valid;
import org.apache.commons.lang.RandomStringUtils;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.validation.BindingResult;

/**
 *
 * @author Noppp
 */
@Controller
@SessionAttributes({"user"})
public class BizManagemenentController {
    
    private final Logger logger = LoggerFactory.getLogger(getClass());
    
    private static final int ID_LENTH = 10;
    
    @InitBinder
    public void initBinder(WebDataBinder webDataBinder) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        dateFormat.setLenient(false);
        webDataBinder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
    }
    
    @Autowired
    private TraderRelativeDataService traderRelativeDaoService;
    
    @Autowired
    private ShareTradingTransactionService shareTradingTransactionService;
    
    @Autowired
    private UserService userService;
    
    @Autowired
    private LoginService loginService;
    
    @Autowired
    private InGameCreditDetailsService inGameCreditDetailsService;
    
    @Autowired
    private TraderDynamicDataService traderDynamicDataService;
    
    @Autowired
    private TransactionService transactionService;
    
    @Autowired
    private VeeCreditTransactionService veeCreditTransactionService;
    
    @Autowired
    private DirectReferalDecideService directReferalDecideService;
    
    @Autowired
    private CalculateMatchingBonusService calculateMatchingBonusService;
    
    @Autowired
    private CalculateDRBAndRBService calculateDRBAndRBService;
    
    @Autowired
    private LotDetailService lotdetailservice;
    
    @Autowired
    private VerificationModeratorService verificationModeratorService;
    
    @Autowired
    private NotificationService notificationService;
    
    @Autowired
    private UserBankDetailService userBankDetailService;
    
    @Autowired
    private ProfileService profileService;
    
    @Autowired
    private JavaMailSender mailSender;
    
    @Autowired
    private SimpleMailMessage simpleMailMessage;    
    
    @RequestMapping(value = "/business", method = RequestMethod.GET)
    public ModelAndView showBusiness(ModelMap model){
        Login user = (Login)model.get("user");
        if(user != null){
            if(user.getUserRole().equals(UserRole.TRADER.getUserRoleValue())){
                List<Notification> notification = notificationService.retreiveNotification(user.getIdentificationNumber());
                model.addAttribute("notification", notification);                
                Long traderCount = loginService.countUser(UserRole.TRADER.getUserRoleValue());
                model.addAttribute("traderCount", traderCount);
                Integer referalParent = traderRelativeDaoService.retrieveReferalParent(user.getIdentificationNumber());
                if(referalParent!=0){
                    User referalParentInfo = userService.retrieveUser(referalParent);
                    model.addAttribute("referalParentName", referalParentInfo.getName());
                }else{
                    model.addAttribute("referalParentName","Company");
                }
                InGameTraderDetail userGameDetails = inGameCreditDetailsService.retrieveIGCD(user.getIdentificationNumber());
                model.addAttribute("userLevel",userGameDetails.getLevelIN());
                return new ModelAndView("business");
            }else{
                return new ModelAndView("redirect:/login");
            }
        }        
        return new ModelAndView("redirect:/login");
    }
    @RequestMapping(value = "/introduce", method = RequestMethod.GET)
    public ModelAndView introduceReader(ModelMap model){
        
        Login user = (Login)model.get("user");
        if(user != null){
            if(user.getUserRole().equals(UserRole.TRADER.getUserRoleValue())){                
                List<Notification> notification = notificationService.retreiveNotification(user.getIdentificationNumber());
                model.addAttribute("notification", notification);                
                User userInfo = userService.retrieveUser(user.getIdentificationNumber());
                InGameTraderDetail IGCD = inGameCreditDetailsService.retrieveIGCD(user.getIdentificationNumber());
                model.addAttribute("updateCredit",IGCD.getvCredit());
                model.addAttribute("updateIGC",IGCD.getIGC());
                model.addAttribute("TotalShare",IGCD.getShareHold());
                logger.info("user is"+user.getUserName());
                model.addAttribute("userInfo",userInfo);                
                return new ModelAndView("tree");
            }else if(user.getUserRole().equals(UserRole.MODERATOR.getUserRoleValue())){
                model.addAttribute("name",userService.retrieveUser(user.getIdentificationNumber()).getName());
                User userInfo = userService.retrieveUser(1);
                model.addAttribute("userInfo",userInfo);
                return new ModelAndView("moderatortree");
            }
        }
        return new ModelAndView("redirect:/login");
    }
    
    @RequestMapping(value = "/retrieveChild/{identificationNumber}", method = RequestMethod.GET)
    public
    @ResponseBody
    List<String> retrieveChild(@PathVariable Integer identificationNumber,ModelMap model){

        List<TraderRelativeData> child = traderRelativeDaoService.retrieveChild(identificationNumber);
        logger.info("child is"+child);
        ArrayList<String> childToParent = new ArrayList<String>();  
        if(!child.isEmpty()){
            if(child.size()==2){
                for(TraderRelativeData IN: child){
                    User childFound = userService.retrieveUser(IN.getIdentificationNumber());
                    logger.info("child is /n"+childFound);
                    childToParent.add(childFound.getIdentificationNumber().toString());
                    childToParent.add(childFound.getName());
                }
            }else if(child.size()==1){
                for(TraderRelativeData IN: child){
                    if(IN.getpTP()==0){
                        User childFound = userService.retrieveUser(IN.getIdentificationNumber());
                        logger.info("child is /n"+childFound);
                        childToParent.add(childFound.getIdentificationNumber().toString());
                        childToParent.add(childFound.getName());
                    }else{
                        User childFound = userService.retrieveUser(IN.getIdentificationNumber());
                        logger.info("child is /n"+childFound);
                        childToParent.add(null);
                        childToParent.add(null);
                        childToParent.add(childFound.getIdentificationNumber().toString());
                        childToParent.add(childFound.getName());
                    }
                }
            }
            return childToParent;
        }else{
            return null;
        }

    }
    
    @RequestMapping(value = "/registerChild/{binaryParent}/{PTP}", method = RequestMethod.GET)
    public ModelAndView registrationPage(@ModelAttribute("registration") Registration registration,
            @PathVariable Integer binaryParent,@PathVariable Integer PTP,ModelMap model){
        Login user = (Login)model.get("user");
        if(user != null){
            if(user.getUserRole().equals(UserRole.TRADER.getUserRoleValue())){
                List<Notification> notification = notificationService.retreiveNotification(user.getIdentificationNumber());
                model.addAttribute("notification", notification);                
                InGameTraderDetail IGCD = inGameCreditDetailsService.retrieveIGCD(user.getIdentificationNumber());
                logger.info("V Crdit of "+user.getUserName()+" is "+IGCD.getvCredit());
                if(PTP == 0 || PTP == 1){
                    model.addAttribute("vCreditMax", IGCD.getvCredit());
                    registration.setBP(binaryParent);
                    registration.setRP(user.getIdentificationNumber());
                    registration.setPTP(PTP);
                    return new ModelAndView("register",model);
                }else{
                    return null;
                }
            }
        }
        return new ModelAndView("redirect:/login");
    }
    
    @RequestMapping(value = "/registerChild", method = RequestMethod.POST)
    public String registerTrader(@Valid @ModelAttribute("registration") Registration registration,
            BindingResult error,ModelMap model){
        
        Login user = (Login)model.get("user");
        
        if(user != null && user.getUserRole().equals(UserRole.TRADER.getUserRoleValue())){
            
            if(error.hasErrors()){
                List<Notification> notifications = notificationService.retreiveNotification(user.getIdentificationNumber());
                model.addAttribute("notification", notifications);
                logger.info("error in register is:"+error);
                InGameTraderDetail IGCD = inGameCreditDetailsService.retrieveIGCD(user.getIdentificationNumber());
                model.addAttribute("vCreditMax", IGCD.getvCredit());
                registration.setBP(registration.getBP());
                registration.setRP(user.getIdentificationNumber());
                registration.setPTP(registration.getPTP());
                return "register";
            }
            if(inGameCreditDetailsService.retrieveIGCD(user.getIdentificationNumber()).getvCredit() >= registration.getvCredit()){
                List<Notification> notifications = notificationService.retreiveNotification(user.getIdentificationNumber());
                model.addAttribute("notification", notifications);

                LevelDecide ld = new LevelDecide((double)registration.getvCredit());

                InGameTraderDetail retrieveIGCD = inGameCreditDetailsService.retrieveIGCD(user.getIdentificationNumber());
                retrieveIGCD.setvCredit(retrieveIGCD.getvCredit()-registration.getvCredit());
                InGameTraderDetail DRD = directReferalDecideService.DirectReferalDecide(ld.getLevelDecided(), retrieveIGCD);
                retrieveIGCD = DRD;
                inGameCreditDetailsService.updateIGTD(retrieveIGCD);

                User trader = new User();
                trader.setName(registration.getName());
                trader.setEmail(registration.getEmail());
                trader.setContact(registration.getContact());
                trader.setGender(registration.getGender());
                trader.setRegistrationDate(new Date());
                trader.setResidentCountry(registration.getResidentCountry());
                trader.setPassportNo(registration.getPassportNo());
                trader.setUserStatus(UserStatus.Active.getUserStatusValue());

                userService.saveUser(trader);

                UserBankDetail userBankDetail = new UserBankDetail();

                UserAddressInfo userAddressInfo = new UserAddressInfo();
                userAddressInfo.setIdentificationNumber(trader.getIdentificationNumber());

                profileService.addAddresssInfo(userAddressInfo);

                userBankDetailService.addBankDeatil(userBankDetail, trader.getIdentificationNumber());

                TraderRelativeData trd = new TraderRelativeData();
                trd.setbP(registration.getBP());
                trd.setrP(user.getIdentificationNumber());
                trd.setpTP(registration.getPTP());
                trd.setIdentificationNumber(trader.getIdentificationNumber());

                traderRelativeDaoService.saveTRD(trd);

                TraderDynamicData tdd = new TraderDynamicData();
                tdd.setIdentificationNumber(trader.getIdentificationNumber());
                tdd.setMatchingBonusPaid(0);
                tdd.setMoneyOnLeft(0);
                tdd.setMoneyOnRight(0);
                tdd.setPackageAmount(0.0);

                traderDynamicDataService.saveTDD(tdd);

                InGameTraderDetail igtd = new InGameTraderDetail();
                igtd.setACP(0.0);
                igtd.setAERG(0);
                igtd.setDMB(0.00);
                igtd.setDR1000(0);
                igtd.setDR2000(0);
                igtd.setDR5000(0);
                igtd.setDRD(0);
                igtd.setDRG(0);
                igtd.setDRS(0);
                igtd.setIGC(0.0);
                igtd.setIdentificationNumber(trader.getIdentificationNumber());
                igtd.setLTD(new Date());

                igtd.setLevelIN(UserLevel.NONE.getUserLevelValue());
                igtd.setShareHold(0);
                igtd.setTitle(UserTitle.NONE.getUserTitleValue());
                igtd.setvCredit((double)registration.getvCredit());

                inGameCreditDetailsService.saveIGCD(igtd);

                String password = RandomStringUtils.randomAlphanumeric(ID_LENTH);
                Login log = new Login();
                log.setUserName(registration.getLoginId());
                log.setUserRole(UserRole.TRADER.getUserRoleValue());
                log.setIdentificationNumber(trader.getIdentificationNumber());
                log.setPassword(password);

                loginService.saveTraderCredentials(log);

                String Subject = "Registered SuccessFully!";
                String Content = "Dear" + " " + trader.getName() + "\n" + 
                        "You are registered successfully!" + "\n Your UserName: " +
                        log.getUserName() + "\n" + "your Password:" + password +"\n visit localhost:8080\\veepropbeta";


                MimeMessage message = mailSender.createMimeMessage();   

                try {

                    MimeMessageHelper helper = new MimeMessageHelper(message, false);
                    helper.setFrom(simpleMailMessage.getFrom());
                    helper.setTo(trader.getEmail());
                    helper.setSubject(Subject);
                    helper.setText(Content);

                } catch (MessagingException e) {
                    //throw new MailParseException(e);
                    logger.error(Arrays.toString(e.getStackTrace()));
                }

                //mailSender.send(message);        

                Transaction transaction = new Transaction();
                transaction.setAmount((double)registration.getvCredit());
                transaction.setBuyerIN(trader.getIdentificationNumber());
                transaction.setSellerIN(user.getIdentificationNumber());
                transaction.setTransactionDate(new Date());
                transaction.setTransactionTime(new Date(System.currentTimeMillis()));
                transaction.setTransactionType(TransactionType.VEECREDITTRANSACTION.getTransactionTypeValue());

                transactionService.saveTransaction(transaction);

                VeeCreditTransaction vct = new VeeCreditTransaction();
                vct.setVeeCreditTransactionToken(transaction.getTransactionId());
                vct.setTransactionStatus(TransactionStatus.COMPLETE.getTransactionStatusValue());
                vct.setVeeCredit((double)registration.getvCredit());
                vct.setVeeCreditTransactionType(VeeCreditTransactionType.FROMTRADER.getVeeCreditTransactionTypeValue());
                vct.setRemark("Referal time Transaction");

                veeCreditTransactionService.saveVCT(vct);

                Notification notification = new Notification();
                notification.setIdentificationNumber(user.getIdentificationNumber());
                notification.setNotificationDate(new Date());
                //false is nuread and true is read
                notification.setNotificationStatus(Boolean.FALSE);
                notification.setNotification("You have introduced a user");
                String link = "/veepropbeta/notification/"+transaction.getTransactionId()+"/"+transaction.getTransactionType();
                notification.setNotificationLink(link);
                notificationService.saveNotification(notification);



                //calculateDRBAndRBService.calculateDRB(retrieveIGCD,registration.getvCredit(),10.0,CommisionType.DRB.getCommisionTypeValue());

                //calculateMatchingBonusService.calculateMatchingBonus(registration.getBP(),registration.getvCredit(),registration.getPTP());
            }else{
                model.addAttribute("insuff","In Sufficient Fund");
                return "redirect:/registerChild/"+registration.getBP()+"/"+registration.getPTP();
            }
        }
        return "redirect:/login";
    }
    
    @RequestMapping(value="/ingamedetailconvert" ,method = RequestMethod.GET)
    public ModelAndView convertIGC(@ModelAttribute("convert")Registration GameDetail,ModelMap model ){
        Login user = (Login)model.get("user");
        if(user != null && user.getUserRole().equals(UserRole.TRADER.getUserRoleValue())){
            List<Notification> notifications = notificationService.retreiveNotification(user.getIdentificationNumber());
            model.addAttribute("notification", notifications);            
            InGameTraderDetail IGCD = inGameCreditDetailsService.retrieveIGCD(user.getIdentificationNumber());
            model.addAttribute("vCreditMax", IGCD.getvCredit());
            return new ModelAndView("ingamedetailconvert");
        } 
        return new ModelAndView("redirect:/login");
    }
    
    @RequestMapping(value="/ingamedetailconvert", method =RequestMethod.POST)
    public ModelAndView saveConvertIGC(@ModelAttribute("convert")Registration GameDetail,ModelMap model){
        Login user = (Login)model.get("user");
        if(user != null && user.getUserRole().equals(UserRole.TRADER.getUserRoleValue())){
            List<Notification> notifications = notificationService.retreiveNotification(user.getIdentificationNumber());
            model.addAttribute("notification", notifications);              
            InGameTraderDetail IGCD = inGameCreditDetailsService.retrieveIGCD(user.getIdentificationNumber());
            inGameCreditDetailsService.updateConvertIGC(IGCD.getvCredit()-GameDetail.getvCredit(),GameDetail.getIGC()+IGCD.getIGC(),user.getIdentificationNumber());
            Integer RP = traderRelativeDaoService.retrieveReferalParent(user.getIdentificationNumber());
            TraderRelativeData BP = traderRelativeDaoService.retrieveBinaryParent(user.getIdentificationNumber());
            if(RP>0){
                InGameTraderDetail RPIGCD = inGameCreditDetailsService.retrieveIGCD(RP);
                calculateDRBAndRBService.calculateDRB(RPIGCD,GameDetail.getvCredit(),10.0,CommisionType.DRB.getCommisionTypeValue());                
            }
            if(BP.getbP() > 0 && BP.getpTP() != null){
                calculateMatchingBonusService.calculateMatchingBonus(BP.getbP(),GameDetail.getvCredit(),BP.getpTP());
            }
            
            TraderDynamicData tdd = traderDynamicDataService.retrieveTDD(user.getIdentificationNumber());
            tdd.setPackageAmount(tdd.getPackageAmount() + GameDetail.getvCredit());

            traderDynamicDataService.updateTDD(tdd);
            
            LevelDecide ld = new LevelDecide(tdd.getPackageAmount());
            InGameTraderDetail retrieveIGCD = inGameCreditDetailsService.retrieveIGCD(user.getIdentificationNumber());
            int oldLevel = retrieveIGCD.getLevelIN();
            retrieveIGCD.setLevelIN(ld.getLevelDecided());
            inGameCreditDetailsService.updateIGTD(retrieveIGCD);
            
            if(oldLevel < ld.getLevelDecided()){
                Notification notificationLevel = new Notification();
                notificationLevel.setIdentificationNumber(user.getIdentificationNumber());
                notificationLevel.setNotificationStatus(Boolean.FALSE);
                notificationLevel.setNotificationDate(new Date());
                String link = "/veepropbeta/notification/"+(-1)+"/"+0;
                notificationLevel.setNotificationLink(link);
                notificationLevel.setNotification("Level updated to "+ld.getLevelDecided());

                notificationService.saveNotification(notificationLevel);
            }            
            
        }
        return new ModelAndView("redirect:/introduce");
    }
    
    
    @RequestMapping(value="/sharepurchase",method = RequestMethod.GET)
    public ModelAndView showPurchase(@ModelAttribute("share")InGameTraderDetail GameDetail,ModelMap model){
        Login user=(Login) model.get("user");
        if(user != null && user.getUserRole().equals(UserRole.TRADER.getUserRoleValue())){
            List<Notification> notifications = notificationService.retreiveNotification(user.getIdentificationNumber());
            model.addAttribute("notification", notifications);
            
            InGameTraderDetail IGCD = inGameCreditDetailsService.retrieveIGCD(user.getIdentificationNumber());
            model.addAttribute("MaxIGC", IGCD.getIGC());
         
            LotDetails lot=lotdetailservice.retrieveLotDetail();
            model.addAttribute("priceRate", lot.getPricePerShare());
            return new ModelAndView("sharepurchase");
        }
        return new ModelAndView("redirect:/login");
    }
    
    @RequestMapping(value="/sharepurchase",method = RequestMethod.POST)
    public ModelAndView sharePurchase(@ModelAttribute("share")InGameTraderDetail GameDetail,ModelMap model){
        Login user=(Login) model.get("user");
        if(user != null && user.getUserRole().equals(UserRole.TRADER.getUserRoleValue())){
            
            LotDetails lot=lotdetailservice.retrieveLotDetail();
            logger.info("number of sjhare"+lot.getNumberOfShare());
            if(lot.getGenerationNumber()==1){
                if(lot.getNumberOfShare()>= GameDetail.getShareHold()){
                    InGameTraderDetail IGCD = inGameCreditDetailsService.retrieveIGCD(user.getIdentificationNumber());
                    inGameCreditDetailsService.purchaseShare(IGCD.getIGC()-GameDetail.getIGC(),IGCD.getShareHold()+GameDetail.getShareHold(),user.getIdentificationNumber());

                    lot.setNumberOfShare(lot.getNumberOfShare()-GameDetail.getShareHold());
                    lotdetailservice.updateShareCount(lot,GameDetail.getShareHold());

                    Transaction transaction = new Transaction();
                    transaction.setAmount(GameDetail.getIGC());
                    transaction.setBuyerIN(user.getIdentificationNumber());
                    transaction.setSellerIN(2);
                    transaction.setTransactionDate(new Date());
                    transaction.setTransactionTime(new Date(System.currentTimeMillis()));
                    transaction.setTransactionType(TransactionType.SHARETRADINGTRANSACTION.getTransactionTypeValue());

                    transactionService.saveTransaction(transaction);
                    
                    Notification notification = new Notification();
                    notification.setNotificationStatus(Boolean.FALSE);
                    notification.setNotificationDate(new Date());
                    notification.setIdentificationNumber(user.getIdentificationNumber());
                    String link = "/veepropbeta/notification/"+transaction.getTransactionId()+"/"+transaction.getTransactionType();
                    notification.setNotificationLink(link);
                    notification.setNotification("You have purchased "+GameDetail.getShareHold()+" share from Company");

                    notificationService.saveNotification(notification);
                    
                    ShareTradingTransaction shareTradingTransaction = new ShareTradingTransaction();
                    shareTradingTransaction.setCostPerShare(lot.getPricePerShare());
                    shareTradingTransaction.setNumberOfShare(GameDetail.getShareHold());
                    shareTradingTransaction.setShareTradingTransactionToken(transaction.getTransactionId());
                    shareTradingTransaction.setTransactionStatus(TransactionStatus.COMPLETE.getTransactionStatusValue());

                    shareTradingTransactionService.saveShareTrade(shareTradingTransaction);
                }else{
                    model.addAttribute("availableShare",lot.getNumberOfShare());
                    return new ModelAndView("redirect:/sharepurchase");
                }
            }else{
                // 1) here we will code to purchase from seller board 
                //2) this has been done in WalletController as /shareSole
            }
        }
        return new ModelAndView("redirect:/introduce");
    }
    
    @RequestMapping(value = "/sellshare", method = RequestMethod.GET)
    public ModelAndView showSellShare(@ModelAttribute("share")InGameTraderDetail GameDetail,ModelMap model){
        Login user= (Login) model.get("user");
        if(user!=null && user.getUserRole().equals(UserRole.TRADER.getUserRoleValue())){
            List<Notification> notifications = notificationService.retreiveNotification(user.getIdentificationNumber());
            model.addAttribute("notification", notifications);
            
            LotDetails lot = lotdetailservice.retrieveLotDetail();
            model.addAttribute("currentPrice", lot.getPricePerShare());
            InGameTraderDetail GameShareSell=inGameCreditDetailsService.retrieveIGCD(user.getIdentificationNumber());
            model.addAttribute("shareAvailable",GameShareSell.getShareHold());
            return new ModelAndView("sellshare");
            
        }
        return new ModelAndView("redirect:/login");
    }
    
    @RequestMapping(value = "/sellshare", method = RequestMethod.POST)
    public ModelAndView sellShare(@ModelAttribute("share")InGameTraderDetail GameDetail,ModelMap model){
        Login user= (Login) model.get("user");
        if(user!=null){
            LotDetails lot = lotdetailservice.retrieveLotDetail();
            if(lot.getGenerationNumber()>1){
                List<Integer> userList = loginService.retrieveUserByRole(UserRole.MODERATOR.getUserRoleValue());
                
                //generate random moderator
                Random rand = new Random();
                int randomNum = rand.nextInt(userList.size());
                Integer moderatorIdentificationNumber = userList.get(randomNum);
                
                InGameTraderDetail IGCD = inGameCreditDetailsService.retrieveIGCD(user.getIdentificationNumber());
                IGCD.setShareHold(IGCD.getShareHold()-GameDetail.getShareHold());
                
                inGameCreditDetailsService.updateIGTD(IGCD);
                
                Transaction transaction = new Transaction();
                transaction.setAmount((double)lot.getPricePerShare()* GameDetail.getShareHold());
                transaction.setSellerIN(user.getIdentificationNumber());
                transaction.setTransactionDate(new Date());
                transaction.setTransactionTime(new Date(System.currentTimeMillis()));
                transaction.setTransactionType(TransactionType.SHARETRADINGTRANSACTION.getTransactionTypeValue());

                transactionService.saveTransaction(transaction);

                ShareTradingTransaction shareTradingTransaction = new ShareTradingTransaction();
                shareTradingTransaction.setCostPerShare(lot.getPricePerShare());
                shareTradingTransaction.setNumberOfShare(GameDetail.getShareHold());
                shareTradingTransaction.setShareTradingTransactionToken(transaction.getTransactionId());
                shareTradingTransaction.setTransactionStatus(TransactionStatus.PENDING.getTransactionStatusValue());

                shareTradingTransactionService.saveShareTrade(shareTradingTransaction);

                VerificationModerator verificationModerator = new VerificationModerator();
                verificationModerator.setIdentificationNumber(moderatorIdentificationNumber);
                verificationModerator.setTransactionId(transaction.getTransactionId());
                verificationModerator.setComment("Share Sell Request");
                verificationModerator.setVerificationStatus(Boolean.FALSE);

                verificationModeratorService.saveVerificationModerator(verificationModerator);
                
                Notification notification = new Notification();
                notification.setNotificationStatus(Boolean.FALSE);
                notification.setNotificationDate(new Date());
                notification.setNotification("You have requested to sell "+GameDetail.getShareHold()+" share, You will get credit when your share will be sold out");
                String link = "/veepropbeta/notification/"+transaction.getTransactionId()+"/"+transaction.getTransactionType();
                notification.setNotificationLink(link);
                notification.setIdentificationNumber(user.getIdentificationNumber());
                notificationService.saveNotification(notification);
            }
            return new ModelAndView("redirect:/sellshare");
        }
        return new ModelAndView("redirect:/login");
    }

}
