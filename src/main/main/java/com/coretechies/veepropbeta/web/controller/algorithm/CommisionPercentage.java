/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.coretechies.veepropbeta.web.controller.algorithm;

/**
 *
 * @author Noppp
 */
public enum CommisionPercentage {
    
    LEVEL(0),
   
    ATLEVEL1(6),
    
    ATLEVEL2(6),
    
    ATLEVEL3(7),
    
    ATLEVEL4(8),
    
    ATLEVEL5(9),
    
    ATLEVEL6(10),
   
    ATLEVEL7(10),
    
    ATLEVEL8(10);
    
    private final int CommisionPercentageIs;
    
    private CommisionPercentage(int CommisionPercentageIs){
        this.CommisionPercentageIs = CommisionPercentageIs;
    }
    
    public int getCommisionPercentageIs(){
        return CommisionPercentageIs;
    }
    
    public int getCommisionPercentageByLevel(int userLevel){
        if(userLevel==UserLevel.LEVEL1.getUserLevelValue()){
            return ATLEVEL1.getCommisionPercentageIs();
        }else if(userLevel==UserLevel.LEVEL2.getUserLevelValue()){
            return ATLEVEL2.getCommisionPercentageIs();
        }else if(userLevel==UserLevel.LEVEL3.getUserLevelValue()){
            return ATLEVEL3.getCommisionPercentageIs();
        }else if(userLevel==UserLevel.LEVEL4.getUserLevelValue()){
            return ATLEVEL4.getCommisionPercentageIs();
        }else if(userLevel==UserLevel.LEVEL5.getUserLevelValue()){
            return ATLEVEL5.getCommisionPercentageIs();
        }else if(userLevel==UserLevel.LEVEL6.getUserLevelValue()){
            return ATLEVEL6.getCommisionPercentageIs();
        }else if(userLevel==UserLevel.LEVEL7.getUserLevelValue()){
            return ATLEVEL7.getCommisionPercentageIs();
        }else if(userLevel==UserLevel.LEVEL8.getUserLevelValue()){
            return ATLEVEL8.getCommisionPercentageIs();
        }else{
            return 0;
        }
    }
}
