/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.coretechies.veepropbeta.web.controller.algorithm;

/**
 *
 * @author Noppp
 */
public enum TransactionType {
    
    VEECREDITTRANSACTION(0),
    
    SHARETRADINGTRANSACTION(1),
    
    BONUSTRANSACTION(2);
    
    private final int TransactionTypeValue;
    
    private TransactionType(int TransactionTypeValue){
        this.TransactionTypeValue = TransactionTypeValue;
    }
    
    public int getTransactionTypeValue(){
        return TransactionTypeValue;
    }
}
