/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.coretechies.veepropbeta.web.controller;
import com.coretechies.veepropbeta.domain.Login;
import com.coretechies.veepropbeta.domain.Notification;
import com.coretechies.veepropbeta.domain.ProfileChangeRequest;
import com.coretechies.veepropbeta.domain.SecurityQuestion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import com.coretechies.veepropbeta.domain.User;
import com.coretechies.veepropbeta.domain.UserAddressInfo;
import com.coretechies.veepropbeta.domain.UserBankDetail;
import com.coretechies.veepropbeta.service.login.LoginService;
import com.coretechies.veepropbeta.service.notification.NotificationService;
import com.coretechies.veepropbeta.service.profile.ProfileService;
import com.coretechies.veepropbeta.service.profilechangerequest.ProfileChangeRequestService;
import com.coretechies.veepropbeta.service.user.UserService;
import com.coretechies.veepropbeta.web.controller.algorithm.UserRole;
import com.coretechies.veepropbeta.web.controller.algorithm.UserStatus;
import com.coretechies.veepropbeta.web.controller.webdomain.Address;
import com.coretechies.veepropbeta.web.controller.webdomain.ProfileUpgrade;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

        

/**
 *
 * @author Rohit
 */
@Controller
@SessionAttributes({"user"})
public class ProfileController{
    private final Logger logger=LoggerFactory.getLogger(getClass());
    
    @InitBinder
    public void initBinder(WebDataBinder webDataBinder) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        dateFormat.setLenient(false);
        webDataBinder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
    }
     
    
    @Autowired
    private ProfileService profileService;
    
    @Autowired
    private ProfileChangeRequestService pcrs;
    
    @Autowired
    private UserService userService;
    
    @Autowired
    private LoginService loginService;
    
    @Autowired
    NotificationService notificationService;
    
        @RequestMapping(value ="/profile",method = RequestMethod.GET)
        public ModelAndView showuserprofile(@ModelAttribute("profile")User usrp,ModelMap mdp){
            Login user=(Login)mdp.get("user");
            if(user != null && user.getUserRole().equals(UserRole.TRADER.getUserRoleValue())){
                List<Notification> notifications = notificationService.retreiveNotification(user.getIdentificationNumber());
                mdp.addAttribute("notification", notifications);                 
                User usr= profileService.retrivePersonal(user.getIdentificationNumber());
                mdp.addAttribute("us",usr);
                UserAddressInfo usradd=profileService.retriveAddress(user.getIdentificationNumber());
                mdp.addAttribute("usradd",usradd);            
                UserBankDetail usrbank=profileService.retriveBank(user.getIdentificationNumber());
                mdp.addAttribute("usrbank", usrbank);  
                return  new ModelAndView("profile");
            }
            return new ModelAndView("redirect:/login");
        } 
     
        @RequestMapping(value = "/updatepersonaldetail",method =RequestMethod.GET)
        public ModelAndView UpdateDeails(@ModelAttribute("updatepersonal")ProfileUpgrade trader,ModelMap model){
            Login user=(Login) model.get("user");
            if(user !=null && user.getUserRole().equals(UserRole.TRADER.getUserRoleValue())){
                List<Notification> notifications = notificationService.retreiveNotification(user.getIdentificationNumber());
                model.addAttribute("notification", notifications);                 
                User Trader =profileService.retrivePersonal(user.getIdentificationNumber());
                List<SecurityQuestion> securityQuestions = userService.retrieveSecurityQuestion();
                model.addAttribute("securityQuestions", securityQuestions);
                trader.setName(Trader.getName());
                trader.setEmail(Trader.getEmail());
                trader.setContact(Trader.getContact());
                trader.setFaxNo(Trader.getFaxNo());
                trader.setHomeTelNo(Trader.getHomeTelNo());
                trader.setOfficeTelNo(Trader.getOfficeTelNo());
                trader.setPassportNo(Trader.getPassportNo());
                trader.setScreenName(Trader.getScreenName());
                trader.setResidentCountry(Trader.getResidentCountry());
                trader.setSecurityQuestionId(Trader.getSecurityQuestionId());
                trader.setSecurityAnswer(Trader.getSecurityAnswer());
                trader.setGender(Trader.getGender());
                trader.setDateOfBirth(Trader.getDateOfBirth());
                trader.setImage(Trader.getImage());
                trader.setOfficeTelNo(Trader.getOfficeTelNo());
                trader.setScreenName(Trader.getScreenName());
                trader.setHomeTelNo(Trader.getHomeTelNo());
                trader.setFaxNo(Trader.getFaxNo());
                trader.setPassportNo(Trader.getPassportNo());
                trader.setResidentCountry(Trader.getResidentCountry());
                return new ModelAndView("updatepersonaldetail");
            }
            return new ModelAndView("redirect:/login");
        }
        
        @RequestMapping(value ="/updatepersonaldetail" ,method = RequestMethod.POST)
        public ModelAndView UpdateDatePersonal(@Valid @ModelAttribute("updatepersonal") ProfileUpgrade trader,BindingResult result,HttpServletRequest request,
                                              @RequestParam("file") MultipartFile file,ModelMap model) throws IOException{
            Login login=(Login)model.get("user");
            if(login !=null && login.getUserRole().equals(UserRole.TRADER.getUserRoleValue())){
                User Trader =profileService.retrivePersonal(login.getIdentificationNumber());
                if(result.hasErrors()){
                List<Notification> notifications = notificationService.retreiveNotification(login.getIdentificationNumber());
                model.addAttribute("notification", notifications);                 
                List<SecurityQuestion> securityQuestions = userService.retrieveSecurityQuestion();
                model.addAttribute("securityQuestions", securityQuestions);
                trader.setName(Trader.getName());
                trader.setEmail(Trader.getEmail());
                trader.setContact(Trader.getContact());
                trader.setFaxNo(Trader.getFaxNo());
                trader.setHomeTelNo(Trader.getHomeTelNo());
                trader.setOfficeTelNo(Trader.getOfficeTelNo());
                trader.setPassportNo(Trader.getPassportNo());
                trader.setScreenName(Trader.getScreenName());
                trader.setResidentCountry(Trader.getResidentCountry());
                trader.setSecurityQuestionId(Trader.getSecurityQuestionId());
                trader.setSecurityAnswer(Trader.getSecurityAnswer());
                trader.setGender(Trader.getGender());
                trader.setDateOfBirth(Trader.getDateOfBirth());
                trader.setImage(Trader.getImage());
                trader.setOfficeTelNo(Trader.getOfficeTelNo());
                trader.setScreenName(Trader.getScreenName());
                trader.setHomeTelNo(Trader.getHomeTelNo());
                trader.setFaxNo(Trader.getFaxNo());
                trader.setPassportNo(Trader.getPassportNo());
                trader.setResidentCountry(Trader.getResidentCountry());                    
                    return new ModelAndView("updatepersonaldetail");
                }
                ProfileChangeRequest profilechangerequest= pcrs.retievePCR(login.getIdentificationNumber());
                if(profilechangerequest==null){
                    profilechangerequest = new ProfileChangeRequest();
                    List<Integer> userList = loginService.retrieveUserByRole(UserRole.MODERATOR.getUserRoleValue());
                    //generate random moderator
                    Random rand = new Random();
                    int randomNum = rand.nextInt(userList.size());
                    Integer moderatorIdentificationNumber = userList.get(randomNum);  
                    profilechangerequest.setVerifiedBy(moderatorIdentificationNumber);
                }              
                
                profilechangerequest.setIdentificationNumber(login.getIdentificationNumber());
                profilechangerequest.setName(trader.getName());
                profilechangerequest.setEmail(trader.getEmail());
                profilechangerequest.setContact(trader.getContact());
                profilechangerequest.setSecurityQuestionId(trader.getSecurityQuestionId());
                profilechangerequest.setSecurityAnswer(trader.getSecurityAnswer());
                profilechangerequest.setGender(trader.getGender());

                profilechangerequest.setScreenName(trader.getScreenName());
                profilechangerequest.setHomeTelNo(trader.getHomeTelNo());
                profilechangerequest.setPassportNo(trader.getPassportNo());
                profilechangerequest.setOfficeTelNo(trader.getOfficeTelNo());
                profilechangerequest.setFaxNo(trader.getFaxNo());
                profilechangerequest.setResidentCountry(trader.getResidentCountry());
                profilechangerequest.setDateOfBirth(trader.getDateOfBirth());
                
                ServletContext servletContext = request.getSession().getServletContext();
                String absoluteFilesystemPath = servletContext.getRealPath("/");
                byte[] fileData = file.getBytes();
                String name=Trader.getImage();
;                if (fileData.length != 0) {
                    String fileName =absoluteFilesystemPath+"\\"+login.getUserName()+".jpeg";
                    name=login.getUserName()+".jpeg";
                    logger.info("hhhhhhhhhhhhhhhhh"+fileName);
                    FileOutputStream fileOutputStream = new FileOutputStream(fileName);
                    fileOutputStream.write(fileData);
                    fileOutputStream.close();
                   
                }
                 profilechangerequest.setImage(name);
                 

                profilechangerequest.setRequestStatus(Boolean.FALSE);
                profilechangerequest.setRequestDate(new Date());
                
                UserAddressInfo retriveAddress = profileService.retriveAddress(login.getIdentificationNumber());
                
                if(profilechangerequest.getCity()==null
                        &&  profilechangerequest.getCountry() == null
                        &&  profilechangerequest.getHouseNumber()==null
                        &&  profilechangerequest.getStreetName1() == null
                        &&  profilechangerequest.getStreetName2() == null
                        &&  profilechangerequest.getZipCode() == null
                        &&  profilechangerequest.getLandmark() == null
                        &&  profilechangerequest.getUserState() == null){
                    profilechangerequest.setCity(retriveAddress.getCity());
                    profilechangerequest.setHouseNumber(retriveAddress.getHouseNumber());
                    profilechangerequest.setStreetName1(retriveAddress.getStreetName1());
                    profilechangerequest.setStreetName2(retriveAddress.getStreetName2());
                    profilechangerequest.setCountry(retriveAddress.getCountry());
                    profilechangerequest.setUserState(retriveAddress.getUserstate());
                    profilechangerequest.setLandmark(retriveAddress.getLandmark());
                    profilechangerequest.setZipCode(retriveAddress.getZipcode());
                }
                pcrs.SavePCR(profilechangerequest);
                
                return new ModelAndView("redirect:/profile");
            }
            return new ModelAndView("redirect:/login");
        }
    
    
    
        @RequestMapping(value="/updatePersonalInformation",method = RequestMethod.GET)
        public ModelAndView showAddress(@ModelAttribute("showaddress") Address useraddressinfo,ModelMap model){
            Login user=(Login) model.get("user");
            if(user != null && user.getUserRole().equals(UserRole.TRADER.getUserRoleValue())){
                List<Notification> notifications = notificationService.retreiveNotification(user.getIdentificationNumber());
                model.addAttribute("notification", notifications);  
                
                UserAddressInfo usradd=profileService.retriveAddress(user.getIdentificationNumber());
                useraddressinfo.setCity(usradd.getCity());
                useraddressinfo.setHouseNumber(usradd.getHouseNumber());
                useraddressinfo.setStreetName1(usradd.getStreetName1());
                useraddressinfo.setStreetName2(usradd.getStreetName2());
                useraddressinfo.setLandmark(usradd.getLandmark());
                useraddressinfo.setZipcode(usradd.getZipcode());
                useraddressinfo.setCountry(usradd.getCountry());
                useraddressinfo.setUserstate(usradd.getUserstate());

                return new ModelAndView("updatePersonalInformation");
            }
            return new ModelAndView("redirect:/login");
        }
     
        @RequestMapping(value="/updatePersonalInformation",method = RequestMethod.POST)
        public ModelAndView addAddressInfo(@Valid @ModelAttribute("showaddress")Address useraddressinfo,BindingResult result,ModelMap model){
        Login user=(Login) model.get("user");
        if(user !=null && user.getUserRole().equals(UserRole.TRADER.getUserRoleValue())){
            if(result.hasErrors()){
                return  new ModelAndView("updatePersonalInformation");
            }
            
            ProfileChangeRequest profilechangerequest= pcrs.retievePCR(user.getIdentificationNumber());
            if(profilechangerequest==null){
                profilechangerequest = new ProfileChangeRequest();
                List<Integer> userList = loginService.retrieveUserByRole(UserRole.MODERATOR.getUserRoleValue());
                //generate random moderator
                Random rand = new Random();
                int randomNum = rand.nextInt(userList.size());
                Integer moderatorIdentificationNumber = userList.get(randomNum);  
                profilechangerequest.setVerifiedBy(moderatorIdentificationNumber);
            }  
            
            profilechangerequest.setIdentificationNumber(user.getIdentificationNumber());
            profilechangerequest.setHouseNumber(useraddressinfo.getHouseNumber());
            profilechangerequest.setStreetName1(useraddressinfo.getStreetName1());
            profilechangerequest.setStreetName2(useraddressinfo.getStreetName2());
            profilechangerequest.setCity(useraddressinfo.getCity());
            profilechangerequest.setLandmark(useraddressinfo.getLandmark());
            profilechangerequest.setCountry(useraddressinfo.getCountry());
            profilechangerequest.setUserState(useraddressinfo.getUserstate());
            profilechangerequest.setZipCode(useraddressinfo.getZipcode());
            profilechangerequest.setRequestStatus(Boolean.FALSE);
            profilechangerequest.setRequestDate(new Date());
            User retrivePersonal = profileService.retrivePersonal(user.getIdentificationNumber());
            if(profilechangerequest.getContact() == null
                    && profilechangerequest.getDateOfBirth() == null
                    && profilechangerequest.getEmail() == null
                    && profilechangerequest.getName()== null
                    && profilechangerequest.getSecurityAnswer() == null
                    && profilechangerequest.getSecurityQuestionId() == null){
                profilechangerequest.setContact(retrivePersonal.getContact());
                profilechangerequest.setDateOfBirth(retrivePersonal.getDateOfBirth());
                profilechangerequest.setEmail(retrivePersonal.getEmail());
                profilechangerequest.setName(retrivePersonal.getName());
                profilechangerequest.setGender(retrivePersonal.getGender());
                profilechangerequest.setSecurityAnswer(retrivePersonal.getSecurityAnswer());
                profilechangerequest.setSecurityQuestionId(retrivePersonal.getSecurityQuestionId());
            }
            pcrs.SavePCR(profilechangerequest);
            return new ModelAndView("redirect:/profile");
        }
        return new ModelAndView("redirect:/login"); 
     }
        
        @RequestMapping(value = "/trader/{str}", method = RequestMethod.GET)
        public ModelAndView showTradersEnries(@PathVariable String str,ModelMap model){
            Login user = (Login)model.get("user");
            if(user != null){
                ArrayList<User> userList = new ArrayList<User>();
                List<User> users = userService.retrieveUser(str);
                for(User u : users){
                    Login login = loginService.retrieveUser(u.getIdentificationNumber());
                    if(login.getUserRole().equals(UserRole.TRADER.getUserRoleValue())){
                        userList.add(u);
                    }
                }
                logger.info("no of user found is:"+userList.size());
                model.addAttribute("users",userList);
                return new ModelAndView("entries");
            }
            return new ModelAndView("redirect:/login");
        }
        
        @RequestMapping(value = "/trader/block/{id}", method = RequestMethod.GET)
        public ModelAndView blockTrader(@PathVariable Integer id, ModelMap model){
            Login user = (Login)model.get("user");
            if(user != null && !user.getUserRole().equals(UserRole.TRADER.getUserRoleValue())){
                User userGet = userService.retrieveUser(id);
                userGet.setUserStatus(UserStatus.InActive.getUserStatusValue());
                userService.updateUser(userGet);
            }
            return new ModelAndView("redirect:/login");
        }
 }
       
