/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.coretechies.veepropbeta.web.controller.algorithm;

/**
 *
 * @author Noppp
 */
public enum UserStatus {
    
    Active(0),
    
    InActive(1);
    
    private final int userStatusValue;

    private UserStatus(int userStatusValue) {
        this.userStatusValue = userStatusValue;
    }

    public int getUserStatusValue() {
        return userStatusValue;
    }
    
}
