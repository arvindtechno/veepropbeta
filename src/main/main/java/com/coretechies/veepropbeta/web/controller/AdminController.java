/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.coretechies.veepropbeta.web.controller;

import com.coretechies.veepropbeta.domain.Announcement;
import com.coretechies.veepropbeta.domain.ContactUs;
import com.coretechies.veepropbeta.domain.Faq;
import com.coretechies.veepropbeta.domain.Feedback;
import com.coretechies.veepropbeta.domain.Login;
import com.coretechies.veepropbeta.domain.User;
import com.coretechies.veepropbeta.domain.UserAddressInfo;
import com.coretechies.veepropbeta.service.announcement.announcementService;
import com.coretechies.veepropbeta.service.contact.ContactService;
import com.coretechies.veepropbeta.service.faq.FaqService;
import com.coretechies.veepropbeta.service.feedback.FeedBackService;
import com.coretechies.veepropbeta.service.login.LoginService;
import com.coretechies.veepropbeta.service.profile.ProfileService;
import com.coretechies.veepropbeta.service.user.UserService;
import com.coretechies.veepropbeta.web.controller.algorithm.UserRole;
import com.coretechies.veepropbeta.web.controller.algorithm.UserStatus;
import com.coretechies.veepropbeta.web.controller.webdomain.Registration;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.validation.Valid;
import org.apache.commons.lang.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author Arvind
 */
@Controller
@SessionAttributes("user")
public class AdminController {
     private final Logger logger =LoggerFactory.getLogger(getClass());
    
     @Autowired
     private FaqService faqservice;
     
     @Autowired
     private ContactService contactService;
     
     @Autowired
     private FeedBackService feedBackService;
     
     @Autowired
     private UserService userService;
     
     @Autowired
     private ProfileService profileService;
     
     @Autowired
     private LoginService loginService;
     
     @Autowired
     private announcementService announceService;
     
     private static final int ID_LENTH = 10;

    @InitBinder
    public void initBinder(WebDataBinder webDataBinder) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        dateFormat.setLenient(false);
        webDataBinder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
    }     
     
    @RequestMapping(value="/faqadmin" ,method = RequestMethod.GET)
     public ModelAndView showFaqForm(@ModelAttribute("faqForm") Faq faq, ModelMap model){
         Login user = (Login)model.get("user");
         if(user != null && user.getUserRole().equals(UserRole.ADMIN.getUserRoleValue())){
             if(user.getUserRole().equals(UserRole.ADMIN.getUserRoleValue())){
                model.addAttribute("userId", user.getIdentificationNumber());
                 return new ModelAndView("faqadmin");
             }
         }
         return new ModelAndView("redirect:/login");
     }
     
     @RequestMapping(value = "/faqadmin",method =RequestMethod.POST)
     public ModelAndView SaveDataFaq(@Valid @ModelAttribute("faqForm") Faq faq,BindingResult result,ModelMap model){
         Login user = (Login)model.get("user");
         if(user != null){
             if(user.getUserRole().equals(UserRole.ADMIN.getUserRoleValue())){
                 if(result.hasErrors()){
                     return new ModelAndView("faqadmin");
                 }
                faq.setAdminId(user.getIdentificationNumber());
                faqservice.saveFaqData(faq);
                return new ModelAndView("redirect:/faqadmin");
             }
         }
         return new ModelAndView("redirect:/login");
    }     

     @RequestMapping(value = "/contacts",method =RequestMethod.GET)
     public ModelAndView showContacts(ModelMap model){
         Login user = (Login)model.get("user");
         if(user != null && user.getUserRole().equals(UserRole.ADMIN.getUserRoleValue())){
             if(user.getUserRole().equals(UserRole.ADMIN.getUserRoleValue())){
                List<ContactUs> contactList = contactService.retrieveContacts();
                model.addAttribute("contactList",contactList);
                return new ModelAndView("contacts");
             }
         }
         return new ModelAndView("redirect:/login");
    }     
     
     @RequestMapping(value = "/delete/contact/{var}",method =RequestMethod.GET)
     public ModelAndView deleteContacts(@PathVariable Integer var,ModelMap model){
         Login user = (Login)model.get("user");
         if(user != null && user.getUserRole().equals(UserRole.ADMIN.getUserRoleValue())){
            contactService.deleteContact(var);
            return new ModelAndView("redirect:/contacts");
         }
         return new ModelAndView("redirect:/login");
    }
     
    @RequestMapping(value = "/admin/feedbacks", method =RequestMethod.GET)
    public ModelAndView retrieveContacts(ModelMap model){
        Login user = (Login)model.get("user");
        if(user != null && user.getUserRole().equals(UserRole.ADMIN.getUserRoleValue())){
            List<Feedback> feedbacks = feedBackService.retrieveFeedBack();
            model.addAttribute("feedbacks",feedbacks);
            int positive = 0, negative = 0;
            for(Feedback f : feedbacks){
                if(f.getFeedbackType()==1){
                    positive++;
                }else{
                    negative++;
                }
            }
            model.addAttribute("positive",positive);
            model.addAttribute("negative",negative);
            return new ModelAndView("feedbacks");
        }
        return new ModelAndView("redirect:/login");
    }     
    
    @RequestMapping(value = "/addmoderator", method = RequestMethod.GET)
    public ModelAndView showModeratorRegistration(@ModelAttribute("registration") Registration registration,ModelMap model){
        Login user = (Login)model.get("user");
        if(user != null && user.getUserRole().equals(UserRole.ADMIN.getUserRoleValue())){
            return new ModelAndView("addmoderator");
        }
        return new ModelAndView("redirect:/login");
    }
    
    @RequestMapping(value = "/addmoderator", method = RequestMethod.POST)
    public ModelAndView addModeraor(@ModelAttribute("registration") Registration registration,ModelMap model){
        Login user = (Login)model.get("user");
        if(user != null && user.getUserRole().equals(UserRole.ADMIN.getUserRoleValue())){
            User moderator = new User();
            moderator.setName(registration.getName());
            moderator.setEmail(registration.getEmail());
            moderator.setGender(registration.getGender());
            moderator.setContact(registration.getContact());
            moderator.setRegistrationDate(new Date());
            moderator.setUserStatus(UserStatus.Active.getUserStatusValue());
            
            userService.saveUser(moderator);
            
            UserAddressInfo moderatorAddress = new UserAddressInfo();
            moderatorAddress.setIdentificationNumber(moderator.getIdentificationNumber());
            moderatorAddress.setCountry(registration.getCountry());
            
            profileService.addAddresssInfo(moderatorAddress);
            
            String password = RandomStringUtils.randomAlphanumeric(ID_LENTH);
            Login moderatorLogin = new Login();
            moderatorLogin.setIdentificationNumber(moderator.getIdentificationNumber());
            moderatorLogin.setPassword(password);
            moderatorLogin.setUserName(registration.getEmail());
            moderatorLogin.setUserRole(UserRole.MODERATOR.getUserRoleValue());
            
            loginService.saveTraderCredentials(moderatorLogin);
            
            model.addAttribute("success","Moderator name: <b>"+registration.getName()+"</b> Added Successfully!");
            return new ModelAndView("redirect:/addmoderator");
        }
        return new ModelAndView("redirect:/login");
    }
    
    @RequestMapping(value = "/viewmoderator", method = RequestMethod.GET)
    public ModelAndView viewModeratores(ModelMap model){
        Login user = (Login)model.get("user");
        if(user != null && user.getUserRole().equals(UserRole.ADMIN.getUserRoleValue())){
            List<Integer> moderatorsIN = loginService.retrieveUserByRole(UserRole.MODERATOR.getUserRoleValue());
            ArrayList<User> moderatorList = new ArrayList<User>();
            if(moderatorsIN != null){
                for(Integer i : moderatorsIN){
                    moderatorList.add(userService.retrieveUser(i));
                }
            }
            model.addAttribute("moderatorList",moderatorList);
            return new ModelAndView("viewmoderator");
        }
        return new ModelAndView("redirect:/login");
    }    
    
    @RequestMapping(value = "/inactive/{moderatorIN}", method = RequestMethod.GET)
    public ModelAndView inactiveModeratores(@PathVariable Integer moderatorIN,ModelMap model){
        Login user = (Login)model.get("user");
        if(user != null && user.getUserRole().equals(UserRole.ADMIN.getUserRoleValue())){
            Login moderator = loginService.retrieveUser(moderatorIN);
            if(moderator.getUserRole().equals(UserRole.MODERATOR.getUserRoleValue())){
                User retrieveUser = userService.retrieveUser(moderatorIN);
                if(retrieveUser.getUserStatus().equals(UserStatus.Active.getUserStatusValue())){
                    retrieveUser.setUserStatus(UserStatus.InActive.getUserStatusValue());
                }else{
                    retrieveUser.setUserStatus(UserStatus.Active.getUserStatusValue());
                }
                userService.updateUser(retrieveUser);
            }
            return new ModelAndView("redirect:/viewmoderator");
        }
        return new ModelAndView("redirect:/login");
    }    
    
    @RequestMapping(value = "/announcements", method = RequestMethod.GET)
    public ModelAndView viewAnnouncements(@ModelAttribute("announcement") Announcement announcement,ModelMap model){
        Login user = (Login)model.get("user");
        if(user != null && user.getUserRole().equals(UserRole.ADMIN.getUserRoleValue())){
            List<Announcement> announcements = announceService.Retriveannouncement(new Date());
            model.addAttribute("announcements",announcements);
            return new ModelAndView("announcements");
        }
        return new ModelAndView("redirect:/login");
    }    

    @RequestMapping(value = "/announcements", method = RequestMethod.POST)
    public ModelAndView addAnnouncements(@Valid @ModelAttribute("announcement") Announcement announcement,BindingResult result,ModelMap model){
        Login user = (Login)model.get("user");
        if(user != null && user.getUserRole().equals(UserRole.ADMIN.getUserRoleValue())){
            if(result.hasErrors()){
                return new ModelAndView("announcements");
            }
            announcement.setCreatedBy(user.getIdentificationNumber());
            announcement.setCreationDate(new Date());
            
            announceService.saveAnnouncement(announcement);
            return new ModelAndView("redirect:/announcements");
        }
        return new ModelAndView("redirect:/login");
    }    
}