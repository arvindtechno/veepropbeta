/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.coretechies.veepropbeta.web.controller.webdomain;

/**
 *
 * @author Noppp
 */
public class Sellers {
    
    private String name;
    
    private String  email;
    
    private String contact;
    
    private Integer numberOfShare;
    
    private Float sharePrice;
    
    private Double amount;
    
    private Integer identificationNumber;
    
    private Integer shareTransactionId;
    
    private Double vCredit;
    
    private String fileName;
    
    private String bankTransactionId;

    public String getBankTransactionId() {
        return bankTransactionId;
    }

    public void setBankTransactionId(String bankTransactionId) {
        this.bankTransactionId = bankTransactionId;
    }
    
    

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public Double getvCredit() {
        return vCredit;
    }

    public void setvCredit(Double vCredit) {
        this.vCredit = vCredit;
    }
    

    public Integer getIdentificationNumber() {
        return identificationNumber;
    }

    public void setIdentificationNumber(Integer identificationNumber) {
        this.identificationNumber = identificationNumber;
    }

    public Integer getShareTransactionId() {
        return shareTransactionId;
    }

    public void setShareTransactionId(Integer shareTransactionId) {
        this.shareTransactionId = shareTransactionId;
    }
    
    

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public Integer getNumberOfShare() {
        return numberOfShare;
    }

    public void setNumberOfShare(Integer numberOfShare) {
        this.numberOfShare = numberOfShare;
    }

    public Float getSharePrice() {
        return sharePrice;
    }

    public void setSharePrice(Float sharePrice) {
        this.sharePrice = sharePrice;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return "Sellers{" + "name=" + name + ", email=" + email + ", contact=" + contact + ", numberOfShare=" + numberOfShare + ", sharePrice=" + sharePrice + ", amount=" + amount + ", identificationNumber=" + identificationNumber + ", shareTransactionId=" + shareTransactionId + ", vCredit=" + vCredit + ", fileName=" + fileName + ", bankTransactionId=" + bankTransactionId + '}';
    }

}
