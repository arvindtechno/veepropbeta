/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.coretechies.veepropbeta.web.controller.webdomain;

import java.util.Date;

/**
 *
 * @author Noppp
 */
public class PendingShareTransaction {
 
    private String sellerName;
    
    private String buyerName;
    
    private Date transactionDate;
    
    private String transactionStatus;
    
    private Integer numberOfShare;
    
    private float costPerShare;
    
    private Integer transactionId;

    public Integer getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(Integer transactionId) {
        this.transactionId = transactionId;
    }
    
    public String getSellerName() {
        return sellerName;
    }

    public void setSellerName(String sellerName) {
        this.sellerName = sellerName;
    }

    public String getBuyerName() {
        return buyerName;
    }

    public void setBuyerName(String buyerName) {
        this.buyerName = buyerName;
    }

    public Date getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(Date transactionDate) {
        this.transactionDate = transactionDate;
    }

    public String getTransactionStatus() {
        return transactionStatus;
    }

    public void setTransactionStatus(String transactionStatus) {
        this.transactionStatus = transactionStatus;
    }

    public Integer getNumberOfShare() {
        return numberOfShare;
    }

    public void setNumberOfShare(Integer numberOfShare) {
        this.numberOfShare = numberOfShare;
    }

    public float getCostPerShare() {
        return costPerShare;
    }

    public void setCostPerShare(float costPerShare) {
        this.costPerShare = costPerShare;
    }

    @Override
    public String toString() {
        return "PendingShareTransaction{" + "sellerName=" + sellerName + ", buyerName=" + buyerName + ", transactionDate=" + transactionDate + ", transactionStatus=" + transactionStatus + ", numberOfShare=" + numberOfShare + ", costPerShare=" + costPerShare + ", transactionId=" + transactionId + '}';
    }
    
}
