/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.coretechies.veepropbeta.web.controller;
import com.coretechies.veepropbeta.domain.Login;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import com.coretechies.veepropbeta.domain.Announcement;
import com.coretechies.veepropbeta.service.announcement.announcementService;
import java.util.Date;
import java.util.List;
/**
 *
 * 
 * @author Rohit
 */

@Controller
@SessionAttributes({"user"})
public class announcementTraderController{
    
    private final Logger logger= LoggerFactory.getLogger(getClass());
   
    @Autowired
    private announcementService announcement;

    @RequestMapping(value = "/announcementdata",method = RequestMethod.GET)
    public ModelAndView retriveannouncement(ModelMap model){
        Login user=(Login)model.get("user");
        if(user!=null){
            List<Announcement> annoument= announcement.Retriveannouncement(new Date());
            model.addAttribute("annoument", annoument);
            logger.info("Error Announcement"+annoument.size());
            return new ModelAndView("announcementdata");
        }
        return new ModelAndView("redirect:/login");
    }
}
