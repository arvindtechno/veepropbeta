/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.coretechies.veepropbeta.web.controller;

import com.coretechies.veepropbeta.domain.Feedback;
import com.coretechies.veepropbeta.domain.Login;
import com.coretechies.veepropbeta.domain.Notification;
import com.coretechies.veepropbeta.service.feedback.FeedBackService;
import com.coretechies.veepropbeta.service.notification.NotificationService;
import com.coretechies.veepropbeta.web.controller.algorithm.UserRole;
import java.util.List;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
/**
 *
 * @author Rohit
 */
@Controller
@SessionAttributes("user")
public class FeedbackController {
    
    private final Logger logger= LoggerFactory.getLogger(getClass());
    
    @Autowired
    private FeedBackService feedbackservice;
    
    @Autowired
    private NotificationService notificationService;
    
     @RequestMapping(value = "/feedback",method = RequestMethod.GET)
     public ModelAndView addfeedback(@ModelAttribute("feedback") Feedback feedback, ModelMap model){
         Login user = (Login)model.get("user");
         if(user != null && user.getUserRole().equals(UserRole.TRADER.getUserRoleValue())){
            List<Notification> notifications = notificationService.retreiveNotification(user.getIdentificationNumber());
            model.addAttribute("notification", notifications);             
             return new ModelAndView("feedback");
         }
         return new ModelAndView("redirect:/login");
     }
     
     @RequestMapping(value ="/feedback",method = RequestMethod.POST)
     public ModelAndView Savefeedback(@Valid @ModelAttribute("feedback")Feedback feedback,BindingResult result,ModelMap model){
         Login user = (Login) model.get("user");         
         if(user != null && user.getUserRole().equals(UserRole.TRADER.getUserRoleValue())){
            if(result.hasErrors()){
               return new ModelAndView("feedback");
            }
            List<Notification> notifications = notificationService.retreiveNotification(user.getIdentificationNumber());
            model.addAttribute("notification", notifications);              
            feedbackservice.saveFeedBack(feedback,user.getIdentificationNumber());
            return new ModelAndView("redirect:/feedback");
         }
         return  new ModelAndView("redirect:/login");
     }
}