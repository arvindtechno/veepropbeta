/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.coretechies.veepropbeta.web.controller;

import com.coretechies.veepropbeta.domain.Dispute;
import com.coretechies.veepropbeta.domain.InGameTraderDetail;
import com.coretechies.veepropbeta.domain.Login;
import com.coretechies.veepropbeta.domain.LotDetails;
import com.coretechies.veepropbeta.domain.Notification;
import com.coretechies.veepropbeta.domain.ProfileChangeRequest;
import com.coretechies.veepropbeta.domain.SecurityQuestion;
import com.coretechies.veepropbeta.domain.ShareTradingTransaction;
import com.coretechies.veepropbeta.domain.TraderDynamicData;
import com.coretechies.veepropbeta.domain.Transaction;
import com.coretechies.veepropbeta.domain.User;
import com.coretechies.veepropbeta.domain.UserAddressInfo;
import com.coretechies.veepropbeta.domain.UserBankDetail;
import com.coretechies.veepropbeta.domain.VeeCreditTransaction;
import com.coretechies.veepropbeta.domain.VerificationModerator;
import com.coretechies.veepropbeta.service.bonustransaction.BonusTransactionService;
import com.coretechies.veepropbeta.service.dispute.DisputeService;
import com.coretechies.veepropbeta.service.ingamecreditdetails.InGameCreditDetailsService;
import com.coretechies.veepropbeta.service.lotdetails.LotDetailService;
import com.coretechies.veepropbeta.service.notification.NotificationService;
import com.coretechies.veepropbeta.service.profile.ProfileService;
import com.coretechies.veepropbeta.service.profilechangerequest.ProfileChangeRequestService;
import com.coretechies.veepropbeta.service.sharetradingtransaction.ShareTradingTransactionService;
import com.coretechies.veepropbeta.service.traderdynamicdata.TraderDynamicDataService;
import com.coretechies.veepropbeta.service.transaction.TransactionService;
import com.coretechies.veepropbeta.service.user.UserService;
import com.coretechies.veepropbeta.service.userbankdetail.UserBankDetailService;
import com.coretechies.veepropbeta.service.veecredittransaction.VeeCreditTransactionService;
import com.coretechies.veepropbeta.service.verificationmoderator.VerificationModeratorService;
import com.coretechies.veepropbeta.web.controller.algorithm.LevelDecide;
import com.coretechies.veepropbeta.web.controller.algorithm.TransactionStatus;
import com.coretechies.veepropbeta.web.controller.algorithm.TransactionType;
import com.coretechies.veepropbeta.web.controller.algorithm.UserRole;
import com.coretechies.veepropbeta.web.controller.webdomain.ModeratorData;
import com.coretechies.veepropbeta.web.controller.webdomain.PendingShareTransaction;
import com.coretechies.veepropbeta.web.controller.webdomain.PendingVeeCreditTransaction;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author Noppp
 */
@Controller
@SessionAttributes({"user"})
public class ModeratorController {
    
    private final Logger logger = LoggerFactory.getLogger(getClass());
    
    @Autowired
    private UserService userService;
    
    @Autowired
    private NotificationService notificationService;
    
    @Autowired
    private ShareTradingTransactionService shareTradingTransactionService;
    
    @Autowired
    private TransactionService transactionService;
    
    @Autowired
    private VeeCreditTransactionService veeCreditTransactionService;
    
    @Autowired
    private ProfileChangeRequestService profileChangeRequestService;
    
    @Autowired
    private LotDetailService lotDetailService;
    
    @Autowired
    private VerificationModeratorService verificationModeratorService;
    
    @Autowired
    private DisputeService disputeService;
    
    @Autowired
    private ProfileService profileService;
    
    @Autowired
    private UserBankDetailService userBankDetailService;
    
    @Autowired
    private InGameCreditDetailsService inGameCreditDetailsService;
    
    @Autowired
    private TraderDynamicDataService traderDynamicDataService;
    
    @InitBinder
    public void initBinder(WebDataBinder webDataBinder) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        dateFormat.setLenient(false);
        webDataBinder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
    }    
    
    @RequestMapping(value = "/pendingshare", method = RequestMethod.GET)
    public ModelAndView viewPendingShareTransaction(ModelMap model){
        Login user = (Login)model.get("user");
        if(user != null && user.getUserRole().equals(UserRole.MODERATOR.getUserRoleValue())){
            LotDetails currentLot = lotDetailService.retrieveLotDetail();
            List<Transaction> transactionList = transactionService.retrieveTransactionsPastTenDates(new Date());
            logger.info("transaction list size"+transactionList.size());
            model.addAttribute("name",userService.retrieveUser(user.getIdentificationNumber()).getName());
            ArrayList<PendingShareTransaction> pendingSTT = new ArrayList<PendingShareTransaction>();
            for(Transaction transaction : transactionList){
                if(transaction.getTransactionType() == TransactionType.SHARETRADINGTRANSACTION.getTransactionTypeValue()){
                    ShareTradingTransaction STT = shareTradingTransactionService.retrieveSTTByTransaction(transaction.getTransactionId());
                    logger.info("STT is:"+STT.getCostPerShare()+" for "+STT.getNumberOfShare());
                    logger.info("STT status is:"+TransactionStatus.STATUS.getStatusType(STT.getTransactionStatus()));
                    PendingShareTransaction pendingShareTransaction = new PendingShareTransaction();
                    if(transaction.getBuyerIN()==null){
                        pendingShareTransaction.setBuyerName("---");
                    }else{
                        pendingShareTransaction.setBuyerName(userService.retrieveUser(transaction.getBuyerIN()).getName());
                    }
                    pendingShareTransaction.setSellerName(userService.retrieveUser(transaction.getSellerIN()).getName());
                    pendingShareTransaction.setCostPerShare(STT.getCostPerShare());
                    pendingShareTransaction.setNumberOfShare(STT.getNumberOfShare());
                    pendingShareTransaction.setTransactionDate(transaction.getTransactionDate());
                    pendingShareTransaction.setTransactionId(transaction.getTransactionId());
                    pendingShareTransaction.setTransactionStatus(TransactionStatus.STATUS.getStatusType(STT.getTransactionStatus()));
                    pendingSTT.add(pendingShareTransaction);
                }
            }
            logger.info("pending share transaction size: "+pendingSTT.size());
            model.addAttribute("currentLot",currentLot);
            model.addAttribute("pendingSTT",pendingSTT);
            return new ModelAndView("pendingshare");
        }
        return new ModelAndView("redirect:/login");
    }
    
    @RequestMapping(value = "/pendingvcredit" , method = RequestMethod.GET)
    public ModelAndView viewPendingVCredit(ModelMap model){
        Login user = (Login)model.get("user");
        if(user != null && user.getUserRole().equals(UserRole.MODERATOR.getUserRoleValue())){
            LotDetails currentLot = lotDetailService.retrieveLotDetail();
            Date date = new Date();
            date.setDate(date.getDate()-10);
            List<Transaction> transactionList = transactionService.retrieveTransactionsPastTenDates(date);
            logger.info("transaction list size"+transactionList.size());
            model.addAttribute("name",userService.retrieveUser(user.getIdentificationNumber()).getName());
            ArrayList<PendingVeeCreditTransaction> pendingVCT = new ArrayList<PendingVeeCreditTransaction>();
            for(Transaction transaction : transactionList){
                if(transaction.getTransactionType() == TransactionType.VEECREDITTRANSACTION.getTransactionTypeValue()){
                    VerificationModerator VM = verificationModeratorService.retrieveVM(transaction.getTransactionId());
                    if(VM != null){
                        if(VM.getIdentificationNumber() == user.getIdentificationNumber()){
                            VeeCreditTransaction VCT = veeCreditTransactionService.retrieveVCTByTransaction(transaction.getTransactionId());
                            if(VCT.getTransactionStatus() != TransactionStatus.COMPLETE.getTransactionStatusValue()){
                                logger.info("VCT is:"+VCT.getVeeCredit());
                                logger.info("VCT status is:"+TransactionStatus.STATUS.getStatusType(VCT.getTransactionStatus()));
                                PendingVeeCreditTransaction pendingVeeCreditTransaction = new PendingVeeCreditTransaction();
                                if(transaction.getBuyerIN()==null){
                                    pendingVeeCreditTransaction.setBuyerName("---");
                                }else{
                                    pendingVeeCreditTransaction.setBuyerName(userService.retrieveUser(transaction.getBuyerIN()).getName());
                                }
                                pendingVeeCreditTransaction.setSellerName(userService.retrieveUser(transaction.getSellerIN()).getName());
                                pendingVeeCreditTransaction.setTransactionDate(transaction.getTransactionDate());
                                pendingVeeCreditTransaction.setTransactionId(VCT.getVeeCreditTransactionId());
                                pendingVeeCreditTransaction.setTransactionStatus(TransactionStatus.STATUS.getStatusType(VCT.getTransactionStatus()));
                                pendingVeeCreditTransaction.setVeeCredit(VCT.getVeeCredit());
                                pendingVeeCreditTransaction.setSellerIn(transaction.getSellerIN());
                                if(transaction.getBuyerIN()!=null){
                                    pendingVeeCreditTransaction.setBuyerIn(transaction.getBuyerIN());
                                }
                                pendingVCT.add(pendingVeeCreditTransaction);
                            }
                        }
                    }
                }
            }
            logger.info("pending share transaction size: "+pendingVCT.size());
            model.addAttribute("currentLot",currentLot);
            model.addAttribute("pendingVCT",pendingVCT);
            return new ModelAndView("pendingvcredit");           
        }
        return new ModelAndView("redirect:/login");
    }
    
    @RequestMapping(value = "/pendingprofiles", method = RequestMethod.GET)
    public ModelAndView pendingProfiles(ModelMap model){
        Login user = (Login)model.get("user");
        if(user != null && user.getUserRole().equals(UserRole.MODERATOR.getUserRoleValue())){
            model.addAttribute("name",userService.retrieveUser(user.getIdentificationNumber()).getName());
            List<ProfileChangeRequest> changeList = profileChangeRequestService.retieveAllPendingRequest(user.getIdentificationNumber());
            model.addAttribute("changeList",changeList);
            logger.info("pending reuest for you is:"+changeList.size());
            return new ModelAndView("pendingprofiles");
        }
        return new ModelAndView();
    }
    
    @RequestMapping(value = "/{action}/profile/{userIN}", method = RequestMethod.GET)
    public ModelAndView actionPendingProfiles(@PathVariable String action,@PathVariable Integer userIN,ModelMap model){
        Login user = (Login)model.get("user");
        if(user != null && user.getUserRole().equals(UserRole.MODERATOR.getUserRoleValue())){
            model.addAttribute("name",userService.retrieveUser(user.getIdentificationNumber()).getName());
            ProfileChangeRequest retievePCR = profileChangeRequestService.retievePCR(userIN);
            
            Notification notification = new Notification();
            notification.setIdentificationNumber(userIN);
            notification.setNotification("Your profile upgrade request has been "+action+"ed.");
            notification.setNotificationDate(new Date());
            notification.setNotificationStatus(Boolean.FALSE);
            notification.setNotificationLink("/veepropbeta/notification/"+-2+"/"+0);
            notificationService.saveNotification(notification);
            if(action.equalsIgnoreCase("accept")){
                retievePCR.setRequestStatus(Boolean.TRUE);
                retievePCR.setVerificationDate(new Date());
                profileChangeRequestService.updatePCRS(retievePCR);
                
                UserAddressInfo address = profileService.retriveAddress(userIN);
                User personal = profileService.retrivePersonal(userIN);
                
                address.setCity(retievePCR.getCity());
                address.setCountry(retievePCR.getCountry());
                address.setHouseNumber(retievePCR.getHouseNumber());
                address.setLandmark(retievePCR.getLandmark());
                address.setStreetName1(retievePCR.getStreetName1());
                address.setStreetName2(retievePCR.getStreetName2());
                address.setUserstate(retievePCR.getUserState());
                address.setZipcode(retievePCR.getUserState());
                
                personal.setContact(retievePCR.getContact());
                personal.setDateOfBirth(retievePCR.getDateOfBirth());
                personal.setEmail(retievePCR.getEmail());
                personal.setGender(retievePCR.isGender());
                personal.setName(retievePCR.getName());
                personal.setSecurityAnswer(retievePCR.getSecurityAnswer());
                personal.setSecurityQuestionId(retievePCR.getSecurityQuestionId());
                
                profileService.updateProfile(address,personal);
            }else if(action.equalsIgnoreCase("reject")){
                profileChangeRequestService.deletePCRS(retievePCR);
            }
            return new ModelAndView("redirect:/pendingprofiles");
        }
        return new ModelAndView("redirect:/login");
    }
    
    @RequestMapping(value = "/moderator/profile/{val}", method = RequestMethod.GET)
    public ModelAndView viewModeProfile(@ModelAttribute("moderator") ModeratorData moderator,@PathVariable Integer val,ModelMap model){
        Login user = (Login)model.get("user");
        if(user != null){
            model.addAttribute("mode",val);
            UserAddressInfo addressMode = profileService.retriveAddress(user.getIdentificationNumber());
            User personalMode = profileService.retrivePersonal(user.getIdentificationNumber());
            List<SecurityQuestion> retrieveSecurityQuestion = userService.retrieveSecurityQuestion();
            model.addAttribute("questions",retrieveSecurityQuestion);
            for(SecurityQuestion sq :retrieveSecurityQuestion){
                if(sq.getSecurityQuestionId() == personalMode.getSecurityQuestionId()){
                    model.addAttribute("question",sq.getSecurityQuestion());
                }
            }
            moderator.setCity(addressMode.getCity());
            moderator.setContact(personalMode.getContact());
            moderator.setCountry(addressMode.getCountry());
            moderator.setDateOfBirth(personalMode.getDateOfBirth());
            moderator.setEmail(personalMode.getEmail());
            moderator.setGender(personalMode.getGender());
            moderator.setHouseNumber(addressMode.getHouseNumber());
            moderator.setLandmark(addressMode.getLandmark());
            moderator.setName(personalMode.getName());
            moderator.setSecurityAnswer(personalMode.getSecurityAnswer());
            moderator.setSecurityQuestionId(personalMode.getSecurityQuestionId());
            moderator.setStreetName1(addressMode.getStreetName1());
            moderator.setStreetName2(addressMode.getStreetName2());
            moderator.setUserstate(addressMode.getUserstate());
            moderator.setZipcode(addressMode.getZipcode());
            if(user.getUserRole().equals(UserRole.MODERATOR.getUserRoleValue())){
                return new ModelAndView("modeprofile");
            }else if(user.getUserRole().equals(UserRole.ADMIN.getUserRoleValue())){
                return new ModelAndView("adminprofile");
            }
        }
        return new ModelAndView("redirect:/login");
    }
    
    @RequestMapping(value = "/moderator/profile", method = RequestMethod.POST)
    public String updateModeProfile(@ModelAttribute("moderator") ModeratorData moderator,ModelMap model){
        Login user = (Login)model.get("user");
        if(user != null){
            UserAddressInfo addressMode = profileService.retriveAddress(user.getIdentificationNumber());
            User personalMode = profileService.retrivePersonal(user.getIdentificationNumber());
            addressMode.setCity(moderator.getCity());
            addressMode.setCountry(moderator.getCountry());
            addressMode.setHouseNumber(moderator.getHouseNumber());
            addressMode.setLandmark(moderator.getLandmark());
            addressMode.setStreetName1(moderator.getStreetName1());
            addressMode.setStreetName2(moderator.getStreetName2());
            addressMode.setUserstate(moderator.getUserstate());
            addressMode.setZipcode(moderator.getZipcode());
            
            personalMode.setContact(moderator.getContact());
            personalMode.setDateOfBirth(moderator.getDateOfBirth());
            personalMode.setEmail(moderator.getEmail());
            personalMode.setGender(moderator.getGender());
            personalMode.setName(moderator.getName());
            personalMode.setSecurityAnswer(moderator.getSecurityAnswer());
            personalMode.setSecurityQuestionId(moderator.getSecurityQuestionId());
            
            
            profileService.updateProfile(addressMode, personalMode);
            return "redirect:/moderator/profile/0";
        }
        return "redirect:/login";
    }
    
    @RequestMapping(value = "/disputecases", method = RequestMethod.GET)
    public ModelAndView viewDisupteToModerator(ModelMap model){
        Login user = (Login)model.get("user");
        if(user != null && user.getUserRole().equals(UserRole.MODERATOR.getUserRoleValue())){
            model.addAttribute("name",userService.retrieveUser(user.getIdentificationNumber()).getName());
            List<VeeCreditTransaction> diputeTransaction = veeCreditTransactionService.retrievePendingTransaction(TransactionStatus.DISPUTE.getTransactionStatusValue());
            List<Dispute> disputeForModerator = new ArrayList<Dispute>();
            for(VeeCreditTransaction creditTransaction:diputeTransaction){
                Dispute dispute = disputeService.retrieveDisputeForModByVCT(creditTransaction.getVeeCreditTransactionId());
                if(dispute.getModeratorIN().equals(user.getIdentificationNumber())){
                    disputeForModerator.add(dispute);
                }
            }
            logger.info("dispute Cases is:"+disputeForModerator.size());
            model.addAttribute("disputeForModerator",disputeForModerator);
            return new ModelAndView("disputemoderator");
        }
        return new ModelAndView("redirect:/login");
    }
    
    @RequestMapping(value = "/disputedetail/{vctId}" , method = RequestMethod.GET)
    public ModelAndView viewDisputeDetails(@PathVariable Integer vctId,ModelMap model){
        Login user = (Login)model.get("user");
        if(user != null && user.getUserRole().equals(UserRole.MODERATOR.getUserRoleValue())){
            Dispute dispute = disputeService.retrieveDisputeForModByVCT(vctId);
            model.addAttribute("name",userService.retrieveUser(user.getIdentificationNumber()).getName());            
            logger.info("dispute is:"+dispute);
            Transaction transaction = transactionService.retrieveTransaction(dispute.getTransactionId());
            VeeCreditTransaction VCT = veeCreditTransactionService.retrieveVCT(dispute.getVeeCreditTransactionId());
            User seller = userService.retrieveUser(transaction.getSellerIN());
            UserBankDetail sellerBank = userBankDetailService.retriveBank(seller.getIdentificationNumber());
            model.addAttribute("transaction",transaction);
            model.addAttribute("sellerBank",sellerBank);
            model.addAttribute("dispute",dispute);
            model.addAttribute("seller",seller);
            return new ModelAndView("disputedata");
        }
        return new ModelAndView("redirect:/login");
    }
    
    @RequestMapping(value = "/dispute/{command}/{vctId}", method = RequestMethod.GET)
    public ModelAndView actionDispute(@PathVariable String command, 
            @PathVariable Integer vctId,ModelMap model){
        Login user = (Login)model.get("user");
        if(user!=null && user.getUserRole().equals(UserRole.MODERATOR.getUserRoleValue())){
            Dispute dispute = disputeService.retrieveDisputeForModByVCT(vctId);
            if(command.equalsIgnoreCase("decline")){
                VeeCreditTransaction VCT = veeCreditTransactionService.retrieveVCT(vctId);
                VCT.setTransactionStatus(TransactionStatus.LOCK.getTransactionStatusValue());
                veeCreditTransactionService.updateVCT(VCT);
                disputeService.deleteDispute(dispute);
            }else if(command.equalsIgnoreCase("accept")){
                    InGameTraderDetail sellerDetail = inGameCreditDetailsService.retrieveIGCD(dispute.getBuyerIN());
                    InGameTraderDetail buyerDetail = inGameCreditDetailsService.retrieveIGCD(dispute.getSellerIN());

                    Transaction t = transactionService.retrieveTransaction(dispute.getTransactionId());
                    sellerDetail.setvCredit(sellerDetail.getvCredit() + t.getAmount());

                    TraderDynamicData TDD = traderDynamicDataService.retrieveTDD(dispute.getBuyerIN());
                    LevelDecide ld = new LevelDecide(TDD.getPackageAmount()+t.getAmount());
                    int oldLevel = sellerDetail.getLevelIN();
                    buyerDetail.setvCredit(buyerDetail.getvCredit()+(t.getAmount()*.1));
                    buyerDetail.setvCreditHold(buyerDetail.getvCreditHold() - (t.getAmount()*.1));
                    sellerDetail.setLevelIN(ld.getLevelDecided());

                    inGameCreditDetailsService.updateIGTD(buyerDetail);       
                    inGameCreditDetailsService.updateIGTD(sellerDetail);

                    TDD.setPackageAmount(TDD.getPackageAmount() +t.getAmount());
                    traderDynamicDataService.updateTDD(TDD);
                    
                    if(oldLevel < ld.getLevelDecided()){
                        Notification notificationLevel = new Notification();
                        notificationLevel.setIdentificationNumber(dispute.getBuyerIN());
                        notificationLevel.setNotificationStatus(Boolean.FALSE);
                        notificationLevel.setNotificationDate(new Date());
                        String link = "/veepropbeta/notification/"+(-1)+"/"+0;
                        notificationLevel.setNotificationLink(link);
                        notificationLevel.setNotification("Level updated to "+ld.getLevelDecided());

                        notificationService.saveNotification(notificationLevel);
                    }
                    
                    VerificationModerator retrieveVM = verificationModeratorService.retrieveVM(t.getTransactionId());
                    verificationModeratorService.deleteVM(retrieveVM);     
                    VeeCreditTransaction retrieveVCT = veeCreditTransactionService.retrieveVCT(dispute.getVeeCreditTransactionId());
                    retrieveVCT.setTransactionStatus(TransactionStatus.COMPLETE.getTransactionStatusValue());
                    veeCreditTransactionService.updateVCT(retrieveVCT);
                    disputeService.deleteDispute(dispute);                    
            }
            return new ModelAndView("redirect:/disputecases");
        }
        return new ModelAndView("redirect:/login");
    }
}
