
<%@taglib  prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE HTML>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" href="/veepropbeta/css/style.css" type="text/css" media="screen" />
    <link href='http://fonts.googleapis.com/css?family=Open+Sans|Baumans' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="/veepropbeta/css/bootstrap-responsive.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="/veepropbeta/css/bootstrap-responsive.min.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="/veepropbeta/css/bootstrap.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="/veepropbeta/css/bootstrap.min.css" type="text/css" media="screen" />
    <script src="http://code.jquery.com/jquery-1.7.2.min.js"></script>
    <link rel="stylesheet" type="text/css" href="/veepropbeta/css/style_light.css">
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>
    <script src="/veepropbeta/js/jquery-ui-1.8.14.custom.min.js" type="text/javascript"></script>
    <script src="/veepropbeta/js/ttw-notification-menu.min.js" type="text/javascript"></script>
    <script src="/veepropbeta/js/demo.js" type="text/javascript"></script>      
    <style type="text/css">
        .tooltip {
            width: 250px;
            font-size: 11px;
            font-family: Arial, sans-serif;
            background: #444;
            border: 1px solid #090909;
            border-radius: 4px;
            -moz-border-radius: 4px;
            -webkit-border-radius: 4px;
            position: absolute;
            z-index: 1;
            box-shadow: 0 2px 5px rgba(0, 0, 0, 0.5);
            -webkit-box-shadow: 0 2px 5px rgba(0, 0, 0, 0.5);
            -moz-box-shadow: 0 2px 5px rgba(0, 0, 0, 0.5);
            color:#fff;
            padding:12px 24px;
            line-height:18px;
        }

        .tooltip:after {
            content: '';
            position: absolute;
            border-color: transparent  #444 transparent transparent;
            border-style: solid;
            border-width: 10px;
            height: 0;
            width: 0;
            position: absolute;
            left: 0;
            top: 50%;
            margin-top: -10px;
            margin-left: -20px;
        }

        .tooltip:before {
            content: '';
            position: absolute;
            border-color:   transparent #090909 transparent transparent;
            border-style: solid;
            border-width: 10px;
            height: 0;
            width: 0;
            position: absolute;
            left: 0;
            top: 50%;
            margin-top: -10px;
            margin-left: -20px;
        }

        .ttw-notification-menu{
                width: 276px;
        }
        
        .error{
            color: red;
            font-weight: bold;
        }


    </style>
</head>
<body>
    <input type="hidden" id="notification-form" /> 
    <%@include file="header.jsp" %>
    <%@include file="information.jsp" %>
    <c:forEach var="notification" items="${notification}" varStatus="status">
        <script type="text/javascript">
            $(document).ready(function(e){
            $("#notification-form").val('<a href="${notification.notificationLink}/${notification.notificationId}">${notification.notification}</a>');
            $("#notification-form").click();
        });
        </script>
    </c:forEach>  
    <div class="container-fluid" style="margin-top: -10px;">
        <div class="row-fluid">
         <%@include  file="sellboard.jsp" %>
        <div class="span6">
              <ul class="nav nav-pills">
                  <li><a href="/veepropbeta/home">Dashboard</a></li>
                 <li><a href="/veepropbeta/profile">Profile</a></li>
                 <li><a href="/veepropbeta/wallet">Wallet</a></li>
                 <li><a href="/veepropbeta/business">Biz Management</a></li>
                 <li><a href="/veepropbeta/feedback">Feedback</a></li>
                <li><a id="projects" class="notification-menu-item first-item list_inline" style="float: right;">Notifications</a></li>
               </ul>
            <a href="/veepropbeta/wallet" class="list-group-item active" style="margin-top: 2%;">Feedback Form</a>
            <div style="height:30px;"></div>
            <div id="info"></div>
            <form:form action="/veepropbeta/feedback" modelAttribute="feedback" method="post">
            <div class="form-group">
                     <form:label for="inputEmail3" path="feedbackType" class="col-sm-4 control-label">Feedback Type</form:label>
                   <div class="col-sm-8">
                   <form:select path="feedbackType" class="form-control" id="feedtype">
                        <form:option value="">--Please Select Feedback Type--</form:option>
                        <form:option value="1">Positive</form:option>
                        <form:option value="2">Negative</form:option>
                    </form:select> 
                   <form:errors path="feedbackType" element="span" cssClass="error"/>
                   </div>
                </div>
            <div style="height:30px;"></div>
                   <div class="form-group" >
                     <form:label for="inputEmail3" path="content" class="col-sm-4 control-label">Feedback Message</form:label>
                   <div class="col-sm-8">
                   <form:textarea path="content" class="form-control" id="content" />
                   <form:errors path="content" cssClass="error" element="span"/>
                   </div>
                </div>
                  
                   <div class="col-md-2 offset4" style="margin-top: 5%;">
                       <form:button  class="btn btn-success" >Submit</form:button>
                       
                       </div>
                      <div class="col-md-2 offset" style="margin-top: 5%;">
                       <form:button  class="btn btn-danger" >Cancel</form:button>
                       
                       </div>
               
            </form:form>
          </div>
             <%@include  file="noticeboard.jsp" %>
                 
        </div>
    </div>
  
 
    <%@include file="footer.jsp" %>
</body>
</html>



