
<!DOCTYPE HTML>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Veeprop</title>
<link href='http://fonts.googleapis.com/css?family=Open+Sans|Baumans' rel='stylesheet' type='text/css'>

<link rel="stylesheet" href="/veepropbeta/css/bootstrap.css" type="text/css" media="screen" />
<link rel="stylesheet" href="/veepropbeta/css/bootstrap-responsive.css" type="text/css" media="screen" />   
<script src="http://code.jquery.com/jquery-1.7.2.min.js"></script>
<style>
    table tr th, table tbody tr td {
        border: 1px black solid;
        padding: 0 20px;
        text-align: center;
    }
    table tr {
        border: 1px black solid;
        text-align: center;
    }
    table {
        border: 1px black solid;
        text-align: center;
    }
</style>
<script type="text/javascript">
    function moderatorReport(){
        var x = $("#moderator").val();
        var y = $("#type").val();
        if(x == -1 | y == -1){
            alert("please select entries");
            return false;
        }else{
            var url = "/veepropbeta/admin/"+x+"/"+y;
            $('#anchor').attr('href',url);
            return true
        }
    }
    
    function checkBonus(){
        var y = $("#bonus").val();
        if(y == -1){
            alert("please select Bonus Type");
            return false;
        }else{
            var url = "/veepropbeta/admin/bonus/"+y;
            $('#anchor2').attr('href',url);
            return true;
        }
    }
</script>
</head>
<body style="padding-top: 50px; font-family: sans-serif; color: #999;">
    <div class="container">    
        <div class="navbar navbar-fixed-top" style="padding: 0 20px 15px; background-color: #FDD35F; border-bottom: 1px solid #eea236;">
            <h4 style=" color: #000;">Veeprop Admin Panel</h4>
            <div class="navbar-inner pull-right">
                <ul class="nav nav-pills" style>

                <li><a href="/veepropbeta/home" style=" color: #000;">Dashboard</a></li>
                <li><a href="/veepropbeta/logout" style=" color: #000;">Logout</a></li>
            </ul>
            </div>
        </div>
    </div>
   
    <div class="container-fluid" style="padding-top:10px;">
        <div class="row" style="margin-top: 25px; border-bottom: 1px solid #eeeeee;">
            <div class="span3" style="float: left;border-right: 1px solid #eeeeee;border-bottom: 1px solid #eeeeee;">
                <ol class="breadcrumb">
                    <li>Admin Panel</li>
                </ol>

                <div class="navbar">
                  <ul class="nav nav-list">
                      <br class="clear"/>
                      <span class="light_head">Dashboard</span>
                      <hr />
                      <li><a href="/veepropbeta/home">Overview</a></li>
                      <li><a href="/veepropbeta/moderator/report">Reports</a></li>
                      <li><a href="/veepropbeta/moderator/profile/0">Profile</a></li>
                      <li><a href="/veepropbeta/announcements">Announcements</a></li>
                  </ul>
                </div>
                <div class="navbar">
                  <ul class="nav nav-list">
                      <span class="light_head"> Moderator</span>
                      <hr />
                      <li><a href="/veepropbeta/addmoderator">Add a Moderator</a></li>
                      <li><a href="/veepropbeta/viewmoderator">Active/Inactive a Moderator</a></li>
                  </ul>
                </div>
                <div class="navbar">
                  <ul class="nav nav-list">
                      <span class="light_head">Analysis</span>
                      <hr />
                      <li><a href="/veepropbeta/admin/feedbacks">Feedbacks</a></li>
                      <li><a href="/veepropbeta/contacts">Contacts</a></li>
                      <li><a href="/veepropbeta/faqadmin">FAQ</a></li>
                  </ul>
                </div>
            </div>
            <div class="span10" style=" overflow:auto; padding: 20px 20px;">
                <br class="clear"/>
                <div class="row" style="margin-left: 0px;">
                    <h5>Reports</h5>
                    <hr />
                    <br class="clear"/>
                    <div class="span3">
                        <div class="panel panel-default">
                            <div class="panel-heading">Moderator Report
                            </div>
                            <div class="panel-body">
                                <center>
                                    <select style="width: 200px;" class="btn btn-default" id="moderator">
                                        <option value="-1">-Select Moderator-</option>
                                        <c:forEach var="mode" items="${moderators}">
                                            <option value="${mode.identificationNumber}">
                                                ${mode.name}
                                            </option>
                                        </c:forEach>
                                    </select>   
                                    <br />
                                    <br />
                                    <select style="width: 200px;" class="btn btn-default" id="type">
                                        <option value="-1">-Select Type-</option>
                                        <option value="transaction">Transaction</option>
                                        <option value="profile">Profile request</option>
                                    </select>        
                               <br />
                               <br />
                                <a id="anchor" class="btn btn-primary" style="height: 25px; padding: 2px 15px;" onclick="return moderatorReport();">Generate</a>
                                </center>
                                <br />
                                <p style="color: green; text-align: center;">
                                Selecting a moderator will let you know complete transaction approved by 
                                Moderator like vee credit transaction approved and profile change request 
                                approved by him/her.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="span3">
                        <div class="panel panel-default">
                            <div class="panel-heading">User Report</div>
                            <div class="panel-body">
                                <center>
                                    <select class="btn btn-default" id="bonus">
                                        <option value="-1">--Select Bonus Type--</option>
                                        <option value="1">Residual Bonus</option>
                                        <option value="0">Direct Refferal bonus</option>
                                        <option value="3">E-Rental bonus</option>
                                        <option value="2">Matching Bonus</option>
                                    </select>
                                    <br /><br />
                                    <a id="anchor2" class="btn btn-primary" style="height: 25px; padding: 2px 15px;" onclick="return checkBonus();">Generate</a>
                                </center>
                                <br />
                                <p style="color: green; text-align: center;">
                                    In user Report you will get information of bonus given to users.
                                    which trader has got what amount of bonus and which type of bonus,
                                    and its respective details.
                                </p>
                            </div>
                        </div>
                    </div>                    
                    <div class="span3">
                        <div class="panel panel-default">
                            <div class="panel-heading">System Report</div>
                            <div class="panel-body">
                                <center>
                                    <a class="btn btn-primary" style="height: 25px; padding: 2px 15px;" href="/veepropbeta/system/report">Generate</a>
                                </center>
                                <br />
                                <p style="color: green; text-align: center;">
                                    Under this option, you will be able to see current status of the system
                                    and data amount in system like no of player moderator etc. 
                                    Also it provide you mini-lot entries.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>