
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Veeprop</title>
        <link rel="stylesheet" href="/veepropbeta/css/reset.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="/veepropbeta/css/style_admin.css" type="text/css" media="screen" />
        <link href='http://fonts.googleapis.com/css?family=Open+Sans|Baumans' rel='stylesheet' type='text/css'>
        <script src="http://code.jquery.com/jquery-1.7.2.min.js"></script>

        <style>
            table tr th, table tbody tr td {
                border: 1px black solid;
                padding: 0 20px;
            }
            table tr {
                border: 1px black solid;
            }
            table {
                border: 1px black solid;
                text-align: center;
            }
        </style>
    </head>

    <body>
        <section id="spacer">
            <p>Veeprop Moderator Panel</p>
            <div style="    margin-left: 1200px;margin-top: 16px;padding-top: 20px;color: #000;"><a href="/veepropbeta/logout" style="margin-left: 30px;">Sign Out</a></div>
        </section>
        <section id="text_column_admin">
            <section id="boxcontent_admin">
                <div class="sell_board"><span class="sb">Moderator Tasks [${name}]</span></div>
                <div class="ctg"><span class="ctg_p">Daily Task</span></div>  
                
                <article class="link_grp">
                    <ul>
                        <li><a href="/veepropbeta/home">Overview</a></li>
                        <li><a href="/veepropbeta/pendingshare">View Share Transaction</a></li>
                        <li><a href="/veepropbeta/pendingvcredit">Pending vCredit Transaction</a></li>
                        <li><a href="/veepropbeta/pendingprofiles">Pending Profile Upgrade Request</a></li>
                        <li><a href="/veepropbeta/introduce">Binary Tree</a></li>
                        <li><a href="/veepropbeta/home">Block Player</a></li>
                        <li><a href="/veepropbeta/disputecases">Complaints</a></li>
                    </ul>
                </article>
                
                <div class="ctg"><span class="ctg_p">Moderator Profile</span></div>
                
                <article class="link_grp">
                    <ul>
                        <li><a href="/veepropbeta/moderator/profile/0">Profile</a></li>
                        <li><a href="/veepropbeta/moderator/profile/1">Edit Profile</a></li>
                        <li>Report Problem</li>
                    </ul>
                </article>
                
                <br class="clear"/>
            </section>
            
            <article class="column1">                
                <br class="clear"/>
                <div style="  margin-top: 20px; ">
                    <table style="text-align: center;">
                        <tr>
                            <th style="width: 60px;">Seller ID</th>
                            <th style="width: 60px;">Buyer ID</th>
                            <th style="width: 150px;">Vee Credit Amount </th>
                            <th style="width: 160px;">Check Detail</th>
                        </tr>
                        <c:if test="${empty disputeForModerator}">
                            <tr>
                                <td colspan="4"> No Dispute Yet</td>
                            </tr>
                        </c:if>
                        <c:forEach var="dfm" items="${disputeForModerator}">
                            <tr>
                                <td>${dfm.sellerIN}</td>
                                <td>${dfm.buyerIN}</td>
                                <td>${dfm.veeCreditAmount}</td>
                                <td><a href="/veepropbeta/disputedetail/${dfm.veeCreditTransactionId}" style="color: blue;">check and action</a></td>
                            </tr>
                        </c:forEach>
                    </table>
                </div>
            </article>
        </section>
    </body>
</html>