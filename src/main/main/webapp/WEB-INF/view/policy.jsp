
<%-- 
    Document   : contact
    Created on : Jan 29, 2014, 4:39:42 PM
    Author     : CoreTechies
--%>
  <%@page contentType="text/html" pageEncoding="windows-1252"%> 
  <%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core" %>
  <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
 
<html>
 <head>
  <title>About Us</title>
     <link rel="stylesheet" href="/veepropbeta/css/style.css" type="text/css" media="screen" />
     <link href="/veepropbeta/css/lightbox.css" rel="stylesheet" />
     <link href='http://fonts.googleapis.com/css?family=Open+Sans|Baumans' rel='stylesheet' type='text/css'>
     <link rel="stylesheet" href="/veepropbeta/css/bootstrap-responsive.css" type="text/css" media="screen" />
     <link rel="stylesheet" href="/veepropbeta/css/bootstrap-responsive.min.css" type="text/css" media="screen" />
     <link rel="stylesheet" href="/veepropbeta/css/bootstrap.css" type="text/css" media="screen" />
     <link rel="stylesheet" href="/veepropbeta/css/bootstrap.min.css" type="text/css" media="screen" />
     
 </head>      
  <body>
      <%@include file="header.jsp" %>
      <%@include file="Lotinformation.jsp" %>
         
       
    <div class="row">
        <a  class="list-group-item active"><b>Policy</b></a>
        <a class="list-group-item ">
          <p>
              <b>Company Policy</b><br>
              <u>Privacy Statement</u><br>
The protection of privacy and the safeguarding of our Player�s personal information is our highest priority. Please view the following Privacy Statement which clearly explains how we, at Veeprop, collect, process, store and protect the Players�s information.
By opening an account, the Player hereby gives Veeprop its consent to such collection, processing, storage and use of personal information by our company as explained below.

<br><u>Personal Information</u><br>
Veeprop collects the necessary information required to open, transact and safeguard your game account and your privacy and to provide you with the services you require. To perform this efficiently, we gather information from you and may, in certain circumstances, gather information from relevant banks and/or credit agencies, and/or other sources which help us to create a detailed profile for your requirements and preferences and to provide better services to you.
The information Veeprop collects includes information required to communicate with and to identify its Players. We may also collect certain demographic information, including, birth date, education, occupation, etc.
<br>
<u>Usage of Personal Information</u><br>
Veeprop uses personal information only as required to provide quality service and security to you. This information helps improve services, customize browsing experience and enables us to inform you of additional products, services or promotions relevant to you and the products and services you require.
<br>
<u>Affiliates & Partners</u><br>
Veepropr may share information with affiliates and credit reporting or collection agencies in the event such information is reasonably required by such affiliate in order to provide the products or services to you. It shall be delivered in a manner that is useful and relevant only where you have authorized us to do so.

<br><u>Non-affiliated third parties</u><br>
Veeprop does not sell, license, lease or otherwise disclose personal information to third parties, except as described in this Privacy Statement. We reserve the right to disclose personal information to third parties where required by law, regulatory, law enforcement or other government authority.
To help us improve our services, Veeprop may engage third parties to help carry out certain internal functions such as account processing, fulfillment, client service, client satisfaction surveys or other data collection activities relevant to our business. Use of the shared information is strictly limited to the performance of the above and is not permitted for any other purpose. All third parties, with which we share personal information, are required to protect such personal information in accordance with all relevant legislation. We will not share personal information with third parties which it considers is not able to provide Players the required level of protection.
You acknowledge that in order to provide services to you, it may be necessary for information to be transferred outside of the land of incorporation of Veeprop and you consent to such information transfer.

<br><u>Cookies</u></br>
Veeprop uses cookies to secure your trading activities and to enhance the performance of the web site. Cookies used by Veeprop do not contain personal information or other sensitive information.
Veeprop may share certain website usage statistics with reputable advertising companies and with its affiliated marketing companies. The information collected by the advertising company is not personally identifiable.

<br><u>Privacy statements updates</u></br>
From time to time, Veeprop may update this Privacy Statement. In the event our company materially changes this Privacy Statement including how we collect, process or use your personal information, the revised Privacy Statement will be posted to the web site. Players agrees to accept posting of a revised Privacy Statement electronically on the web site as actual notice to Players. Any dispute over our Privacy Statement is subject to this notice and our Customer Agreement. Veeprop encourages Player to periodically check back and review this policy so that Player always will know what information veeprop.com collects, how our company uses these information, and to whom we may disclose it to.




          </p>
      </a>
    </div>   
    <%@include file="footer.jsp" %>      
</body>
</html>