

<!DOCTYPE HTML>
<html>

<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

    <head>

        <style type="text/css">
            a{
                cursor: pointer;
            }
        </style>
        <link rel="stylesheet" href="/veepropbeta/css/style.css" type="text/css" media="screen" />
        <link href='http://fonts.googleapis.com/css?family=Open+Sans|Baumans' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="/veepropbeta/css/bootstrap-responsive.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="/veepropbeta/css/bootstrap-responsive.min.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="/veepropbeta/css/bootstrap.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="/veepropbeta/css/bootstrap.min.css" type="text/css" media="screen" />
        <link rel="stylesheet" type="text/css" href="/veepropbeta/css/tree.css" />
         <script src="http://code.jquery.com/jquery-1.7.2.min.js"></script>
        <link rel="stylesheet" type="text/css" href="/veepropbeta/css/style_light.css">
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>
        <script src="/veepropbeta/js/jquery-ui-1.8.14.custom.min.js" type="text/javascript"></script>
        <script src="/veepropbeta/js/ttw-notification-menu.min.js" type="text/javascript"></script>
        <script src="/veepropbeta/js/demo.js" type="text/javascript"></script>      
        <style type="text/css">
        .tooltip {
            width: 250px;
            font-size: 11px;
            font-family: Arial, sans-serif;
            background: #444;
            border: 1px solid #090909;
            border-radius: 4px;
            -moz-border-radius: 4px;
            -webkit-border-radius: 4px;
            position: absolute;
            z-index: 1;
            box-shadow: 0 2px 5px rgba(0, 0, 0, 0.5);
            -webkit-box-shadow: 0 2px 5px rgba(0, 0, 0, 0.5);
            -moz-box-shadow: 0 2px 5px rgba(0, 0, 0, 0.5);
            color:#fff;
            padding:12px 24px;
            line-height:18px;
        }

        .tooltip:after {
            content: '';
            position: absolute;
            border-color: transparent  #444 transparent transparent;
            border-style: solid;
            border-width: 10px;
            height: 0;
            width: 0;
            position: absolute;
            left: 0;
            top: 50%;
            margin-top: -10px;
            margin-left: -20px;
        }

        .tooltip:before {
            content: '';
            position: absolute;
            border-color:   transparent #090909 transparent transparent;
            border-style: solid;
            border-width: 10px;
            height: 0;
            width: 0;
            position: absolute;
            left: 0;
            top: 50%;
            margin-top: -10px;
            margin-left: -20px;
        }

        .ttw-notification-menu{
                width: 276px;
        }


    </style>
   <script type="text/javascript">
	function extend(id){
            var url = "/veepropbeta/retrieveChild/"+id+"/";
            var registerChild = "/veepropbeta/registerChild/"+id;
            $.get(url, function(data) {
                if(data){
                    var len = data.length;
                    if(len===4){
                        if(data[0]===null){
                            var html = '<li><a href="'+registerChild+'/0">+</a></li>'
                                                 +'<li><a id="'+data[2]+'" onclick="return extend(this.id);">'+data[3]+'</a><ul class="'+data[2]+'"></ul></li>';
                            $('.'+id).html(html);
                        }else{
                            var html = '<li><a id="'+data[0]+'" onclick="return extend(this.id);">'+data[1]+'</a><ul class="'+data[0]+'"></ul></li>'
                                                 +'<li><a id="'+data[2]+'" onclick="return extend(this.id);">'+data[3]+'</a><ul class="'+data[2]+'"></ul></li>';
                            $('.'+id).html(html);
                        }
                    }else if(len===2){
                            var html = '<li><a id="'+data[0]+'" onclick="return extend(this.id);">'+data[1]+'</a><ul class="'+data[0]+'"></ul></li>'
                                                 +'<li><a href="'+registerChild+'/1">+</a></li>';
                            $('.'+id).html(html);

                    }else{
                        var html = '<li><a href="'+registerChild+'/0" >+</a></li>'
                        +'<li><a href="'+registerChild+'/1" >+</a></ul></li>';
                        $('.'+id).html(html);
                    }
                }else{
                    var html = '<li><a href="'+registerChild+'/0" >+</a></li>'
		 			 +'<li><a href="'+registerChild+'/1" >+</a></ul></li>';
		 $('.'+id).html(html);
                }
            });
	}
</script>    

        <script src="http://code.jquery.com/jquery-1.7.2.min.js"></script>
        <script type="text/javascript">
            function searchTrader(){
                var x = $("#search").val();
                x = x.replace('.','&');
                alert(x);
                var url ="/veepropbeta/search/trader/"+x;
                $.get(url, function(data){
                   if(data){
                       $("#body").html(data);
                   }else{
                       alert("No user found with "+x);
                   } 
                });
            }
    </script>
    </head>
<body id="body">
    <input type="hidden" id="notification-form" />   
       <%@include file="header.jsp" %>
      
       <%@include file="information.jsp" %>
     <c:forEach var="notification" items="${notification}" varStatus="status">
        <script type="text/javascript">
            $(document).ready(function(e){
            $("#notification-form").val('<a href="${notification.notificationLink}/${notification.notificationId}">${notification.notification}</a>');
            $("#notification-form").click();
        });
        </script>
    </c:forEach>
    <div class="container-fluid" style="margin-top: -10px;">
        <div class="row-fluid">
            <%@include  file="sellboard.jsp" %>
            <div class="span6">                
                <ul class="nav nav-pills">
                    <li><a href="/veepropbeta/home">Dashboard</a></li>
                    <li><a href="/veepropbeta/profile">Profile</a></li>
                    <li><a href="/veepropbeta/wallet">Wallet</a></li>
                    <li><a href="/veepropbeta/business">Biz Management</a></li>
                    <li><a href="/veepropbeta/feedback">Feedback</a></li>
                      <li><a id="projects" class="notification-menu-item first-item list_inline" style="float: right;">Notifications</a></li>
                </ul>
                <a href="/veepropbeta/wallet" class="list-group-item active" style="margin-top: 2%;">Your Bussiness Tree</a>
                <div class="col-md-6 offset3" style="margin-top: 2%;">
                    <input  type="text" class="form-control"  placeholder="Search Here" id="search" style="margin-left: 33%;" />
                </div>
                <div class="col-md-1 offset0" style="margin-top: 2%;">
                    <button type="submit" class="btn btn-primary" onclick="return searchTrader();">Search</button>
                </div>
                <c:if test="${userInfo == null}">
                    ${note}
                </c:if>
                <c:if test="${position != null}">
                    Trader is on your ${position}
                </c:if>
                <c:if test="${userInfo != null}">
                    <div class="tree">
                        <ul>
                            <li>
                                    <a id="${userInfo.identificationNumber}" onclick="return extend(this.id);"> ${userInfo.name}</a>
                                <ul class="${userInfo.identificationNumber}"></ul>
                            </li>
                        </ul>
                    </div> 
                </c:if>
            </div>
             <%@include  file="noticeboard.jsp" %>     
        </div>
    </div>
    <%@include file="footer.jsp" %>
</body>
</html>




