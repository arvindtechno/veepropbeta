
<!DOCTYPE HTML>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Veeprop</title>
<link href='http://fonts.googleapis.com/css?family=Open+Sans|Baumans' rel='stylesheet' type='text/css'>

<link rel="stylesheet" href="/veepropbeta/css/bootstrap.css" type="text/css" media="screen" />
<link rel="stylesheet" href="/veepropbeta/css/bootstrap-responsive.css" type="text/css" media="screen" />    
<style>
            label.valid {
              width: 24px;
              height: 24px;
              background: url() center center no-repeat;
              display: inline-block;
              text-indent: -9999px;
              }
              label.error {
              font-weight: bold;
              color: red;
              padding: 2px 8px;
              margin-top: 2px;
              }
              .error{
               color: red;
               font-weight: bold;
              }
      
         </style>

          <script src="/veepropbeta/js/jquery-1.9.1.min.js"></script>
          <script src="/veepropbeta/js/jquery.validate.js"></script>
            <script>
                $(document).ready(function(){
                
                $('#contact-form').validate(
                {
                rules: {
                question: {
                minlength: 20,
                required: true
                },
                ans: {
                required: true,
                minlength:25
                }
                },
                highlight: function(element) {
                $(element).closest('.control-group').removeClass('success').addClass('error');
                },
                success: function(element) {
                element
                .text('OK!').addClass('valid')
                .closest('.control-group').removeClass('error').addClass('success');
                }
                });
                }); 
        </script>
</head>
<body style="padding-top: 50px; font-family: sans-serif; color: #999;">
    <div class="container">    
        <div class="navbar navbar-fixed-top" style="padding: 0 20px 15px; background-color: #FDD35F; border-bottom: 1px solid #eea236;">
            <h4 style=" color: #000;">Veeprop Admin Panel</h4>
            <div class="navbar-inner pull-right">
                <ul class="nav nav-pills" style>

                <li><a href="/veepropbeta/home" style=" color: #000;">Dashboard</a></li>
                <li><a href="/veepropbeta/logout" style=" color: #000;">Logout</a></li>
            </ul>
            </div>
        </div>
    </div>
   
    <div class="container-fluid" style="padding-top:10px;">
        <div class="row" style="margin-top: 25px; border-bottom: 1px solid #eeeeee;">
            <div class="span3" style="float: left;border-right: 1px solid #eeeeee;border-bottom: 1px solid #eeeeee;">
                <ol class="breadcrumb">
                    <li>Admin Panel</li>
                </ol>

                <div class="navbar">
                  <ul class="nav nav-list">
                      <br class="clear"/>
                      <span class="light_head">Dashboard</span>
                      <hr />
                      <li><a href="/veepropbeta/home">Overview</a></li>
                      <li><a href="/veepropbeta/moderator/report">Reports</a></li>
                      <li><a href="/veepropbeta/moderator/profile/0">Profile</a></li>
                      <li><a href="/veepropbeta/announcements">Announcements</a></li>
                  </ul>
                </div>
                <div class="navbar">
                  <ul class="nav nav-list">
                      <span class="light_head"> Moderator</span>
                      <hr />
                      <li><a href="/veepropbeta/addmoderator">Add a Moderator</a></li>
                      <li><a href="/veepropbeta/viewmoderator">Active/Inactive a Moderator</a></li>
                  </ul>
                </div>
                <div class="navbar">
                  <ul class="nav nav-list">
                      <span class="light_head">Analysis</span>
                      <hr />
                      <li><a href="/veepropbeta/admin/feedbacks">Feedbacks</a></li>
                      <li><a href="/veepropbeta/contacts">Contacts</a></li>
                      <li><a href="/veepropbeta/faqadmin">FAQ</a></li>
                  </ul>
                </div>
            </div>
            <div class="span10" style=" overflow:auto; padding: 20px 20px;">
            <div>
            <h3>Please Fill the FAQ :-</h3>
            
            <form:form commandName="faqForm" action="/veepropbeta/faqadmin" id="" class="form-horizontal" method="POST"  >
                <fieldset>
                    <div class="control-group">
                     <form:label path="question" class="control-label" for="">Question</form:label>
                    <div class="controls">
                    <form:textarea path="question" class="input-xlarge" name="" id="question"></form:textarea>
                    <form:errors path="question" element="span" cssClass="error"/>
                    </div>
                    </div>
                    <div class="control-group">
                    <form:label path="answer" class="control-label" for="">Answer</form:label>
                    <div class="controls">
                    <form:textarea path="answer" class="input-xlarge" name="" id="ans"></form:textarea>
                    <form:errors path="answer" cssClass="error" element="span"/>
                    </div>
                    </div>
                    <div class="form-actions">
                    <button type="submit" class="btn btn-primary btn-success">Submit</button>
                    <button type="reset" class="btn">Cancel</button>
                    </div>
                
                </fieldset>   
            </form:form>
            
        </div>





         
            </div>
        </div>
    </div>
</body>
</html>