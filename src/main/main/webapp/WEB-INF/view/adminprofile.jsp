
<!DOCTYPE HTML>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Veeprop</title>
<link href='http://fonts.googleapis.com/css?family=Open+Sans|Baumans' rel='stylesheet' type='text/css'>

<link rel="stylesheet" href="/veepropbeta/css/bootstrap.css" type="text/css" media="screen" />
<link rel="stylesheet" href="/veepropbeta/css/bootstrap-responsive.css" type="text/css" media="screen" />    
        <style>
            <c:if test="${mode==1}">
            table tr th, table tbody tr td {
                padding: 0 20px;
            }
            table {
                text-align: center;
            }
            table tr th{
                font-weight: bold;
            }
            input{
                border: 1px black solid;
                width: 180px;
                height: 18px;
                margin: 3px;
                font-size:14px;
            } 
            select{
                border: 1px black solid;
                height: 30px;
                width : 200px;
            }
            </c:if>
            <c:if test="${mode!=1}">
            table tr th, table tbody tr td {
                border: 1px black solid;
                padding: 0 20px;
            }
            table tr {
                border: 1px black solid;
            }
            table {
                border: 1px black solid;
                text-align: center;
            }
            table tr th{
                font-weight: bold;
            }
            input{
                border: 1px black solid;
                width: 180px;
                height: 18px;
                margin: 3px;
                font-size:14px;
            }                
            </c:if>
        </style>
</head>
<body style="padding-top: 50px; font-family: sans-serif; color: #999;">
    <div class="container">    
        <div class="navbar navbar-fixed-top" style="padding: 0 20px 15px; background-color: #FDD35F; border-bottom: 1px solid #eea236;">
            <h4 style=" color: #000;">Veeprop Admin Panel</h4>
            <div class="navbar-inner pull-right">
                <ul class="nav nav-pills" style>

                <li><a href="/veepropbeta/home" style=" color: #000;">Dashboard</a></li>
                <li><a href="/veepropbeta/logout" style=" color: #000;">Logout</a></li>
            </ul>
            </div>
        </div>
    </div>
   
    <div class="container-fluid" style="padding-top:10px;">
        <div class="row" style="margin-top: 25px; border-bottom: 1px solid #eeeeee;">
            <div class="span3" style="float: left;border-right: 1px solid #eeeeee;border-bottom: 1px solid #eeeeee;">
                <ol class="breadcrumb">
                    <li>Admin Panel</li>
                </ol>

                <div class="navbar">
                  <ul class="nav nav-list">
                      <br class="clear"/>
                      <span class="light_head">Dashboard</span>
                      <hr />
                      <li><a href="/veepropbeta/home">Overview</a></li>
                      <li><a href="/veepropbeta/moderator/report">Reports</a></li>
                      <li><a href="/veepropbeta/moderator/profile/0">Profile</a></li>
                      <li><a href="/veepropbeta/announcements">Announcements</a></li>
                  </ul>
                </div>
                <div class="navbar">
                  <ul class="nav nav-list">
                      <span class="light_head"> Moderator</span>
                      <hr />
                      <li><a href="/veepropbeta/addmoderator">Add a Moderator</a></li>
                      <li><a href="/veepropbeta/viewmoderator">Active/Inactive a Moderator</a></li>
                  </ul>
                </div>
                <div class="navbar">
                  <ul class="nav nav-list">
                      <span class="light_head">Analysis</span>
                      <hr />
                      <li><a href="/veepropbeta/admin/feedbacks">Feedbacks</a></li>
                      <li><a href="/veepropbeta/contacts">Contacts</a></li>
                      <li><a href="/veepropbeta/faqadmin">FAQ</a></li>
                  </ul>
                </div>
            </div>
            <div class="span10" style=" overflow:auto; padding: 20px 20px;">
                <div style="  margin-top: 20px;  ">
                    <c:if test="${mode!=1}">
                    <table style="float: left; margin-right: 10px;">
                        <tr>
                            <th style="width: 120px;">Name</th>
                            <td style="width: 200px;">${moderator.name}</td>
                        </tr>
                        <tr>
                            <th>Email</th>
                            <td>${moderator.email}</td>
                        </tr>
                        <tr>
                            <th>Contact</th>
                            <td>${moderator.contact}</td>
                        </tr>
                        <tr>
                            <th>Gender</th>
                            <td><c:if test="${moderator.gender==0}">Male</c:if>
                                <c:if test="${moderator.gender==1}">Female</c:if>
                            </td>
                        </tr>
                        <tr>
                            <th>Date of Birth</th>
                            <td>${moderator.dateOfBirth}</td>
                        </tr>
                        <tr>
                            <th>Security Question</th>
                            <td>${question}</td>
                        </tr>
                        <tr>
                            <th>Answer</th>
                            <td>${moderator.securityAnswer}</td>
                        </tr>
                        <tr>
                            <th style="width: 120px;">House No</th>
                            <td style="width: 200px;">${moderator.houseNumber}</td>
                        </tr>
                        <tr>
                            <th>Street Name 1</th>
                            <td>${moderator.streetName1}</td>
                        </tr>
                        <tr>
                            <th>Street Name 2</th>
                            <td>${moderator.streetName1}</td>
                        </tr>
                        <tr>
                            <th>City</th>
                            <td>${moderator.city}</td>
                        </tr>
                        <tr>
                            <th>Landmark</th>
                            <td>${moderator.landmark}</td>
                        </tr>
                        <tr>
                            <th>State</th>
                            <td>${moderator.userstate}</td>
                        </tr>
                        <tr>
                            <th>Country</th>
                            <td>${moderator.country}</td>
                        </tr>
                        <tr>
                            <th>Zip Code</th>
                            <td>${moderator.zipcode}</td>
                        </tr>
                    </table>
                        <div><a class="btn-primary btn" href="/veepropbeta/moderator/profile/1">Edit Profile</a></div>
                    </c:if>
                    <c:if test="${mode==1}">
                        <form:form action="/veepropbeta/moderator/profile" commandName="moderator" method="POST">
                            <table style="float: left; margin-right: 10px;">
                                <tr>
                                    <th style="width: 200px;">Name</th>
                                    <td style="width: 200px;"><form:input path="name"/></td>
                                </tr>
                                <tr>
                                    <th>Email</th>
                                    <td><form:input path="email"/></td>
                                </tr>
                                <tr>
                                    <th>Contact</th>
                                    <td><form:input path="contact"/></td>
                                </tr>
                                <tr>
                                    <th>Gender</th>
                                    <td style="text-align: left;">
                                        <form:radiobutton path="gender" value="0" cssStyle="width: 3px;" id="0"/> <form:label path="gender" for="0">Male</form:label>
                                        <form:radiobutton path="gender" value="1" cssStyle="width: 3px;" id="1"/><form:label path="gender" for="1">Female</form:label>
                                    </td>
                                </tr>
                                <tr>
                                    <th>Date of Birth</th>
                                    <td><form:input path="dateOfBirth" /></td>
                                </tr>
                                <tr>
                                    <th>Security Question</th>
                                    <td>
                                        <form:select path="securityQuestionId">
                                            <c:forEach var="item" items="${questions}">
                                                <form:option value="${item.securityQuestionId}">${item.securityQuestion}</form:option>
                                            </c:forEach>
                                        </form:select>
                                    </td>
                                </tr>
                                <tr>
                                    <th>Answer</th>
                                    <td><form:input path="securityAnswer"/></td>
                                </tr>
                                <tr>
                                    <th style="width: 120px;">House No</th>
                                    <td style="width: 200px;"><form:input path="houseNumber"/></td>
                                </tr>
                                <tr>
                                    <th>Street Name 1</th>
                                    <td><form:input path="streetName1"/></td>
                                </tr>
                                <tr>
                                    <th>Street Name 2</th>
                                    <td><form:input path="streetName2"/></td>
                                </tr>
                                <tr>
                                    <th>City</th>
                                    <td><form:input path="city"/></td>
                                </tr>
                                <tr>
                                    <th>Landmark</th>
                                    <td><form:input path="landmark"/></td>
                                </tr>
                                <tr>
                                    <th>State</th>
                                    <td><form:input path="userstate"/></td>
                                </tr>
                                <tr>
                                    <th>Country</th>
                                    <td><form:input path="country"/></td>
                                </tr>
                                <tr>
                                    <th>Zip Code</th>
                                    <td><form:input path="zipcode"/></td>
                                </tr>
                                <tr>
                                    <td colspan="2">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td ></td>
                                    <td style="text-align: right;"><button>Update Profile</button></td>
                                </tr>
                            </table>
                                
                        </form:form>
                    </c:if>
                </div>
                
            </div>
        </div>
    </div>
</body>
</html>