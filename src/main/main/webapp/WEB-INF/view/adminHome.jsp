
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Veeprop</title>
<link href='http://fonts.googleapis.com/css?family=Open+Sans|Baumans' rel='stylesheet' type='text/css'>

<link rel="stylesheet" href="/veepropbeta/css/bootstrap.css" type="text/css" media="screen" />
<link rel="stylesheet" href="/veepropbeta/css/bootstrap-responsive.css" type="text/css" media="screen" />    
       
</head>
<body style="padding-top: 50px; font-family: sans-serif; color: #999;">
    <div class="container">    
        <div class="navbar navbar-fixed-top" style="padding: 0 20px 15px; background-color: #FDD35F; border-bottom: 1px solid #eea236;">
            <h4 style=" color: #000;">Veeprop Admin Panel</h4>
            <div class="navbar-inner pull-right">
                <ul class="nav nav-pills" style>

                <li><a href="/veepropbeta/home" style=" color: #000;">Dashboard</a></li>
                <li><a href="/veepropbeta/logout" style=" color: #000;">Logout</a></li>
            </ul>
            </div>
        </div>
    </div>
   
    <div class="container-fluid" style="padding-top:10px;">
        <div class="row" style="margin-top: 25px; border-bottom: 1px solid #eeeeee;">
            <div class="span3" style="float: left;border-right: 1px solid #eeeeee;border-bottom: 1px solid #eeeeee;">
                <ol class="breadcrumb">
                    <li>Admin Panel</li>
                </ol>

                <div class="navbar">
                  <ul class="nav nav-list">
                      <br class="clear"/>
                      <span class="light_head">Dashboard</span>
                      <hr />
                      <li><a href="/veepropbeta/home">Overview</a></li>
                      <li><a href="/veepropbeta/moderator/report">Reports</a></li>
                      <li><a href="/veepropbeta/moderator/profile/0">Profile</a></li>
                      <li><a href="/veepropbeta/announcements">Announcements</a></li>
                  </ul>
                </div>
                <div class="navbar">
                  <ul class="nav nav-list">
                      <span class="light_head"> Moderator</span>
                      <hr />
                      <li><a href="/veepropbeta/addmoderator">Add a Moderator</a></li>
                      <li><a href="/veepropbeta/viewmoderator">Active/Inactive a Moderator</a></li>
                  </ul>
                </div>
                <div class="navbar">
                  <ul class="nav nav-list">
                      <span class="light_head">Analysis</span>
                      <hr />
                      <li><a href="/veepropbeta/admin/feedbacks">Feedbacks</a></li>
                      <li><a href="/veepropbeta/contacts">Contacts</a></li>
                      <li><a href="/veepropbeta/faqadmin">FAQ</a></li>
                  </ul>
                </div>
            </div>
            <div class="span10" style=" overflow:auto; padding: 20px 20px;">
                <div class="span9">
                    <h5>Overview</h5>
                <hr></hr>
                <br class="clear"/>
                    <div class="panel panel-default">
                    <div class="panel-heading active">Overview</div>
                    <div class="panel-body">
                        <div style="width: 30%; float: left;">
                        <span style="width: 20%">Current Generation :</span> ${lot.generationNumber}
                        <br />
                        <br />
                        <span style="width: 20%">Current Lot :</span> ${lot.lotNumber}
                        <br />
                        <br />
                        <span style="width: 20%">Current Share Price :</span> ${lot.pricePerShare}
                        <br />
                        <br />
                        <span style="width: 20%">Number Of Trader :</span> ${trader}
                        <br />
                        <br />
                        <span style="width: 20%">Number of Moderator:</span> ${moderator}
                        </div>
                        <table style="float: left;" class="table table-hover">
                          <thead>  <tr>
                            <th>Generation Number</th>
                            <th>Lot Number</th>
                            <th>Share Price</th>
                            <th>Release Date</th>
                       
                        </tr> </thead>
                        <c:forEach var="l" items="${lots}">
                        <tbody>
                        <tr>
                            <td>${l.generationNumber}</td>
                            <td>${l.lotNumber}</td>
                            <td>${l.pricePerShare}</td>
                            <td>${l.releaseDate}</td>
                        </tr>
                        </tbody>
                        </c:forEach>
                    </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>