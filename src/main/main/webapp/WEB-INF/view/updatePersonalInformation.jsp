<%-- 
    Document   : profile
    Created on : 11 Feb, 2014, 3:02:47 PM
    Author     : aamir khan
--%>

<%@taglib  prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE HTML>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="/veepropbeta/css/style.css" type="text/css" media="screen" />
    <link href='http://fonts.googleapis.com/css?family=Open+Sans|Baumans' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="/veepropbeta/css/bootstrap-responsive.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="/veepropbeta/css/bootstrap-responsive.min.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="/veepropbeta/css/bootstrap.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="/veepropbeta/css/bootstrap.min.css" type="text/css" media="screen" />
    <script src="http://code.jquery.com/jquery-1.7.2.min.js"></script>
    <link rel="stylesheet" type="text/css" href="/veepropbeta/css/style_light.css">
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>
    <script src="/veepropbeta/js/jquery-ui-1.8.14.custom.min.js" type="text/javascript"></script>
    <script src="/veepropbeta/js/ttw-notification-menu.min.js" type="text/javascript"></script>
    <script src="/veepropbeta/js/demo.js" type="text/javascript"></script>      
    <style type="text/css">
     .tooltip {
            width: 250px;
            font-size: 11px;
            font-family: Arial, sans-serif;
            background: #444;
            border: 1px solid #090909;
            border-radius: 4px;
            -moz-border-radius: 4px;
            -webkit-border-radius: 4px;
            position: absolute;
            z-index: 1;
            box-shadow: 0 2px 5px rgba(0, 0, 0, 0.5);
            -webkit-box-shadow: 0 2px 5px rgba(0, 0, 0, 0.5);
            -moz-box-shadow: 0 2px 5px rgba(0, 0, 0, 0.5);
            color:#fff;
            padding:12px 24px;
            line-height:18px;
        }

        .tooltip:after {
            content: '';
            position: absolute;
            border-color: transparent  #444 transparent transparent;
            border-style: solid;
            border-width: 10px;
            height: 0;
            width: 0;
            position: absolute;
            left: 0;
            top: 50%;
            margin-top: -10px;
            margin-left: -20px;
        }

        .tooltip:before {
            content: '';
            position: absolute;
            border-color:   transparent #090909 transparent transparent;
            border-style: solid;
            border-width: 10px;
            height: 0;
            width: 0;
            position: absolute;
            left: 0;
            top: 50%;
            margin-top: -10px;
            margin-left: -20px;
        }

        .ttw-notification-menu{
                width: 276px;
        }


    </style>
</head>
<body>
     <input type="hidden" id="notification-form" />   
    <%@include file="header.jsp" %>
    <%@include file="information.jsp" %>
    <c:forEach var="notification" items="${notification}" varStatus="status">
        <script type="text/javascript">
            $(document).ready(function(e){
            $("#notification-form").val('<a href="${notification.notificationLink}/${notification.notificationId}">${notification.notification}</a>');
            $("#notification-form").click();
        });
        </script>
    </c:forEach>  
    <div class="container-fluid" style="margin-top: -10px;">
        <div class="row-fluid">
         <%@include  file="sellboard.jsp" %>
        <div class="span6">
              <ul class="nav nav-pills">
                  <li><a href="/veepropbeta/home">Dashboard</a></li>
                <li><a href="/veepropbeta/profile">Profile</a></li>
                <li><a href="/veepropbeta/wallet">Wallet</a></li>
                <li><a href="/veepropbeta/business">Biz Management</a></li>
                 <li><a href="/veepropbeta/feedback">Feedback</a></li>
                <li><a id="projects" class="notification-menu-item first-item list_inline" style="float: right;">Notifications</a></li>
               </ul>
              <a href="/veepropbeta/updatePersonalInformation" class="list-group-item active" style="margin-top: 2%;">Your Address Details<b style="margin-left: 70%;">Update</b></a>
               <form:form action="/veepropbeta/updatePersonalInformation" id="contact-form" class="form-horizontal" method="POST" modelAttribute="showaddress" >
               
                <div class="form-group">
                <form:label for="inputEmail3" path="houseNumber" class="col-sm-3 control-label">House Number</form:label>
                <div class="col-sm-8">
                <form:input path="houseNumber" class="form-control" id="name" placeholder="please Enter House Number"/>
                <form:errors path="houseNumber" cssClass="error" element="span"/>
                </div>
                </div>
                
                <div class="form-group">
                <form:label for="inputEmail3" path="streetName1" class="col-sm-3 control-label">Street Name1</form:label>
                <div class="col-sm-8">
                <form:input path="streetName1" class="form-control" id="name" placeholder="Please Enter Street Name1"/>
                <form:errors path="streetName1" element="span" cssClass="error"/>
                </div>
                </div>
                
                <div class="form-group">
                <form:label for="inputEmail3" path="streetName2" class="col-sm-3 control-label">Street Name2</form:label>
                <div class="col-sm-8">
                <form:input path="streetName2" class="form-control" id="name" placeholder="Please Enter Street Name2"/>
                <form:errors path="streetName2" element="span" cssClass="error"/>
                </div>
                </div>
            
                <div class="form-group">
                <form:label for="inputEmail3" path="city" class="col-sm-3 control-label">City</form:label>
                <div class="col-sm-8">
                <form:input path="city" class="form-control" id="name" placeholder="Please Enter City Name"/>
                <form:errors path="city" element="span" cssClass="error"/>
                </div>
                </div>
                
                <div class="form-group">
                <form:label for="inputEmail3" path="landmark" class="col-sm-3 control-label">Landmark</form:label>
                <div class="col-sm-8">
                <form:input path="landmark" class="form-control" id="name" placeholder="Please Enter Landmark."/>
                <form:errors path="landmark" element="span" cssClass="error"/>
                </div>
                </div>
                
                <div class="form-group">
                <form:label for="inputEmail3" path="country" class="col-sm-3 control-label">Country</form:label>
                <div class="col-sm-8">
                <form:input path="country" class="form-control" id="name" placeholder="Please Select Country Name"/>
                <form:errors path="country" element="span" cssClass="error"/>
                </div>
                </div>
                
                <div class="form-group">
                <form:label for="inputEmail3" path="userstate" class="col-sm-3 control-label">State</form:label>
                <div class="col-sm-8">
                <form:input path="userstate" class="form-control" id="name" placeholder="Please Enter State Name"/>
                <form:errors path="userstate" element="span" cssClass="error"/>
                </div>
                </div>
                
                <div class="form-group">
                <form:label for="inputEmail3" path="zipcode" class="col-sm-3 control-label">Zip Code</form:label>
                <div class="col-sm-8">
                <form:input path="zipcode" class="form-control" id="name" placeholder="Please Enter Zipcode"/>
                <form:errors path="zipcode" element="span" cssClass="error"/>
                </div>
                </div>
                
                <div class="form-group" style="margin-bottom: 2%;">
                <div class="col-sm-2 offset3">
                <form:button  class="btn btn-success">Update</form:button>
                </div>
                <div class="col-sm-2">
                <button type="button" class="btn btn-default">Cancel</button>
                </div>
               </div>
     </form:form>
      </div>
        
            <%@include  file="noticeboard.jsp" %>
                 
        </div>
    </div>
  
 
    <%@include file="footer.jsp" %>
    
       
    
</body>
   
</html>
