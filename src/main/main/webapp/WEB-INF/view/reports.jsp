<%-- 
    Document   : reports
    Created on : 22 Feb, 2014, 4:41:19 PM
    Author     : sony
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        
        <link rel="stylesheet" href="bootstrap/css/bootstrap.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="bootstrap/css/bootstrap-responsive.css" type="text/css" media="screen" />
    </head>
    <body>
        <div class="span9">
            
            <div class="row">
                <h5>Reports</h5>
                <hr></hr>
                <br class="clear"/>
                <div class="span3">
                   <div class="panel panel-default">
  <div class="panel-heading">Moderator Report</div>
  <div class="panel-body">
    Panel content
  </div>
</div>
                </div>
                <div class="span3">
                    <div class="panel panel-default">
  <div class="panel-heading">Site Report</div>
  <div class="panel-body">
    Panel content
  </div>
</div>
                </div>
                <div class="span3">
                    <div class="panel panel-default">
  <div class="panel-heading">User Report</div>
  <div class="panel-body">
    Panel content
  </div>
</div>
                </div>
            </div>
        </div>
    </body>
</html>
