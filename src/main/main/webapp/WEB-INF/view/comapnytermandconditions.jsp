
<%-- 
    Document   : contact
    Created on : Jan 29, 2014, 4:39:42 PM
    Author     : CoreTechies
--%>
 
 
<html>
 <head>
     <title>Terms and Conditions of Company's</title>
     <link rel="stylesheet" href="/veepropbeta/css/style.css" type="text/css" media="screen" />
     <link href="/veepropbeta/css/lightbox.css" rel="stylesheet" />
     <link href='http://fonts.googleapis.com/css?family=Open+Sans|Baumans' rel='stylesheet' type='text/css'>
     <link rel="stylesheet" href="/veepropbeta/css/bootstrap-responsive.css" type="text/css" media="screen" />
     <link rel="stylesheet" href="/veepropbeta/css/bootstrap-responsive.min.css" type="text/css" media="screen" />
     <link rel="stylesheet" href="/veepropbeta/css/bootstrap.css" type="text/css" media="screen" />
     <link rel="stylesheet" href="/veepropbeta/css/bootstrap.min.css" type="text/css" media="screen" />
 </head>     
 
   <body>
      <%@include file="header.jsp" %>
         <section id="spacer">
            <h2 class="hidden">Notice Bar</h2>
            <p>New Stock has been updated..</p>
            <div class="search">
              <form action="#"></form>
            </div>
        </section>
    <div class="row">
        <a  class="list-group-item active"><b>Terms and Conditions</b></a>
        <a class="list-group-item ">
          <p>
             
              <strong>VEEPROP USER AGREEMENT</strong><br>
This User Agreement is the legal agreement between User (referred to individual who wishes to join and has actually joined Veeprop Pte Ltd (hereinafter referred to as ?Veeprop? or "This Company") as a user of Veeprop) and Veeprop Pte Ltd (hereinafter referred to as ?Veeprop? or "This Company"). Therefore, please read all the contents in this user agreement carefully before you register as a user of Veeprop. Due to situational changes in user, market condition and operation, Veeprop has the right to alter this user agreement and related terms of service. When amending this user agreement and related regulations, Veeprop website will announce the modified clause in the related web pages but will not give individual notification to the user separately. A user shall inquire for related notice in Veeprop official website during each log in into Veeprop website to read about changes to this User Agreement and other terms of service. The user understand and agree that: regardless whether this agreement is actually being studied carefully or not, as long as the user register as a member of Veeprop, the user are deemed to have agreed to this user agreement, various terms of service announced by Veeprop and also any amendments made to this agreement and various terms of service while be willing to be bonded by it. If any dispute occurs, the user cannot counter plead with the reason that he or she has not read the agreement carefully. 
Veeprop reserves the final interpretation right on this agreement and Veeprop has the ultimate right to determine whether the user violates this agreement or related regulations. This agreement, related terms of service, amendments made irregularly by FINNCITI to this agreement and related terms of service and also data saved in the system memory of Veeprop?s server are the judging standards for client's actions.

</p>
<b>ONE: COPYRIGHT STATEMENT</b><br>
<strong>Sources and Copyright of Website Content</strong>
<ol>
    
        <li>The copyright of any content, whether it is a trademark, design, text, image or other information, presented by any media in this website belongs to Veeprop, without the need for special instructions.</li>
       
        <li>"All rights reserved" marked in this site is the copyright statement with autonomy authority on the logo icon of the whole site, site design, site structure and also contents and images autonomously designed, compiled and produced by this site.</li>
       
        <li>Veeprop shares equally all the intellectual property rights, subject to related copyrights, trademarks, service trademark, patents, exclusive rights and other protections under intellectual property law.</li>
        <li>	Web services contents of Veeprop include: texts, software, images, charts and all contents in the advertisement; including other information provided by Veeprop to the user. All these contents are protected by the relevant law stated above.</li>
        
        <li>This site partially contains text content provided by other organization, group or commercial establishment. The copyrights of these contents belong to corresponding providers. This site has obtained permission from the copyright holders before extracting or reproducing these contents and recites the sources according to the agreement</li>
    
</ol>
<strong>Quoting Contents from This Website</strong>
<ol>
    <li>You can browse and download contents from this website for non-commercial purposes provided that the copyright law and related statement by Veeprop in the content of this website are complied. For commercial purposes (For example: copying, downloading, storing, or sending, converting, renting, presenting, disseminating, publicizing or any form of distributions of any content in this website through hardcopy or electronic-crawling system, or producing derivative product related to the content), a written permission from Veeprop and indicating that the source and copyright are trademarked by Veeprop when using the contents is necessary. Corresponding written application can be emailed to info@Veeprop.com. For websites and print media which violate related laws and regulations, disrespect the statement made by this website and conduct unauthorized use of contents from this website without permission from this website without indicating the sources, this website reserves the right to take legal actions and to hold related party accountable.</li>
    <li>Without clear written permission from Veeprop, no one shall copy or make mirrors on non-affiliated server.</li>
    <li>If you want to utilize contents provided by other organization, groups or commercial establishment contained in this website, please contact appropriate copyright holder directly. Any affair and legal responsibility related to this conduct are not related to this website.</li>
    <li>Any person and organization that are permitted to reproduced and spread all the contents in any means have to abide to the following conditions:
        <ol>
            <li>Extracts or Quotes shall reflect the actual meaning in the original document.</li>
            <li>The sources must be cited, namely the URL of this website: http://www.Veeprop.com.</li>
       </ol>
    </li>
 </ol>
<b>Notes on Linking</b><br>
<strong>If you want to provide link to this website on other websites, please abide to the following regulations:</strong>
<ol>
    <li>A formal agreement must be signed with Veeprop to link to this website.</li>
    <li>Unless having special agreement in the Veeprop Agreement, link must be directed to URL "http://www.Veeprop.com" but not any other pages of this website.</li>
    <li>The appearance, position and other aspects of the link must not damage or blur the name and trademark of Veeprop.</li>
    <li>The appearance, position and other aspects of the link must not lead viewers to false impression.</li>
    <li>When viewer activated the link, the content from this website must be presented in full screen but not in frame on the linked site.</li>
    <li>Veeprop can reclaim the right to link to this website at any time</li>
</ol>

<b>TWO: CONFIDENTIALITY INSTITUTION</b>
<p>Veeprop protects your online privacy seriously. For us, protecting your personal privacy is very important. Veeprop has made every effort to take your personal privacy protection problem into account when designing this website.
Veeprop guarantees that:
</p>
<ol>
    <li>Veeprop will not disclose your personal information obatained by this website. 
This website will not disclose personal information or personal contact methods of Client unless required by legal procedure. The purpose to register Client's personal information with the Company is to ensure that the Company can communicate smoothly with the Client (Client's email); to verify Client's identity with the Company when necessary (Identity Card Number) and etc.
</li>
<li>Veeprop will not sell your registration information (name, phone numbers, addresses and other information), unless you authorize us to do so.
This Company will not sell or exchange your name, addresses, phone number, email address or any other registration information unless being first specially authorized by you. There are two exceptions under this regulation: Veeprop will disclose your personal information under relevant law enforcement and this Company will share partial registration information with partner companies or subsidiaries (bounded by duty of confidentiality). Veeprop uses safe techniques, applies privacy management tools and restricts employee's access to private information to protect your personal privacy. Only limited number of authorized staffs can access your personal information.
</li>
</ol>
<b>THREE: CLIENT REGISTRATION</b>
<ol>
    <li>Veeprop adopts semi-open registration system. Any new account registration must be recommended by a registered account. The account that recommends new account registration will become the recommender of the new account. Only individuals who registered their accounts can become Clients of this Company and enjoy products and services from this Company.</li>
    <li>A Client has to obtain all policies and information about the Company from the recommender. Having not understood the Company's system and regulation after registration shall not be used as an excuse to request for withdrawal or any compensation from Veeprop</li>
    <li>This Company is a network company registered in the British Virgins Islands. Client of any citizenship has to understand the relevant local laws and regulations in this country before registering an account in this Company. Client has to join this Company under the condition that the local law is obeyed. If Client does not obey to the local laws and regulations, the Client has to bear the consequences and this Company will not be responsible.</li>
    <li>The Client shall possess corresponding capacity and responsibility. Parents or guardians shall represent users who are minors or mentally handicapped to complete the registration process and be responsible to the actions and incidents occurs within the minors' ID. The Company will not hold any responsibility for any minors or mentally handicapped persons who independently registered an account in this Company without consulting their parents or guardians. No one shall request this Company for compensation based on this reason.</li>
    <li>The Client shall provide complete, detailed and true personal information (including personal identification information and other personal information); personal identification information includes legal names and identity card number. The Client shall constantly update other information besides personal identification information and bank information to fulfill the requirement that the information has to be truthful, accurate and complete.</li>
    <li>The account will be permanent and cannot be cancelled after the Client successfully registered. The Client have to read all the necessary information before registering and shall not request to cancel the account or request for refund on any excuse. </li>
</ol>
<b>FOUR: PASSWORD SECURITY</b>
<ol>
    <li>The Client is responsible to keep the account and password safe. The Client shall not transfer or loan out the account or password at will, shall not share the account with other person and shall not disclose password of his or her own account or information used for registration to anyone. The Client bear total responsibility for the safety of his or her account and password and also for the series of actions or activities occur after the combination of account user name and password being used to log into the system. (If the Client authorizes other person to administer the account, the Client will still need to be responsible for any actions within the account.</li>
    <li>The Client shall bear own responsibility if the account password was leaked out due to Client's own fault (For example: using passwords that are too simple, using someone else's computer or allowing others to use the Client's computer) or virus infection on the Client's computer (including but not limited to Trojan, attacks from hackers or any other reasons).</li>
    <li>If the Client discovered that the account or password was illegally used by other person or abnormal usage situation, please contact Veeprop immediately and submit relevant proofs that the account belongs to the Client to request for the account to be suspended. The Client understands and agrees that Veeprop will not be responsible for the compensation for losses incurred. Meanwhile, Veeprop bears no obligation to disclose any data to the Client.</li>

</ol>
<b>FIVE: SAFETY OF CONTACTS METHOD</b>
<ol>
    <li>Email Address: Veeprop mainly contact with the Client through emails. The Client must ensure the security and usability of the email address registered in the account. The Client has to be responsible for any consequences caused by and documents sent from the email address registered in his or her account. (If the Client forgets his or her own registered email address and password or the email address expires for usage, any consequences caused must be assumed by the Client.)</li>
    <li>Telephone: Due to the low security to contact through phone, phone contacts are only limited to certain common affair. The Company has the right to request the Client to contact through email with the Company when necessary.</li>
<li>Due to different mailbox settings, spamming rules by email providers (example: 126.com, mail.qq.com, etc), emails that were sent from Veeprop may not reach to the Client Inbox efficiently. It is encouraged that the Client seek these providers advice in making sure that the mails sent from Veeprop are whitelist. The Client understands and agrees that Veeprop will not be responsible for the compensation for losses incurred due to the failures in receiving Veeprop emails.
</li>
</ol>
<b>SIX: CUSTOMER'S BEHAVIOR RESTRICTIONS</b>
<ol>
    <li>The Client shall ensure that he or she abide to the respective country's law and regulations in the process of using Veeprop?s account and service and shall not conduct any illegal activities using the account and services provided by Veeprop.</li>
    <li>The Client shall not conduct any "hack" to the Veeprop?s server, including but not limited to invading the server or trying to make the server overloaded; attack, crack, change and use other account's password; conduct attacks or invades in any form to the Veeprop server using any techniques.</li>
    <li>The Client shall not produce, disseminate or use any form of additional program, supporting program or third-party program to log in or receive services provided by Veeprop; The Client shall not exploit any intellectual property of Veeprop to create or provide identical or similar service, such as emulation server.</li>
    <li>The Client shall not exploit possible technical flaws or bugs in the program to cheat or to gain profit.</li>
    <li>Veeprop prohibits users to exploit service provided by Veeprop to conduct actions irrelevant to such service, including but not limited to jeopardizing reputation or privacy of others; using own, anonymous or impersonation name to disseminate information or texts which are defamatory, false, threatening, indecent, obscene, unlawful, offensive or infringing upon others' right; spreading pornographic or other expressions that violate the social morality; transmit or spread computer virus; engaging in unlawful transactions, putting up false or crime inducing information and others.</li>
    <li>The Client shall cooperate with the management of Veeprop in their work, respond to inquiry documents in time and clarify the facts. Else, Veeprop has the right to terminate the service provided to the Client at any time. The Client shall not submit any request to Veeprop for compensation or refund.</li>
    <li>The Client shall bear the responsibility for the consequences caused by actions or activities (including but not limited to the process of user's self-execution or interactions with other Clients) in the process of receiving service provided by Veeprop. Veeprop bears no responsibility for user's action, activities or transaction in the process of using the service provided.</li>
</li>
</ol>
<b>SEVEN: ASSUMPTION OF RISK AND RELEASE OF LIABILITY</b>
<ol>
    <li>The Client shall understand and agree that the service provided by Veeprop may be fully or partially interrupted, temporarily unavailable, delayed due to malfunction or failure of the network hardware and software equipment owned by this Company, other partner companies or related telecommunication carriers, or due to manual operation negligence of partner or related telecommunication staffs; or suspension or termination of service or loss of user's file due to data falsification, forgery or alteration caused by intrusion of others into Veeprop network. The Client shall not request any compensation from Kirin network.</li>
    <li> Veeprop bear no responsibility to any damage compensation for any direct, indirect or derived damages or profit loss caused by the Client's use of the service or the unavailability of the service.</li>
</li>
</ol>
<b>EIGHT: LINKS</b>
<ol>
    <li>
All the links provided by Veeprop in all web pages in the company website www.finnciti.com may link to other personal, company or organization website. Such sites are provided for the purpose to facilitate Client in searching or obtaining information on his or her own. Veeprop does not guarantee the authenticity, integrity or reliability of the products, service or information provided by personal, company or organization website linked. No employment, appointment, proxy, partnership or any other similar relationship exist between Veeprop and these personal, company or organization. (Unless this Company makes a statement that such company is partner of this Company.)
    </li>
</ol>

<b>NINE: COMPENSATION</b>
<ol>
    <li>
   If Client violated terms of service or related law, causing Veeprop or related business, employees, trustees, agents and/or other relevant implementation aid-party to suffer damages or expense (including but not limited to legal costs, legal fees, actual losses and other expenses incurred due to legal proceeding or administrative procedures), the Client shall bear the responsibility to indemnify the damages.
   </li>
</ol>
<b>
    TEN: ADVERTISEMENT INFORMATION DISCLAIMER</b>
<ol>
    <li>
  Veeprop may post commercial advertisement or promotional advertisement from other company on the related website position during the process of providing service. These contents belong to the advertiser or product and services providers whereby Veeprop is just a medium to publish the contents. Services or products bought by the Client through Veeprop website or other linked websites are transactions between only the Client and respective product and service provider hence are not related to Veeprop.
    </li>
</ol>
<b>ELEVEN: FORCE MAJEURE</b>
<ol>
    <li>
   Under any circumstances, Veeprop will not be responsible for any delay or dysfunction caused by factors beyond reasonable control, including but not limited to unforeseen situations or reason beyond the control of Veeprop, such as natural disaster, war, terrorism, riots, embargo, administrative or military authoritative acts, flood, fire, accidents, strike or shortage of transportation, fuel, labor, material or computer software and hardware.
  </li>
</ol>
<b>
TWELVE: GOVERNING LAW</b>
<ol>
    <li>
Any invalidity of a partial or a full clause provided by these terms of service will not affect the validity of other clauses. 
    </li>  <li>
If any dispute or controversy occurs between the Client and Veeprop, it must be first settled through friendly negotiations. If negotiation fails, the Client hereby completely agrees to refer the dispute or controversy to the jurisdiction of the British Virgin Islands District Court where Veeprop is located. 
    </li>
</ol>
        </a>


       
    </div>   
    <%@include file="footer.jsp" %>      
</body>
</html>