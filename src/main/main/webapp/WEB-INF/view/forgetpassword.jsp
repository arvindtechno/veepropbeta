
        <!DOCTYPE HTML>
        <%@page contentType="text/html" pageEncoding="windows-1252"%> 
        <%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core" %>
        <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

           <html>
             <head>
               <meta charset="UTF-8">
               <meta name="viewport" content="width=device-width, initial-scale=1.0">
                <title>Sign In</title>
               
                <link rel="stylesheet" href="/veepropbeta/css/style.css" type="text/css" media="screen" />
                <link href='http://fonts.googleapis.com/css?family=Open+Sans|Baumans' rel='stylesheet' type='text/css'>
                <link rel="stylesheet" href="/veepropbeta/css/bootstrap-responsive.css" type="text/css" media="screen" />
                <link rel="stylesheet" href="/veepropbeta/css/bootstrap-responsive.min.css" type="text/css" media="screen" />
                <link rel="stylesheet" href="/veepropbeta/css/bootstrap.css" type="text/css" media="screen" />
                <link rel="stylesheet" href="/veepropbeta/css/bootstrap.min.css" type="text/css" media="screen" />
                <script src="/veepropbeta/js/modernizr.js"></script>
                <script src="/veepropbeta/js/respond.min.js"></script>
                <script src="http://code.jquery.com/jquery-1.7.2.min.js"></script>
                <script src="/veepropbeta/js/lightbox.js"></script>
                <script src="/veepropbeta/js/prefixfree.min.js"></script>
                <script src="/veepropbeta/js/jquery.slides.min.js"></script>
           </head>

                <body>
                     <%@include file="header.jsp" %>
                     
                       <section id="spacer">
                         <h2 class="hidden">Notice Bar</h2>
                            <p>Stock News</p>
                        <div class="search">
                            <form action="#">
                      
                            </form>
                     </div>
                    </section>
             
                   <div class="container-fluid">
                   <div class="span5" style="margin-left:25%;">
                   <a href="/veepropbeta/login" class="list-group-item active">Reset your password</a>
                   
                   <form:form commandName="resetpassword" action="/veepropbeta/forgetpassword" method="POST">
                       
                    <div class="form-group">
                        <form:label for="inputEmail3" path="userName" class="col-sm-2 control-label">Email</form:label>
                      <div class="col-sm-10">
                        <form:input path="userName" class="form-control" id="inputEmail3" placeholder="Please Enter your Email "/>
                      </div>
                      </div>
                      
                       
                   
                     <div class="form-group">
                      <div class="col-sm-offset-2 col-sm-10">
                        <div class="checkbox">
                          <label>
                            
                          </label>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-default">Reset</button>
                        <button type="Reset" class="btn btn-danger" style="margin-left: 2%;">Cancel</button>
                        
                      </div>
                    </div>
                   </form:form>

                </div>
                 
                 </div>
<div style="height: 30px;"></div>
          <%@include file="footer.jsp" %>
       </body>
           </html>