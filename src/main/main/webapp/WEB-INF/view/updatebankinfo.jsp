
<%-- 
    Document   : profile
    Created on : 11 Feb, 2014, 3:02:47 PM
    Author     : Core Techies
--%>

<%@taglib  prefix="form" uri="http://www.springframework.org/tags/form" %>
 <!DOCTYPE HTML>
  <html>
    <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="/veepropbeta/css/style.css" type="text/css" media="screen" />
        <link href='http://fonts.googleapis.com/css?family=Open+Sans|Baumans' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="/veepropbeta/css/bootstrap-responsive.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="/veepropbeta/css/bootstrap-responsive.min.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="/veepropbeta/css/bootstrap.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="/veepropbeta/css/bootstrap.min.css" type="text/css" media="screen" />
        <script src="http://code.jquery.com/jquery-1.7.2.min.js"></script>
        <link rel="stylesheet" type="text/css" href="/veepropbeta/css/style_light.css">
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>
        <script src="/veepropbeta/js/jquery-ui-1.8.14.custom.min.js" type="text/javascript"></script>
        <script src="/veepropbeta/js/ttw-notification-menu.min.js" type="text/javascript"></script>
        <script src="/veepropbeta/js/demo.js" type="text/javascript"></script>      
        <style type="text/css">
        .tooltip {
            width: 250px;
            font-size: 11px;
            font-family: Arial, sans-serif;
            background: #444;
            border: 1px solid #090909;
            border-radius: 4px;
            -moz-border-radius: 4px;
            -webkit-border-radius: 4px;
            position: absolute;
            z-index: 1;
            box-shadow: 0 2px 5px rgba(0, 0, 0, 0.5);
            -webkit-box-shadow: 0 2px 5px rgba(0, 0, 0, 0.5);
            -moz-box-shadow: 0 2px 5px rgba(0, 0, 0, 0.5);
            color:#fff;
            padding:12px 24px;
            line-height:18px;
        }

        .tooltip:after {
            content: '';
            position: absolute;
            border-color: transparent  #444 transparent transparent;
            border-style: solid;
            border-width: 10px;
            height: 0;
            width: 0;
            position: absolute;
            left: 0;
            top: 50%;
            margin-top: -10px;
            margin-left: -20px;
        }

        .tooltip:before {
            content: '';
            position: absolute;
            border-color:   transparent #090909 transparent transparent;
            border-style: solid;
            border-width: 10px;
            height: 0;
            width: 0;
            position: absolute;
            left: 0;
            top: 50%;
            margin-top: -10px;
            margin-left: -20px;
        }

        .ttw-notification-menu{
                width: 276px;
        }
</style>
</head>

     <body>
     <input type="hidden" id="notification-form" />   
     <%@include file="header.jsp" %>
     <%@include file="information.jsp" %>
    <c:forEach var="notification" items="${notification}" varStatus="status">
        <script type="text/javascript">
            $(document).ready(function(e){
            $("#notification-form").val('<a href="${notification.notificationLink}/${notification.notificationId}">${notification.notification}</a>');
            $("#notification-form").click();
        });
        </script>
    </c:forEach> 
       <div class="container-fluid" style="margin-top: -10px;">
         <div class="row-fluid">
          <%@include  file="sellboard.jsp" %>
           <div class="span6">
               <ul class="nav nav-pills">
                <li><a href="/veepropbeta/home">Dashboard</a></li>
                <li><a href="/veepropbeta/profile">Profile</a></li>
                <li><a href="/veepropbeta/wallet">Wallet</a></li>
                <li><a href="/veepropbeta/business">Biz Management</a></li>
                <li><a href="/veepropbeta/feedback">Feedback</a></li>
                 <li><a id="projects" class="notification-menu-item first-item list_inline" style="float: right;">Notifications</a></li>
              </ul>
              <a href="#" class="list-group-item active" style="margin-top: 2%;">Your Bank Details<b style="margin-left: 70%;">Update</b></a>
              
               <form:form action="/veepropbeta/updatebankinfo" id="contact-form" class="form-horizontal" method="POST" modelAttribute="updatebankinfo" >
                   
                   <center>
                       <c:if test="${not empty param['update']}">
                           <span style="color:red; font-weight: bold; margin-top: 10px;"> ${param['update']} </span>
                       </c:if>
                   </center>
                    <div class="form-group">
                    <form:label for="inputEmail3" path="bankName" class="col-sm-3 control-label">Bank Name</form:label>
                    <div class="col-sm-8">
                    <form:input path="bankName" class="form-control" id="name" placeholder="Please Enter a Bank Name"/>
                    <form:errors path="bankName" element="span" cssClass="error"/>
                    </div>
                    </div>

                    <div class="form-group">
                    <form:label for="inputEmail3" path="accountNumber" class="col-sm-3 control-label">Account Number</form:label>
                    <div class="col-sm-8">
                    <form:input path="accountNumber" class="form-control" id="name" placeholder="Please Enter a Account Number"/>
                    <form:errors path="accountNumber" element="span" cssClass="error" />
                    </div>
                    </div>

                   <div class="form-group">
                   <form:label for="inputEmail3" path="iFSC" class="col-sm-3 control-label">IFSC Code</form:label>
                   <div class="col-sm-8">
                   <form:input path="iFSC" class="form-control" id="name" placeholder="Please Enter a IFSC code"/>
                    <form:errors path="iFSC" element="span" cssClass="error"/>
                   </div>
                   </div>

                   <div class="form-group">
                    <form:label for="inputEmail3" path="accountType" class="col-sm-3 control-label">Account Type</form:label>
                    <div class="col-sm-8">
                    <form:select path="accountType" style="width:70%; height:10%;" class="form-control">
                      <option value="">Please Select Account Type</option>
                      <option value="1">Saving</option>
                      <option value="2">Current</option>
                      <option value="3">Personal</option>
                      <option value="4">Other</option>
                    </form:select>
                    <form:errors path="accountType" element="span" cssClass="error"/>
                    </div>
                    </div>

                    <div class="form-group">
                    <form:label for="inputEmail3" path="branch" class="col-sm-3 control-label">Branch</form:label>
                    <div class="col-sm-8">
                    <form:input path="branch" class="form-control" id="name" placeholder="Please Enter a Branch Name"/>
                    <form:errors path="branch" element="span" cssClass="error"/>
                    </div>
                    </div>

                    <div class="form-group">
                    <form:label for="inputEmail3" path="swift" class="col-sm-3 control-label">Swift Code</form:label>
                    <div class="col-sm-8">
                    <form:input path="swift" class="form-control" id="name" placeholder="Please Enter Swift Code"/>
                    <form:errors path="swift" element="span" cssClass="error"/>
                    </div>
                    </div>

                    <div class="form-group">
                    <form:label for="" path="bankAddress" class="col-sm-3 control-label">Bank Address</form:label>
                    <div class="col-sm-8">
                    <form:textarea rows="3"  class="form-control" path="bankAddress"></form:textarea>
                    <form:errors path="bankAddress" element="span" cssClass="error"/>
                    </div>
                    </div>

                    <div class="form-group" style="margin-bottom: 2%;">
                    <div class="col-sm-2 offset3">
                    <form:button  class="btn btn-success">Update</form:button>
                    </div>
                    <div class="col-sm-2 offset">
                    <button type="button" class="btn btn-default">Cancel</button>
                    </div>
                    </div>
             </form:form>
          </div>
          <%@include  file="noticeboard.jsp" %>
        </div>
     </div>
  
 
    <%@include file="footer.jsp" %>
    
       
    
</body>
   
</html>
