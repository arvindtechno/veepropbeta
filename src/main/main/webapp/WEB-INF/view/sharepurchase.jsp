<%-- 
    Document   : profile
    Created on : 11 Feb, 2014, 3:02:47 PM
    Author     : aamir khan
--%>


<%@taglib  prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib  prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE HTML>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" href="/veepropbeta/css/style.css" type="text/css" media="screen" />
    <link href='http://fonts.googleapis.com/css?family=Open+Sans|Baumans' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="/veepropbeta/css/bootstrap-responsive.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="/veepropbeta/css/bootstrap-responsive.min.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="/veepropbeta/css/bootstrap.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="/veepropbeta/css/bootstrap.min.css" type="text/css" media="screen" />
    <script src="http://code.jquery.com/jquery-1.7.2.min.js"></script>
    <link rel="stylesheet" type="text/css" href="/veepropbeta/css/style_light.css">
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>
    <script src="/veepropbeta/js/jquery-ui-1.8.14.custom.min.js" type="text/javascript"></script>
    <script src="/veepropbeta/js/ttw-notification-menu.min.js" type="text/javascript"></script>
    <script src="/veepropbeta/js/demo.js" type="text/javascript"></script>      
    <style type="text/css">
        .tooltip {
            width: 250px;
            font-size: 11px;
            font-family: Arial, sans-serif;
            background: #444;
            border: 1px solid #090909;
            border-radius: 4px;
            -moz-border-radius: 4px;
            -webkit-border-radius: 4px;
            position: absolute;
            z-index: 1;
            box-shadow: 0 2px 5px rgba(0, 0, 0, 0.5);
            -webkit-box-shadow: 0 2px 5px rgba(0, 0, 0, 0.5);
            -moz-box-shadow: 0 2px 5px rgba(0, 0, 0, 0.5);
            color:#fff;
            padding:12px 24px;
            line-height:18px;
        }

        .tooltip:after {
            content: '';
            position: absolute;
            border-color: transparent  #444 transparent transparent;
            border-style: solid;
            border-width: 10px;
            height: 0;
            width: 0;
            position: absolute;
            left: 0;
            top: 50%;
            margin-top: -10px;
            margin-left: -20px;
        }

        .tooltip:before {
            content: '';
            position: absolute;
            border-color:   transparent #090909 transparent transparent;
            border-style: solid;
            border-width: 10px;
            height: 0;
            width: 0;
            position: absolute;
            left: 0;
            top: 50%;
            margin-top: -10px;
            margin-left: -20px;
        }

        .ttw-notification-menu{
                width: 276px;
        }


    </style>
    <script>
                function alertMessage(){
               alert("We are in Generation 1, you can not sell share");
          }
      </script> 
                

<script type="text/javascript">
    $(document).ready(function(){
        $("#sharehold").change(function(){
            var share = ($(this).val());
            if(share%10===0){
            }else{
                $(this).val(share - share%10); 
            }
            var IGC = $(this).val() * ${priceRate};
            $("#ingame").val(IGC);
            $(".ingame").show();
        });
        return false;
    });    
      
    function validateIGC(){
        var requiredIGC= $("#ingame").val();
        var availableIGC = ${MaxIGC};
        if(requiredIGC <= availableIGC){
            if(requiredIGC==0){
                $("#fund").html("atleast purchase 10 share");
                return false;
            }else{
                return true;
            }
        }else{
            $("#fund").html("In sufficient IGC");
            return false;
        }
    }
    </script>
   
</head>

<body>
    <input type="hidden" id="notification-form" />   
       <%@include file="header.jsp" %>
      
       <%@include file="information.jsp" %>
     <c:forEach var="notification" items="${notification}" varStatus="status">
        <script type="text/javascript">
            $(document).ready(function(e){
            $("#notification-form").val('<a href="${notification.notificationLink}/${notification.notificationId}">${notification.notification}</a>');
            $("#notification-form").click();
        });
        </script>
    </c:forEach>
    <div class="container-fluid" style="margin-top: -10px;">
        <div class="row-fluid">
        <%@include  file="sellboard.jsp" %>
        <div class="span6">
              <ul class="nav nav-pills">
                <li><a href="#">Dashboard</a></li>
                 <li><a href="/veepropbeta/profile">Profile</a></li>
                 <li><a href="#">Wallet</a></li>
                 <li><a href="#">Biz Management</a></li>
                 <li><a href="#">Feedback</a></li>
                <li><a id="projects" class="notification-menu-item first-item list_inline" style="float: right;">Notifications</a></li>
               </ul>
            <a href="#" class="list-group-item active" style="margin-top: 2%;">Purchase Share</a>
            <div style="height:30px;"></div>
            
            <div class="form-group">
                <label for="inputEmail3" style="margin-left: 28%;"><button type="button" class="btn btn-large btn-primary disabled" disabled="disabled">Your Total IGC Amount is:&nbsp;&nbsp;&nbsp;&nbsp;<b>${MaxIGC}</b></button></label>
           </div>
            <div class="form-group">
                <label for="inputEmail3" style="margin-left: 28%;"><button type="button" class="btn btn-large btn-primary disabled" disabled="disabled">Current Price rate in Market: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>${priceRate}</b></button></label>
             </div>
            <c:if test="${param['availableShare']!=null}">
                <ul><li class="list_inline_form">Available share is:</li> <li class="list_inline_form_db"><b>${param['availableShare']}</b></li></ul>
            </c:if>
             
                <form:form  action="/veepropbeta/sharepurchase" method="POST" modelAttribute="share" onsubmit="return validateIGC();" >
                 <div class="form-group" style="margin-left: 25%;">
                      <form:label for="inputEmail3" path="shareHold" class="col-sm-6 control-label">Enter Number Of Share:</form:label>
                   <div class="col-sm-10">
                       <form:input path="shareHold" class="form-control" id="sharehold" placeholder="Enter Share Number" pattern="[0-9]+"/>
                   </div>
                </div>
            <div style="height:30px;"></div>
                <div class="form-group" style="margin-top: 10%; margin-left: 25%;">
                    <form:label for="inputEmail3" path="IGC" class="col-sm-5 control-label">IGC Required:</form:label>
                  <div class="col-sm-10">
                     <form:input path="IGC" class="form-control" id="ingame" placeholder="require IGC" readonly="true"/>
                  </div>
                </div>
                  <div class="col-md-3 offset3" style="margin-top: 5%; ">
                       <form:button  class="btn btn-success">Purchase Share</form:button>
                   </div>         
                </form:form>
                
                
                
                
          </div>
             <%@include  file="noticeboard.jsp" %>
                 
        </div>
    </div>
  
 
    <%@include file="footer.jsp" %>
</body>
</html>



























































