<%-- 
    Document   : profile
    Created on : 11 Feb, 2014, 3:02:47 PM
    Author     : aamir khan
--%>

<%@page contentType="text/html" pageEncoding="windows-1252"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix = "spring" uri = "http://www.springframework.org/tags" %>

<!DOCTYPE HTML>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<style>
        .tooltip:after {
            content: '';
            position: absolute;
            border-color: transparent  #444 transparent transparent;
            border-style: solid;
            border-width: 10px;
            height: 0;
            width: 0;
            position: absolute;
            left: 0;
            top: 50%;
            margin-top: -10px;
            margin-left: -20px;
        }
        </style>
         <link rel="stylesheet" href="/veepropbeta/css/style.css" type="text/css" media="screen" />
                <link href='http://fonts.googleapis.com/css?family=Open+Sans|Baumans' rel='stylesheet' type='text/css'>
                <link rel="stylesheet" href="/veepropbeta/css/bootstrap-responsive.css" type="text/css" media="screen" />
                <link rel="stylesheet" href="/veepropbeta/css/bootstrap-responsive.min.css" type="text/css" media="screen" />
                <link rel="stylesheet" href="/veepropbeta/css/bootstrap.css" type="text/css" media="screen" />
                <link rel="stylesheet" href="/veepropbeta/css/bootstrap.min.css" type="text/css" media="screen" />
                <script src="/veepropbeta/js/modernizr.js"></script>
                <script src="/veepropbeta/js/respond.min.js"></script>
                <script src="http://code.jquery.com/jquery-1.7.2.min.js"></script>
                <script src="/veepropbeta/js/lightbox.js"></script>
                <script src="/veepropbeta/js/prefixfree.min.js"></script>
                <script src="/veepropbeta/js/jquery.slides.min.js"></script>

<script type="text/javascript">
    function validateVCredit(val){
        var check = ${vCreditMax};
        if(val <= check){
            return true;
        }else{
            alert("You Have InSufficient vCredit to transact,please select again");
            $("#VEE_PROP_V_CREDIT").val(0);
            return false;
        }
    }
    function checkEmail() {
        var email = $("#email").val();
        var pattern=/^([a-zA-Z0-9_.-])+@([a-zA-Z0-9_.-])+\.([a-zA-Z])+([a-zA-Z])+/;
        var message="";
        
        if(!pattern.test(email)){
            message = "Invalid Email";
            $("#verifyEmail").html(message);
            return false;
        } else {
            email = email.replace('.','&');
            var url = "/veepropbeta/checkEmail/"+email;
            $.get(url,function(data){
                
                if(data) {
                    message = "Email Available.";
                    $("#verifyEmail").html(message);
                    return true;
                } else {
                    message = "Email Already Exist.";
                    $("#verifyEmail").html(message);
                    return false;
                }
            });
        }
        $("#verifyEmail").html(message);
        return false;
    }
</script>
<style type="text/css">
    .errorblock{
        font-weight: bold;
        color: red;
    }
</style>
</head>

<body>
    <%@include file="header.jsp" %>
    <%@include file="information.jsp" %>
    <div class="container-fluid" style="margin-top: -10px;">
        <div class="row-fluid">
        <%@include  file="sellboard.jsp" %>
        <div class="span6">
                
                <ul class="nav nav-pills">
                    <li><a href="/veepropbeta/home">Dashboard</a></li>
                    <li><a href="/veepropbeta/profile">Profile</a></li>
                    <li><a href="/veepropbeta/wallet">Wallet</a></li>
                    <li><a href="/veepropbeta/business">Biz Management</a></li>
                    <li><a href="/veepropbeta/feedback">Feedback</a></li>
                    <li><a href="#">Notifications</a></li>
                </ul>
            <a href="/veepropbeta/register" class="list-group-item active" style="margin-top: 2%;">Register Here<b style="margin-left: 50%;">Your VCredit Amount:&nbsp;&nbsp; ${vCreditMax}</b></a>
            <center>
                <c:if test="${not empty param['insuff']}">
                    <b style="color:red;">In Sufficient Fund</b>
                </c:if>
            </center>
            <form:form action="/veepropbeta/registerChild" class="form-horizontal" onsubmit="return checkEmail();" method="POST" modelAttribute="registration" >
                 <div class="form-group">
                 <form:label for="inputEmail3" path="name" class="col-sm-4 control-label">Name</form:label>
                 <div class="col-sm-8">
                 <form:input path="name" class="form-control" id="name" placeholder="Enter Your Name"/>
                 <form:errors path="name" cssClass="errorblock" element="div" />
                 </div>
                 </div>
                
                 <div class="form-group">
                 <form:label for="inputEmail3" path="email" class="col-sm-4 control-label">Email ID:</form:label>
                 <div class="col-sm-8">
                 <form:input path="email" class="form-control" id="email" placeholder="Enter Your Email ID"/>
                 <form:errors path="email" cssClass="errorblock" element="div"/>
                 <span id="verifyEmail" class="errorblock"></span>
                 </div>
                 </div>
                
                 <div class="form-group">
                 <form:label for="inputEmail3" path="loginId" class="col-sm-4 control-label">Login ID:</form:label>
                 <div class="col-sm-8">
                 <form:input path="loginId" class="form-control" id="loginId" placeholder="Enter Your UserName"/>
                 <form:errors path="loginId" cssClass="errorblock" element="div" />
                 </div>
                 </div>
                 
                 <div class="form-group">
                 <form:label for="inputEmail3" path="residentCountry" class="col-sm-4 control-label">Country</form:label>
                 <div class="col-sm-8">
                 <form:select path="residentCountry" class="form-control">
                    <form:option value="0">--Please Select Country--</form:option>
                    <form:option value="1">INDIA</form:option>
                    <form:option value="2">AUS</form:option>
                    <form:option value="3">BRAZIL</form:option>
                    <form:option value="4">USA</form:option>
                    <form:option value="5">UK</form:option>
                    <form:option value="6">DUBAI</form:option>
                  </form:select>
                    <form:errors path="residentCountry" cssClass="errorblock" element="div" />
                 </div>
                 </div>
                
                
                 <div class="form-group">
                 <form:label for="inputEmail3" path="contact" class="col-sm-4 control-label">Mobile</form:label>
                 <div class="col-sm-8">
                 <form:input path="contact" class="form-control" id="name" placeholder="Enter your Mobile Number"/>
                 <form:errors path="contact" cssClass="errorblock" element="div" />
                 </div>
                 </div>
                 
                 <div class="form-group">
                 <form:label for="inputEmail3" path="passportNo" class="col-sm-4 control-label">Identity Card/Passport</form:label>
                 <div class="col-sm-8">
                 <form:input path="passportNo" class="form-control" id="loginId" placeholder="Enter your IC/PassPort Number"/>
                 <form:errors path="passportNo" cssClass="errorblock" element="div" />
                 </div>
                 </div>
                
                <div class="form-group">
                <form:label for="inputEmail3" path="gender" class="col-sm-4 control-label">Gender</form:label>
                <div class="col-sm-8">
                <form:radiobutton path="gender" value="0"/><b style="margin-left: 1%;">Male</b>
                <form:radiobutton path="gender" cssStyle="margin-left:10%;" value="1"/><b style="margin-left: 1%;">Female</b>
                <form:errors path="gender" cssClass="errorblock" element="div" />
                </div>
                </div>
                 
                <div class="form-group">
                <form:label for="inputEmail3" path="vCredit" class="col-sm-4 control-label">V-Credit</form:label>
                <div class="col-sm-8">
                <form:select path="vCredit" onchange="validateVCredit(this.value)" id="VEE_PROP_V_CREDIT"  class="form-control">
                    <form:option value="0">-Please Select vCredit for Trader-</form:option>
                    <form:option value="100">100</form:option>
                    <form:option value="200">200</form:option>
                    <form:option value="500">500</form:option>
                    <form:option value="1000">1000</form:option>
                    <form:option value="2000">2000</form:option>
                    <form:option value="5000">5000</form:option>
                    <form:option value="10000">10000</form:option>
                    <form:option value="100000">100000</form:option>
                </form:select>  
                    <form:errors path="vCredit" cssClass="errorblock" element="div" />
                </div>
                </div>
                <form:hidden path="BP" />
                <form:hidden path="PTP" /> 
                <div class="form-group">
                <div class="col-sm-2 offset4">
                <form:button  class="btn btn-success">Register</form:button>
                </div>
                <div class="col-md-4 offset">
                <form:button type="reset"  class="btn btn-primary">Cancel</form:button>
                </div>
                </div>
             </form:form>
            </div>
        <%@include  file="noticeboard.jsp" %>
                 
        </div>
    </div>
  
 
    <%@include file="footer.jsp" %>
</body>
</html>
