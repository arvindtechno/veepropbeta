/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.coretechies.veepropbeta.service.calculationmatchingBonus;


import com.coretechies.veepropbeta.domain.Login;
import com.coretechies.veepropbeta.domain.TraderRelativeData;
import com.coretechies.veepropbeta.service.login.LoginService;
import com.coretechies.veepropbeta.service.traderrelativedata.TraderRelativeDataService;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Noppp
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
        "classpath:veeprop-service.xml"})
@Transactional
@Ignore
public class CalculateMatchingBonusServiceImplTest {
    
    @Autowired
    private CalculateMatchingBonusService cmbs;
    
    @Autowired
    private TraderRelativeDataService traderRelativeDataService;
    
    @Autowired
    private LoginService loginService;
    
    private final Logger logger = LoggerFactory.getLogger(getClass());
    
    public CalculateMatchingBonusServiceImplTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of calculateMatchingBonus method, of class CalculateMatchingBonusServiceImpl.
     */
    @Test
    @Ignore
    public void testCalculateMatchingBonus() {
        System.out.println("calculateMatchingBonus");
        cmbs.calculateMatchingBonus(3, 2000, 1);
        cmbs.calculateMatchingBonus(3, 2000, 0);
    }
    
    @Test
    @Ignore
    public void testCountTrader(){
        int l = 1,left = 0,right = 0;
        List<TraderRelativeData> retrieveChild = traderRelativeDataService.retrieveChild(l);
        for(TraderRelativeData trd : retrieveChild){
            if(trd.getpTP() == 0){
                left = left + 1;
                retrieveChild = traderRelativeDataService.retrieveChild(trd.getIdentificationNumber());
                while(!retrieveChild.isEmpty()){
                    for(TraderRelativeData trdd : retrieveChild){
                        left = left + 1;
                        retrieveChild = traderRelativeDataService.retrieveChild(trdd.getIdentificationNumber());
                    }
                }
            }else if(trd.getpTP() == 1){
                right = right + 1;
                retrieveChild = traderRelativeDataService.retrieveChild(trd.getIdentificationNumber());
                while(!retrieveChild.isEmpty()){
                    for(TraderRelativeData trdd : retrieveChild){
                        right = right + 1;
                        retrieveChild = traderRelativeDataService.retrieveChild(trdd.getIdentificationNumber());
                    }
                }
            }
        }
    }
    
}
